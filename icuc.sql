-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Feb 10, 2021 alle 22:46
-- Versione del server: 10.1.37-MariaDB
-- Versione PHP: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `icuc`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `annata`
--

CREATE TABLE `annata` (
  `Id` int(11) NOT NULL,
  `Annata` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `annata`
--

INSERT INTO `annata` (`Id`, `Annata`) VALUES
(1, '2012'),
(2, '2013'),
(3, '2015'),
(4, '2016'),
(5, '2017'),
(6, '2018'),
(7, '2019'),
(8, '2020');

-- --------------------------------------------------------

--
-- Struttura della tabella `categoria`
--

CREATE TABLE `categoria` (
  `Id` int(11) NOT NULL,
  `Categoria` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `cupon`
--

CREATE TABLE `cupon` (
  `Codice` varchar(30) NOT NULL,
  `Prezzo` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struttura della tabella `indirizzo`
--

CREATE TABLE `indirizzo` (
  `Id` int(11) NOT NULL,
  `IdUtente` int(11) NOT NULL,
  `ViaCivico` varchar(50) NOT NULL,
  `Citta` varchar(30) NOT NULL,
  `Provincia` varchar(30) NOT NULL,
  `Cap` varchar(30) NOT NULL,
  `Nome` varchar(30) NOT NULL,
  `Cognome` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `indirizzo`
--

INSERT INTO `indirizzo` (`Id`, `IdUtente`, `ViaCivico`, `Citta`, `Provincia`, `Cap`, `Nome`, `Cognome`) VALUES
(1, 1, 'via Roma, 15', 'Genova', 'Genova', '43235', 'Alfonso', 'Castrazzozza'),
(2, 1, 'via del Campo, 15', 'Cesena', 'Forlì-Cesena', '47521', 'Gianfelice', 'Spagnagatti'),
(3, 1, 'via Roma, 15', 'Campobasso', 'cb', '47521', 'Mattia', 'Lorenzinini'),
(4, 1, 'via Fulcieri Paulucci de Calboli, 126', 'ForlÃ¬', 'fc', '47121', 'Ana Maria Andreea', 'Murvai LP'),
(5, 1, 'via prova, 15', 'Agrigento', 'ag', '11232', 'Ciao', 'Mia'),
(6, 1, 'via ciao, 5', 'San Vito Lo Capo', 'tp', '12345', 'Prova', '2'),
(7, 1, 'via Maccio, 15', 'Bologna', 'bo', '11111', 'Pino', 'Cammino'),
(8, 3, 'Viale Salinatore, 125', 'Forlì', 'fc', '47121', 'Giovannino', 'Testasiti'),
(11, 6, 'via Fulcieri Spiazzoli de Calbori, 126', 'Cesena', 'kr', '47121', 'Eddie', 'Belloni Vissani'),
(14, 9, 'via Buginello, 15', 'Rocca San Casciano', 'fc', '47111', 'Gianfelice', 'Spagnagatti'),
(25, 20, 'via pippi', 'Pipponia', 'bg', '2342', 'Pippo', 'Pippi'),
(26, 21, 'via Prova, 15', 'Provincia', 'bg', '47121', 'Prova', 'Provina'),
(27, 22, 'via Ambrogio Liverani, 2', 'Forlì', 'fc', '47121', 'Enrico', 'Brunetti'),
(28, 23, 'via Calcinaro, 2910', 'Cesena', 'fc', '47521', 'Mattia', 'Lorenzini'),
(29, 24, 'VIA CALCINARO N.2930 ', 'CESENA', 'fc', '47521', 'barbara', 'pirini casadei');

-- --------------------------------------------------------

--
-- Struttura della tabella `newsletter`
--

CREATE TABLE `newsletter` (
  `Email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `notifica`
--

CREATE TABLE `notifica` (
  `Id` int(11) NOT NULL,
  `IdUtente` int(11) NOT NULL,
  `IdOrdine` int(11) NOT NULL,
  `Titolo` varchar(100) NOT NULL,
  `Testo` text NOT NULL,
  `Colore` varchar(30) NOT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Admin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `notifica`
--

INSERT INTO `notifica` (`Id`, `IdUtente`, `IdOrdine`, `Titolo`, `Testo`, `Colore`, `Timestamp`, `Admin`) VALUES
(1, 20, 84, 'Piemonte DOC Rosso 8 Bucce 2018', '#84: l\'ordine contenente Piemonte DOC Rosso 8 Bucce 2018 x 1 è stato ricevuto correttamente. Riceverai ulteriori notifiche quando sarà spedito.', 'danger', '2021-02-08 22:17:22', 0),
(2, 21, 85, 'Piemonte DOC Rosso 8 Bucce 2018', '#85: l\'ordine contenente Piemonte DOC Rosso 8 Bucce 2018 x 1 è stato ricevuto correttamente. Riceverai ulteriori notifiche quando sarà spedito.', 'danger', '0000-00-00 00:00:00', 0),
(3, 21, 86, 'Piemonte DOC Rosso 8 Bucce 2018', '#86: l\'ordine contenente Piemonte DOC Rosso 8 Bucce 2018 x 1 è stato ricevuto correttamente. Riceverai ulteriori notifiche quando sarà spedito.', 'danger', '0000-00-00 00:00:00', 0),
(4, 21, 87, 'Amarone della Valpolicella DOCG Il Ciliegio 2015', '#87: l\'ordine contenente Amarone della Valpolicella DOCG Il Ciliegio 2015 x 1 è stato ricevuto correttamente. Riceverai ulteriori notifiche quando sarà spedito.', 'danger', '0000-00-00 00:00:00', 0),
(5, 21, 88, 'Monferrato Rosso Nebbiolo DOC Nebula 2016', '#88: l\'ordine contenente Monferrato Rosso Nebbiolo DOC Nebula 2016 x 5 è stato ricevuto correttamente. Riceverai ulteriori notifiche quando sarà spedito.', 'danger', '2021-02-08 22:56:03', 0),
(6, 21, 89, 'Amarone della Valpolicella Classico DOCG 2013', '#89: l\'ordine contenente Amarone della Valpolicella Classico DOCG 2013 x 3 è stato ricevuto correttamente. Riceverai ulteriori notifiche quando sarà spedito.', 'danger', '2021-02-08 22:56:03', 0),
(7, 22, 91, 'Spumante Blanc de Blancs Cuvee Brut', '#91: l\'ordine contenente Spumante Blanc de Blancs Cuvee Brut x 3 è stato ricevuto correttamente. Riceverai ulteriori notifiche quando sarà spedito.', 'danger', '2021-02-09 14:17:50', 0),
(8, 22, 91, 'Spumante Blanc de Blancs Cuvee Brut', '#91: l\'utente 22 ha ordinato Spumante Blanc de Blancs Cuvee Brut x 3.', 'danger', '2021-02-09 14:17:50', 1),
(9, 22, 92, 'Asti DOCG Collezione Speciale', '#92: l\'ordine contenente Asti DOCG Collezione Speciale x 10 è stato ricevuto correttamente. Riceverai ulteriori notifiche quando sarà spedito.', 'danger', '2021-02-09 14:28:40', 0),
(10, 22, 92, 'Asti DOCG Collezione Speciale', '#92: l\'utente #22 ha ordinato Asti DOCG Collezione Speciale x 10.', 'danger', '2021-02-09 14:28:40', 1),
(11, 22, 93, 'Asti DOCG Collezione Speciale', '#93: l\'ordine contenente Asti DOCG Collezione Speciale x 20 è stato ricevuto correttamente. Riceverai ulteriori notifiche quando sarà spedito.', 'danger', '2021-02-09 14:30:58', 0),
(12, 22, 93, 'Asti DOCG Collezione Speciale', '#93: l\'utente #22 ha ordinato Asti DOCG Collezione Speciale x 20.', 'danger', '2021-02-09 14:30:58', 1),
(13, 22, 94, 'Asti DOCG Collezione Speciale', '#94: l\'ordine contenente Asti DOCG Collezione Speciale x 1 è stato ricevuto correttamente. Riceverai ulteriori notifiche quando sarà spedito.', 'danger', '2021-02-09 14:32:03', 0),
(14, 22, 94, 'Asti DOCG Collezione Speciale', '#94: l\'utente #22 ha ordinato Asti DOCG Collezione Speciale x 1.', 'danger', '2021-02-09 14:32:03', 1),
(15, 22, 94, 'Prodotto esaurito', 'Prodotto esaurito: sono terminate le scorte per il prodotto Asti DOCG Collezione Speciale, è necessario il rifornimento.', 'secondary', '2021-02-10 09:25:52', 3),
(16, 22, 95, 'Piemonte DOC Rosato 4 Bucce 2020', '#95: l\'ordine contenente Piemonte DOC Rosato 4 Bucce 2020 x 2 è stato ricevuto correttamente. Riceverai ulteriori notifiche quando sarà spedito.', 'danger', '2021-02-09 14:37:04', 0),
(17, 22, 95, 'Piemonte DOC Rosato 4 Bucce 2020', '#95: l\'utente #22 ha ordinato Piemonte DOC Rosato 4 Bucce 2020 x 2.', 'danger', '2021-02-09 14:37:04', 1),
(18, 22, 97, 'Grignolino d\'Asti DOC Lanfora 2017', '#97: l\'ordine contenente Grignolino d\'Asti DOC Lanfora 2017 x 14 è stato ricevuto correttamente. Riceverai ulteriori notifiche quando sarà spedito.', 'danger', '2021-02-09 14:44:31', 0),
(19, 22, 97, 'Grignolino d\'Asti DOC Lanfora 2017', '#97: l\'utente #22 ha ordinato Grignolino d\'Asti DOC Lanfora 2017 x 14.', 'danger', '2021-02-09 14:44:31', 1),
(20, 22, 97, 'Prodotto esaurito', 'Prodotto esaurito: sono terminate le scorte per il prodotto Grignolino d\'Asti DOC Lanfora 2017, è necessario il rifornimento.', 'secondary', '2021-02-09 21:32:34', 3),
(21, 22, 98, 'Monferrato Rosso Nebbiolo DOC Nebula 2016', '#98: l\'ordine contenente Monferrato Rosso Nebbiolo DOC Nebula 2016 x 1 è stato ricevuto correttamente. Riceverai ulteriori notifiche quando sarà spedito.', 'danger', '2021-02-09 14:45:03', 0),
(22, 22, 98, 'Monferrato Rosso Nebbiolo DOC Nebula 2016', '#98: l\'utente #22 ha ordinato Monferrato Rosso Nebbiolo DOC Nebula 2016 x 1.', 'danger', '2021-02-09 14:45:03', 1),
(23, 22, 98, 'Ordine consegnato.', '#98: l\'ordine è stato consegnato. Facci sapere com\'è andata sui nostri canali social.', 'light', '2021-02-09 15:11:26', 0),
(24, 22, 98, 'Ordine consegnato.', '#: hai consegnato quest\'ordine.', 'light', '2021-02-09 15:11:26', 1),
(25, 22, 98, 'Ordine consegnato.', '#98: l\'ordine è stato consegnato. Facci sapere com\'è andata sui nostri canali social.', 'light', '2021-02-09 15:11:35', 0),
(26, 22, 98, 'Ordine consegnato.', '#: hai consegnato quest\'ordine.', 'light', '2021-02-09 15:11:35', 1),
(27, 22, 91, 'Ordine consegnato.', '#91: l\'ordine è stato consegnato. Facci sapere com\'è andata sui nostri canali social.', 'light', '2021-02-09 15:11:38', 0),
(28, 22, 91, 'Ordine consegnato.', '#: hai consegnato quest\'ordine.', 'light', '2021-02-09 15:11:38', 1),
(29, 1, 49, 'Ordine consegnato.', '#49: l\'ordine è stato consegnato. Facci sapere com\'è andata sui nostri canali social.', 'light', '2021-02-09 15:11:42', 0),
(30, 1, 49, 'Ordine consegnato.', '#: hai consegnato quest\'ordine.', 'light', '2021-02-09 15:11:42', 1),
(31, 22, 98, 'Ordine consegnato.', '#98: l\'ordine è stato consegnato. Facci sapere com\'è andata sui nostri canali social.', 'light', '2021-02-09 15:12:38', 0),
(32, 22, 98, 'Ordine consegnato.', '#: hai consegnato quest\'ordine.', 'light', '2021-02-09 15:12:38', 1),
(33, 22, 98, 'Ordine consegnato.', '#98: l\'ordine è stato consegnato. Facci sapere com\'è andata sui nostri canali social.', 'light', '2021-02-09 15:22:58', 0),
(34, 22, 98, 'Ordine consegnato.', '#98: hai consegnato quest\'ordine.', 'light', '2021-02-09 15:22:58', 1),
(35, 22, 91, 'Ordine consegnato.', '#91: l\'ordine è stato consegnato. Facci sapere com\'è andata sui nostri canali social.', 'light', '2021-02-09 15:22:59', 0),
(36, 22, 91, 'Ordine consegnato.', '#91: hai consegnato quest\'ordine.', 'light', '2021-02-09 15:22:59', 1),
(37, 22, 97, 'Ordine spedito.', '#97: l\'ordine è stato spedito. Lo riceverai all\'indirizzo indicato entro tre giorni lavorativi.', 'success', '2021-02-09 15:23:03', 0),
(38, 22, 97, 'Ordine spedito.', '#97: hai spedito quest\'ordine.', 'success', '2021-02-09 15:23:03', 1),
(39, 22, 95, 'Ordine spedito.', '#95: l\'ordine è stato spedito. Lo riceverai all\'indirizzo indicato entro tre giorni lavorativi.', 'success', '2021-02-09 15:23:12', 0),
(40, 22, 95, 'Ordine spedito.', '#95: hai spedito quest\'ordine.', 'success', '2021-02-09 15:23:12', 1),
(41, 22, 101, 'Nizza DOCG 2016', '#101: l\'ordine contenente Nizza DOCG 2016 x 4 è stato ricevuto correttamente. Riceverai ulteriori notifiche quando sarà spedito.', 'warning', '2021-02-09 15:26:35', 0),
(42, 22, 101, 'Nizza DOCG 2016', '#101: l\'utente #22 ha ordinato Nizza DOCG 2016 x 4.', 'warning', '2021-02-09 15:26:35', 1),
(43, 22, 101, 'Ordine spedito.', '#101: l\'ordine è stato spedito. Lo riceverai all\'indirizzo indicato entro tre giorni lavorativi.', 'success', '2021-02-09 15:27:45', 0),
(44, 22, 101, 'Ordine spedito.', '#101: hai spedito quest\'ordine.', 'success', '2021-02-09 15:27:45', 1),
(45, 22, 101, 'Ordine consegnato.', '#101: l\'ordine è stato consegnato. Facci sapere com\'è andata sui nostri canali social.', 'light', '2021-02-09 16:33:52', 0),
(46, 22, 101, 'Ordine consegnato.', '#101: hai consegnato quest\'ordine.', 'light', '2021-02-09 16:33:52', 1),
(47, 23, 102, 'Amarone della Valpolicella Classico DOCG 2013', '#102: l\'ordine contenente Amarone della Valpolicella Classico DOCG 2013 x 1 è stato ricevuto correttamente. Riceverai ulteriori notifiche quando sarà spedito.', 'warning', '2021-02-09 22:29:08', 0),
(48, 23, 102, 'Amarone della Valpolicella Classico DOCG 2013', '#102: l\'utente #23 ha ordinato Amarone della Valpolicella Classico DOCG 2013 x 1.', 'warning', '2021-02-09 22:29:08', 1),
(49, 22, 104, 'Amarone della Valpolicella Classico DOCG 2013', '#104: l\'ordine contenente Amarone della Valpolicella Classico DOCG 2013 x 74 è stato ricevuto correttamente. Riceverai ulteriori notifiche quando sarà spedito.', 'warning', '2021-02-10 09:34:56', 0),
(50, 22, 104, 'Amarone della Valpolicella Classico DOCG 2013', '#104: l\'utente #22 ha ordinato Amarone della Valpolicella Classico DOCG 2013 x 74.', 'warning', '2021-02-10 09:34:56', 1),
(51, 22, 104, 'Prodotto esaurito', 'Prodotto esaurito: sono terminate le scorte per il prodotto Amarone della Valpolicella Classico DOCG 2013, è necessario il rifornimento.', 'secondary', '2021-02-10 09:36:47', 3),
(52, 22, 105, 'Albana di Romagna DOCG Bianco Secco 2020', '#105: l\'ordine contenente Albana di Romagna DOCG Bianco Secco 2020 x 99 è stato ricevuto correttamente. Riceverai ulteriori notifiche quando sarà spedito.', 'warning', '2021-02-10 09:50:35', 0),
(53, 22, 105, 'Albana di Romagna DOCG Bianco Secco 2020', '#105: l\'utente #22 ha ordinato Albana di Romagna DOCG Bianco Secco 2020 x 99.', 'warning', '2021-02-10 09:50:35', 1),
(54, 22, 105, 'Prodotto esaurito', 'Prodotto esaurito: sono terminate le scorte per il prodotto Albana di Romagna DOCG Bianco Secco 2020, è necessario il rifornimento.', 'secondary', '2021-02-10 09:53:36', 3),
(55, 22, 105, 'Ordine spedito.', '#105: l\'ordine è stato spedito. Lo riceverai all\'indirizzo indicato entro tre giorni lavorativi.', 'success', '2021-02-10 09:53:49', 0),
(56, 22, 105, 'Ordine spedito.', '#105: hai spedito quest\'ordine.', 'success', '2021-02-10 09:53:49', 1),
(57, 22, 105, 'Ordine consegnato.', '#105: l\'ordine è stato consegnato. Facci sapere com\'è andata sui nostri canali social.', 'light', '2021-02-10 09:54:26', 0),
(58, 22, 105, 'Ordine consegnato.', '#105: hai consegnato quest\'ordine.', 'light', '2021-02-10 09:54:26', 1),
(59, 1, 77, 'Piemonte DOC Rosato 4 Bucce 2020', '#77: l\'ordine contenente Piemonte DOC Rosato 4 Bucce 2020 x 1 è stato ricevuto correttamente. Riceverai ulteriori notifiche quando sarà spedito.', 'warning', '2021-02-10 18:35:08', 0),
(60, 1, 77, 'Piemonte DOC Rosato 4 Bucce 2020', '#77: l\'utente #1 ha ordinato Piemonte DOC Rosato 4 Bucce 2020 x 1.', 'warning', '2021-02-10 18:35:08', 1),
(61, 1, 81, 'Asti DOCG Collezione Speciale', '#81: l\'ordine contenente Asti DOCG Collezione Speciale x 1 è stato ricevuto correttamente. Riceverai ulteriori notifiche quando sarà spedito.', 'warning', '2021-02-10 18:35:08', 0),
(62, 1, 81, 'Asti DOCG Collezione Speciale', '#81: l\'utente #1 ha ordinato Asti DOCG Collezione Speciale x 1.', 'warning', '2021-02-10 18:35:08', 1),
(63, 1, 107, 'Lambrusco dell\'Emilia Frizzante Semisecco IGT Solco 2020', '#107: l\'ordine contenente Lambrusco dell\'Emilia Frizzante Semisecco IGT Solco 2020 x 1 è stato ricevuto correttamente. Riceverai ulteriori notifiche quando sarà spedito.', 'warning', '2021-02-10 18:35:08', 0),
(64, 1, 107, 'Lambrusco dell\'Emilia Frizzante Semisecco IGT Solco 2020', '#107: l\'utente #1 ha ordinato Lambrusco dell\'Emilia Frizzante Semisecco IGT Solco 2020 x 1.', 'warning', '2021-02-10 18:35:08', 1),
(65, 1, 108, 'Albana di Romagna DOCG Bianco Secco 2020', '#108: l\'ordine contenente Albana di Romagna DOCG Bianco Secco 2020 x 1 è stato ricevuto correttamente. Riceverai ulteriori notifiche quando sarà spedito.', 'warning', '2021-02-10 18:37:43', 0),
(66, 1, 108, 'Albana di Romagna DOCG Bianco Secco 2020', '#108: l\'utente #1 ha ordinato Albana di Romagna DOCG Bianco Secco 2020 x 1.', 'warning', '2021-02-10 18:37:43', 1),
(67, 1, 109, 'Amarone della Valpolicella Classico DOCG 2013', '#109: l\'ordine contenente Amarone della Valpolicella Classico DOCG 2013 x 1 è stato ricevuto correttamente. Riceverai ulteriori notifiche quando sarà spedito.', 'warning', '2021-02-10 18:38:53', 0),
(68, 1, 109, 'Amarone della Valpolicella Classico DOCG 2013', '#109: l\'utente #1 ha ordinato Amarone della Valpolicella Classico DOCG 2013 x 1.', 'warning', '2021-02-10 18:38:53', 1),
(69, 1, 110, 'Barbera d\'Asti DOCG Superiore 2016', '#110: l\'ordine contenente Barbera d\'Asti DOCG Superiore 2016 x 1 è stato ricevuto correttamente. Riceverai ulteriori notifiche quando sarà spedito.', 'warning', '2021-02-10 18:42:27', 0),
(70, 1, 110, 'Barbera d\'Asti DOCG Superiore 2016', '#110: l\'utente #1 ha ordinato Barbera d\'Asti DOCG Superiore 2016 x 1.', 'warning', '2021-02-10 18:42:27', 1),
(71, 1, 111, 'Piemonte DOC Rosato 4 Bucce 2020', '#111: l\'ordine contenente Piemonte DOC Rosato 4 Bucce 2020 x 1 è stato ricevuto correttamente. Riceverai ulteriori notifiche quando sarà spedito.', 'warning', '2021-02-10 18:42:27', 0),
(72, 1, 111, 'Piemonte DOC Rosato 4 Bucce 2020', '#111: l\'utente #1 ha ordinato Piemonte DOC Rosato 4 Bucce 2020 x 1.', 'warning', '2021-02-10 18:42:27', 1),
(73, 1, 112, 'Amarone della Valpolicella Classico DOCG 2013', '#112: l\'ordine contenente Amarone della Valpolicella Classico DOCG 2013 x 1 è stato ricevuto correttamente. Riceverai ulteriori notifiche quando sarà spedito.', 'warning', '2021-02-10 18:48:37', 0),
(74, 1, 112, 'Amarone della Valpolicella Classico DOCG 2013', '#112: l\'utente #1 ha ordinato Amarone della Valpolicella Classico DOCG 2013 x 1.', 'warning', '2021-02-10 18:48:37', 1),
(75, 1, 113, 'Asti DOCG Collezione Speciale', '#113: l\'ordine contenente Asti DOCG Collezione Speciale x 1 è stato ricevuto correttamente. Riceverai ulteriori notifiche quando sarà spedito.', 'warning', '2021-02-10 18:51:34', 0),
(76, 1, 113, 'Asti DOCG Collezione Speciale', '#113: l\'utente #1 ha ordinato Asti DOCG Collezione Speciale x 1.', 'warning', '2021-02-10 18:51:34', 1),
(77, 24, 114, 'Albana di Romagna DOCG Bianco Secco 2020', '#114: l\'ordine contenente Albana di Romagna DOCG Bianco Secco 2020 x 1 è stato ricevuto correttamente. Riceverai ulteriori notifiche quando sarà spedito.', 'warning', '2021-02-10 20:51:22', 0),
(78, 24, 114, 'Albana di Romagna DOCG Bianco Secco 2020', '#114: l\'utente #24 ha ordinato Albana di Romagna DOCG Bianco Secco 2020 x 1.', 'warning', '2021-02-10 20:51:22', 1),
(79, 24, 114, 'Ordine spedito.', '#114: l\'ordine è stato spedito. Lo riceverai all\'indirizzo indicato entro tre giorni lavorativi.', 'success', '2021-02-10 20:55:21', 0),
(80, 24, 114, 'Ordine spedito.', '#114: hai spedito quest\'ordine.', 'success', '2021-02-10 20:55:21', 1),
(81, 24, 114, 'Ordine consegnato.', '#114: l\'ordine è stato consegnato. Facci sapere com\'è andata sui nostri canali social.', 'light', '2021-02-10 20:55:53', 0),
(82, 24, 114, 'Ordine consegnato.', '#114: hai consegnato quest\'ordine.', 'light', '2021-02-10 20:55:53', 1),
(83, 24, 115, 'Vermentino Maremma Toscana DOC 2019', '#115: l\'ordine contenente Vermentino Maremma Toscana DOC 2019 x 1 è stato ricevuto correttamente. Riceverai ulteriori notifiche quando sarà spedito.', 'warning', '2021-02-10 21:02:10', 0),
(84, 24, 115, 'Vermentino Maremma Toscana DOC 2019', '#115: l\'utente #24 ha ordinato Vermentino Maremma Toscana DOC 2019 x 1.', 'warning', '2021-02-10 21:02:10', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `ordine`
--

CREATE TABLE `ordine` (
  `Id` int(11) NOT NULL,
  `IdUtente` int(11) NOT NULL,
  `IdIndirizzo` int(11) NOT NULL,
  `IdProdotto` int(11) NOT NULL,
  `Quantita` int(11) NOT NULL,
  `PrezzoParziale` float NOT NULL,
  `Stato` int(11) NOT NULL,
  `UltimoAggiornamento` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `ordine`
--

INSERT INTO `ordine` (`Id`, `IdUtente`, `IdIndirizzo`, `IdProdotto`, `Quantita`, `PrezzoParziale`, `Stato`, `UltimoAggiornamento`) VALUES
(48, 1, 1, 70, 10, 239, 1, '2021-02-05'),
(49, 1, 1, 65, 3, 144, 2, '2021-02-09'),
(50, 1, 1, 63, 5, 90, 1, '2021-02-05'),
(51, 1, 1, 64, 2, 16.4, 1, '2021-02-05'),
(52, 1, 5, 3, 2, 27.2, 1, '2021-02-06'),
(53, 1, 5, 2, 5, 49.5, 1, '2021-02-06'),
(54, 1, 2, 67, 8, 71.2, 1, '2021-02-06'),
(55, 1, 2, 73, 2, 67, 1, '2021-02-06'),
(56, 1, 2, 63, 5, 90, 1, '2021-02-06'),
(57, 1, 7, 3, 2, 27.2, 1, '2021-02-06'),
(69, 1, 6, 66, 1, 16.9, 1, '2021-02-06'),
(70, 1, 6, 64, 3, 24.6, 1, '2021-02-06'),
(71, 1, 6, 67, 1, 8.9, 1, '2021-02-06'),
(72, 1, 6, 76, 10, 299, 1, '2021-02-06'),
(77, 1, 4, 64, 1, 8.2, 1, '2021-02-10'),
(78, 9, 14, 55, 4, 198, 1, '2021-02-07'),
(79, 9, 14, 63, 3, 54, 1, '2021-02-07'),
(80, 9, 14, 64, 2, 16.4, 1, '2021-02-07'),
(81, 1, 4, 63, 1, 18, 1, '2021-02-10'),
(83, 20, 25, 63, 8, 144, 1, '2021-02-08'),
(84, 20, 25, 67, 1, 8.9, 1, '2021-02-08'),
(85, 21, 26, 67, 1, 8.9, 1, '2021-02-08'),
(86, 21, 26, 67, 1, 8.9, 1, '2021-02-08'),
(87, 21, 26, 59, 1, 75, 1, '2021-02-08'),
(88, 21, 26, 66, 5, 84.5, 1, '2021-02-08'),
(89, 21, 26, 55, 3, 148.5, 1, '2021-02-08'),
(90, 21, 26, 63, 1, 18, 0, '2021-02-09'),
(91, 22, 27, 0, 3, 18.6, 3, '2021-02-09'),
(92, 22, 27, 63, 10, 180, 1, '2021-02-09'),
(93, 22, 27, 63, 20, 360, 1, '2021-02-09'),
(94, 22, 27, 63, 1, 18, 1, '2021-02-09'),
(95, 22, 27, 64, 2, 16.4, 2, '2021-02-09'),
(97, 22, 27, 68, 14, 245, 2, '2021-02-09'),
(98, 22, 27, 66, 1, 16.9, 3, '2021-02-09'),
(101, 22, 27, 70, 4, 95.6, 3, '2021-02-09'),
(102, 23, 28, 55, 1, 49.5, 1, '2021-02-09'),
(103, 23, 28, 55, 1, 49.5, 0, '2021-02-09'),
(104, 22, 27, 55, 74, 3663, 1, '2021-02-10'),
(105, 22, 27, 17, 99, 574.2, 3, '2021-02-10'),
(106, 22, 27, 55, 1, 49.5, 0, '2021-02-10'),
(107, 1, 4, 10, 1, 9.9, 1, '2021-02-10'),
(108, 1, 3, 17, 1, 5.8, 1, '2021-02-10'),
(109, 1, 3, 55, 1, 49.5, 1, '2021-02-10'),
(110, 1, 2, 78, 1, 48, 1, '2021-02-10'),
(111, 1, 2, 64, 1, 8.2, 1, '2021-02-10'),
(112, 1, 4, 55, 1, 49.5, 1, '2021-02-10'),
(113, 1, 5, 63, 1, 18, 1, '2021-02-10'),
(114, 24, 29, 17, 1, 5.8, 3, '2021-02-10'),
(115, 24, 29, 35, 1, 11, 1, '2021-02-10');

-- --------------------------------------------------------

--
-- Struttura della tabella `ordinecupon`
--

CREATE TABLE `ordinecupon` (
  `CodiceCupon` varchar(30) NOT NULL,
  `IdUtente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotto`
--

CREATE TABLE `prodotto` (
  `Id` int(11) NOT NULL,
  `Titolo` varchar(100) NOT NULL,
  `Sottotitolo` varchar(50) NOT NULL,
  `Provenienza` varchar(30) NOT NULL,
  `Descrizione` text NOT NULL,
  `Prezzo` float NOT NULL,
  `QuantitaDisponibile` int(11) NOT NULL,
  `Immagine` varchar(100) NOT NULL,
  `Colore` text NOT NULL,
  `Profumo` text NOT NULL,
  `Sapore` text NOT NULL,
  `Vitigno` text NOT NULL,
  `Gradazione` text NOT NULL,
  `Temperatura` text NOT NULL,
  `Caratteristiche` text NOT NULL,
  `DataInserimento` date NOT NULL,
  `IdProvenienza` int(11) NOT NULL,
  `IdAnnata` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `prodotto`
--

INSERT INTO `prodotto` (`Id`, `Titolo`, `Sottotitolo`, `Provenienza`, `Descrizione`, `Prezzo`, `QuantitaDisponibile`, `Immagine`, `Colore`, `Profumo`, `Sapore`, `Vitigno`, `Gradazione`, `Temperatura`, `Caratteristiche`, `DataInserimento`, `IdProvenienza`, `IdAnnata`) VALUES
(0, 'Spumante Blanc de Blancs Cuvee Brut 2020', 'TINTORETTO', 'Italia, Emilia Romagna', '', 6.2, 64, 'SpumanteBlancdeBlancsCuveeBrut.png', 'Alla vista si presenta giallo paglierino', 'All\'olfatto si esprime con sentori molto fini di pane e frutta esotica.', 'Al gusto è fresco, sapido e dal perlage delicato.', '', '11', 'Freddo', 'Prosecco/Charmat, Leggero', '2021-02-04', 2, 8),
(2, 'Romagna DOC Spumante Bole Brut 2020', 'BOLE', 'Italia, Emilia Romagna', 'Rifermentazione lenta in autoclave ad una temperatura che va da 15 a 17 gradi C per una durata di 90 gg.', 9.9, 20, 'RomagnaDOCSpumanteBoleBrut.png', 'Giallo paglierino con sfumature dorate, perlage fine e persistente di buona spuma.', 'Delicato e aromatico al naso, con note di fiori bianchi e gelsomino, sambuco e agrumi.', 'Al sorso coinvolgente e piacevole, ben equilibrato in freschezza e acidità, richiama facilmente l’assaggio successivo. Si propone con sentori di mela verde, pera, buccia di limone.', '95% Trebbiano 5% Famoso', '11.5', 'Freddo', 'Prosecco/Charmat', '2021-01-31', 2, 8),
(3, 'Romagna DOC Sangiovese Iove 2019', 'UMBERTO CESARI', 'Italia, Emilia Romagna', '', 13.6, 8, 'RomagnaDOCSangioveseLove2019.png', 'Alla vista si presenta rosso intenso.', 'All\'olfatto esprime un bouquet floreale e fruttato con sentori di viola.', 'Al gusto è secco, caldo, con un tannino giovane, piacevole ed elegante.', '100% Sangiovese', '12.5', 'Ambiente', 'Leggero, Fruttato', '2021-01-31', 2, 7),
(5, 'Lambrusco di Modena DOC Rosso Frizzante Amabile 2020', 'CA\' DE\' MONACI', 'Italia, Emilia Romagna', '', 5.7, 100, 'LambruscoDiModenaDOCRossoFrizzanteAmabile.png', 'Vino rosso frizzante dal colore rosso rubino vivo, con lievi riflessi purpurei.', 'Caratterizzato da profumi floreali, con spiccate note di viola.', 'Dal sapore delicato di frutta matura, con finale leggermente aromatico e amabile.', 'Lambrusco Grasparossa, Salamino, Sorbara, Marani, Maestri, Montericco e Oliva.', '8', 'Fresco', 'Leggero', '2021-02-02', 2, 8),
(6, 'Sangiovese Colli di Faenza DOC L\'ho 2016', 'LA PISTONA', 'Italia, Emilia Romagna', 'La fermentazione avviene in vinificatore rotativo orizzontale, senza quindi operazioni di rimontaggio meccanico del mosto, poi la massa passa in serbatoi inox ove rimane e si affina per 12 mesi. Seguono l\'imbottigliamento e ulteriore affinamento in bottiglia per almeno 6 mesi.', 14, 76, 'SangioveseColliDiFaenzaDOCLHO2016.png\r\n', 'Colore rosso intenso.', 'Profumi freschi e fruttati con note speziate.', 'Di buona struttura, armonico e persistente, con un piacevole retrogusto amarognolo.', 'Sangiovese', '13', 'Ambiente', '', '2021-02-02', 2, 4),
(8, 'Lambrusco Di Sorbara Frizzante Secco DOC 2020', 'PALTRINIERI', 'Italia, Emilia Romagna', 'Questo ottimo Lambrusco di Sorbara dopo la rifermentazione naturale in bottiglia con lieviti indigeni viene fatto affinare per 6 mesi.', 13.9, 22, 'LambruscoDiSorbaraFrizzanteSeccoDOC.png', 'Alla vista appare rosa salmone con perlage fine.', 'Al naso si percepiscono note floreali di viola e fruttate di frutti di bosco. Verso il finale si percepiscono melograno e pompelmo.', 'Al gusto è secco e morbido, dalla spiccata acidità che invita ad un altro sorso.', 'Lambrusco di Sorbara', '11', 'Fresco', 'Fruttato, Secco, Vino Naturale', '2021-02-02', 2, 8),
(9, 'Sangiovese da Appassimento Romagna DOC Uve Portate a Cesena 2018', 'LEONARDO DA VINCI', 'Italia, Emilia Romagna', 'Le uve di Sangiovese destinate all\'appassimento vengono raccolte manualmente in cassette intorno a metà settembre, dopo un\'attenta selezione e verifica qualitativa. Segue la messa a riposo in fruttai a temperatura e umidità controllata, che permette l\'appassimento dell\'uva per un periodo di almeno sessanta giorni. La vinificazione, dopo lieve pressatura delle uve, viene condotta con una fermentazione a temperatura controllata a 25 gradi C in presenza di bucce; vengono effettuati i relativi rimontaggi e le follature che permettono la dissoluzione del colore e lo sviluppo dei componenti aromatici. Successivamente si effettua la svinatura per separare le bucce dal vino mosto. L\'affinamento prevede la fermentazione malolattica per rendere il vino al palato più rotondo e morbido; successivamente una parte del vino viene destinato alle barrique per un periodo minimo di nove mesi.', 15.9, 78, 'SangioveseDaAppassimentoRomagnaDOCUvePortateACesena2018.png', 'Rosso intenso dalle sfumature rubino.', 'Al naso appare fruttato con delicate sfumature vanigliate e sul finale emergono note speziate e di tabacco.', 'L\'acidità combinata alla morbidezza dei tannini arrotondati dall\'affinamento in legno ne esalta il frutto maturo.', '85% Sangiovese 15% Altre Uve a Bacca Nera', '', 'Ambiente', '', '2021-02-02', 2, 6),
(10, 'Lambrusco dell\'Emilia Frizzante Semisecco IGT Solco 2020', 'PALTRINIERI', 'Italia, Emilia Romagna', '', 9.9, 91, 'LambruscoEmiliaFrizzanteSemiseccoIGTSolco.png', 'Rosso violaceo scuro con perlage fine.', 'Al naso gli aromi sono fruttati e floreali.', 'Al gusto è lievemente abboccato, morbido e fine.', '100% Lambrusco Salamino', '11', 'Fresco', 'Prosecco/Charmat, Fruttato, Leggero, Morbido, Vellutato', '2021-02-03', 2, 8),
(11, 'Pignoletto DOC Frizzante 2020', 'ROMIO VINI D\'ITALIA', 'Italia, Emilia Romagna', '', 6.3, 54, 'PignolettoDOCFrizzante.png', 'Alla vista si presenta giallo paglierino con riflessi verdolini.', 'All\'olfatto è fresco e fruttato con una piacevole sfumatura di mela.', 'Al gusto è aromatico, delicato e morbido.', 'Grechetto Gentile 85% Altri vitigni 15%', '12', 'Freddo', 'Fruttato, Leggero', '2021-02-03', 2, 8),
(13, 'Lambrusco Di Sorbara Frizzante Secco DOC Piria 2020', 'PALTRINIERI', 'Italia, Emilia Romagna', '', 9.3, 98, 'LambruscoDiSorbaraFrizzanteSeccoDOCPiria.png', 'Rosso rubino delicato con effervescenza fine e persistente.', 'All\'olfatto è delicato e richiama aromi di viola e frutti di bosco.', 'Al palato è secco, fruttato come al naso e mediamente persistente.', '70% Lambrusco Sorbara 30% Lambrusco Salamino', '11.5', 'Fresco', 'Secco, Leggero, Prosecco/Charmat, Fruttato', '2021-02-03', 2, 8),
(14, 'Rosso del Conte BIO 2012', 'VIGNA CUNIAL', 'Italia, Emilia Romagna', '', 13.9, 51, 'RossoDelConteBIO2012.png', 'Rosso rubino.', 'Profumi di frutti rossi si sposano con le note erbacee del Cabernet.', 'Fresco ed equilibrato.', 'Cabernet Sauvignon, Merlot', '13.5', 'Ambiente', 'Biologico, Fruttato', '2021-02-03', 2, 1),
(15, 'Romagna DOC Sangiovese Superiore Ca\' Grande 2019', 'UMBERTO CESARI', 'Italia, Emilia Romagna', '', 13.9, 44, 'RomagnaDOCSangioveseSuperioreCaGrande2019.png', 'Rosso con riflessi violacei.', 'All\'olfatto è piacevolmente fruttato, floreale con sentori di rosa, viola e prugna.', 'Al gusto è secco, armonico, molto deciso ed elegante anche con il suo giovane tannino.', '100% Sangiovese', '12.5', 'Ambiente', 'Secco', '2021-02-03', 2, 7),
(16, 'Romagna DOC Sangiovese Superiore Riserva 2017', 'ROMIO VINI D\'ITALIA', 'Italia, Emilia Romagna', '', 8.5, 75, 'RomagnaDOCSangioveseSuperioreRiserva2017.png', 'Limpido, di un rosso rubino intenso, pienamente consistente al calice.', 'Al naso presenta evidenti aromi di amarena e spezie.', 'Secco, molto caldo e mediamente morbido al palato, con un tannino ben presente che gli conferisce corpo e struttura. Il gusto è caratterizzato da intense note di frutta rossa matura e piccoli frutti di bosco, con leggere sfumature di frutta secca e legno barricato. Il finale è lungo e piacevolmente persistente.', '100% Sangiovese', '13.5', 'Ambiente', 'Secco', '2021-02-03', 2, 5),
(17, 'Albana di Romagna DOCG Bianco Secco 2020', 'ROMIO VINI D\'ITALIA', 'Italia, Emilia Romagna', '', 5.8, 98, 'AlbanaDiRomagnaDOCGBiancoSecco2020.png', 'Giallo paglierino brillante, dai riflessi leggermente verdolini.', 'Al naso rivela un bouquet di profumi piacevolmente fruttati e floreali, che ricorda il miele, le spezie e i fiori bianchi.', 'Bianco secco, mediamente caldo e morbido, fresco e abbastanza sapido. Il gusto è ricco, avvolgente e schietto, caratterizzato da note fruttate e floreali con una leggera sfumatura minerale. Il finale è mediamente persistente.', '100% Albana', '12.5', 'Freddo', 'Morbido', '2021-02-10', 2, 8),
(19, 'Colli d\'Imola DOC Pignoletto frizzante Sillaro BIO 2020', 'GRUPPO MONTI SALUTE PIU\'', 'Italia, Emilia Romagna', '', 12.9, 28, 'ColliImolaDOCPignolettofrizzanteSillaroBIO.png', 'Colore giallo verdolino.', 'Fruttato, floreale e balsamico. Note di agrumi, frutti bianchi e fiori bianchi. Cedro, pesca bianca, biancospino e un delicato rabarbaro.', 'Elegante e gradevolmente persistente.', '100% pignoletto', '11.5°', 'Freddo', 'Biologico, Leggero', '2021-02-04', 2, 8),
(20, 'Colli Piacentini DOC Malvasia 2017', 'SANTA GIUSTINA', 'Italia, Emilia Romagna', '', 12.9, 90, 'ColliPiacentiniDOCMalvasia2017.png', 'Giallo paglierino intenso.', 'Al naso è intenso, complesso ed emergono note di mentuccia, salvia e frutti gialli.', 'Al palato è piacevolmente sapido, morbido, con un retrogusto che ricorda il rabarbaro. La persistenza e notevole e, nel complesso, è molto armonioso.', 'Malvasia aromatica di candia 100%', '12', 'Freddo', 'Leggero', '2021-02-04', 2, 5),
(21, 'Anricus 2017', 'SANTA GIUSTINA', 'Italia, Emilia Romagna', '', 11.5, 23, 'Anricus2017.png', 'Giallo paglierino.', 'Al naso è intenso, con note prevalenti di mela e pera e ricordi di lime e mughetto. Nel complesso il mix dei tre vitigni è ben equilibrato e armonico.', 'Fresco, beverino; i sentori percepiti al naso si ritrovano anche al palato ove sono ben amalgamate note floreali e fruttate. Il finale è persistente.', 'Ortrugo, Sauvignon Blanc, Malvasia', '12.5', 'Freddo', 'Fruttato', '2021-02-04', 2, 5),
(23, 'Chianti Classico DOCG Gran Selezione Gàudio 2015', 'CARUS VINI', 'Italia, Toscana', '', 26.9, 12, 'ChiantiClassicoDOCGGranSekezioneGaudio2015.png', 'Rosso rubino intenso con riflessi violacei.', 'Intenso, complesso, fine, fruttati e speziato con note di ciliegia matura e cacao.', 'Fresco e giustamente tannico con retrogusto di mora che esalta la struttura e la finezza.', 'E’ il prodotto delle migliori uve di 90% Sangiovese e un tocco di Cabernet Sauvignon.', '14', 'Ambiente', 'Secco', '2021-02-05', 8, 3),
(24, 'Maremma Toscana DOC Poggio alle Nane 2017\r\n', 'FATTORIA LE MORTELLE', 'Italia, Toscana', 'Poggio alle Nane nasce dall’unione delle migliori uve di Cabernet Franc, Cabernet Sauvignon ed una piccola quota di Carménère con il terroir unico dei vigneti di Poggio alle Nane, una delle aree più vocate dell’azienda e capace di produrre uve di straordinaria qualità. Un vino dalla grande complessità aromatica e dall’elegante fitta trama tannica.\r\n\r\n', 51, 80, 'MaremmaToscanaDOCPeggioAlleNane2017.png', 'Si presenta di un colore rosso profondo.', 'Al naso affascina per il carattere varietale, con sensazioni di frutti di bosco maturi che si uniscono a note di erbe di montagna, ribes nero e timo. A completare il bouquet sono i sentori balsamici e floreali di liquirizia e di violetta, ben in armonia con gli aromi speziati.', 'Al palato è ricco, elegante, potente, con piacevoli tannini setosi ed un finale persistente. Chiudono delle delicate note di cioccolato nero all’amarena e di tabacco.', 'Cabernet Franc, Cabernet Sauvignon, Carménère', '14', 'Ambiente', 'Corposo', '2021-02-05', 8, 5),
(25, 'Toscana IGT Rosso Poggio Mandorlo 2012', 'POGGIO MANDORLO', 'Italia, Toscana', 'Vino di punta dell’azienda, Poggiomandorlo è un ottimo esempio di espressione di un territorio utilizzando vitigni internazionali. Le uve Cabernet Franc e Merlot, fortemente caratterizzate dal terroir, danno un vino particolarmente adatto al lungo affinamento in legno. Poggiomandorlo è un vino che racchiude in sé la grande godibilità di un blend internazionale e la potenza ed il fascino di un territorio unico.', 36.5, 50, 'ToscanaIGTRossoPoggioMandorlo2012.png', 'Rosso porpora intenso con sfumature violacee.', 'Note olfattive fruttate di mirtillo, mora e cassis accompagnate da sfumature di vaniglia e cioccolato derivanti dall\'affinamento in legno.', 'In bocca la sensazione è di grande volume e struttura con trama tannica intensa e retrogusto lunghissimo arricchito di note balsamiche. La grande vena acida conferisce a questo vino una notevole freschezza ed un grande potenziale di evoluzione nel tempo.', 'Cabernet Franc, Merlot', '14.5', 'Ambiente', 'Secco, Corposo', '2021-02-05', 8, 1),
(26, 'Bolgheri DOC Il Bruciato 2019', 'ANTINORI', 'Italia, Toscana', 'Tenuta Guado al Tasso si trova nella piccola e prestigiosa DOC di Bolgheri, sulla costa dell’Alta Maremma, a un centinaio di chilometri a sud-ovest di Firenze. Questa denominazione ha una storia relativamente breve (nasce nel 1994) ma vanta di una fama internazionale come nuovo punto di riferimento nel panorama enologico mondiale. Il Bruciato nasce nel 2002, in una delle più difficili vendemmie di Tenuta Guado al Tasso, per raccontare e far conoscere secondo uno stile moderno il terroir unico di Bolgheri.', 20.9, 35, 'BolghieriDOCIlBruciato2019.png', 'Colore rosso rubino intenso.', 'Al naso è caratterizzato da note di piccoli frutti neri integri, spezie dolci e tabacco.', 'Al palato ha buona struttura, armonia e piacevolezza di beva. Nel retrogusto sono le note fresche e fruttate a predominare, donando al vino succulenza e dolcezza.', 'Cabernet Sauvignon, Merlot, Syrah, Cabernet Franc, Petit Verdot', '14.5', 'Ambiente', 'Secco, Fruttato', '2021-02-05', 8, 7),
(27, 'Brunello di Montalcino DOCG Casato Prime Donne 2015', 'DONATELLA CINELLI COLOMBINI', 'Italia, Toscana', 'Una grandissima annata che ha iniziato ad essere celebrata ancora prima di entrare nel mercato. L’andamento climatico mostra le tipiche caratteristiche dei migliori millesimi del Brunello con inverno e primavera piovosi che hanno idratato il terreno, luglio caldissimo che ha bloccato un sviluppo vegetativo iniziato molto velocemente e agosto con temporali alternati a giornate calde, settembre con fortissima escursione termica fra notte e giorno. Un copione che si ripete per tutte le grandi annate di Brunello e sembra dimostrare che il global warming è un’autentica fortuna per il vigneto di Montalcino perché moltiplica le annate cinque stelle e ne innalza il livello qualitativo fino a vertici mai raggiunti nel passato. Il 2015 verrà ricordato per il suo clima capriccioso che nel Sud della Toscana, risparmiò Montalcino, ma fece enormi danni tutto intorno. Montalcino sembrava avere una protezione divina e l’uva arrivò alla vendemmia in condizioni perfette.', 39.5, 95, 'BrunelloDiMontalcinoDOCGCasatoPrimeDonne2015.png', 'Rosso rubino di grande brillantezza, i movimenti lenti nel bicchiere evidenziano la ricchezza del vino.', 'Complesso, profondo, scuro, pulito e fine. Richiama i piccoli frutti rossi maturi la confettura e le spezie.', 'Intenso, armonico, elegante, tannini setosi e ottima struttura ben bilanciata dal frutto con effetto di pienezza appagante. Finale lungo e piacevole', '100% Sangiovese', '14', 'Ambiente', 'Secco', '2021-02-05', 8, 3),
(29, 'Orcia DOC Cenerentola 2016', 'DONATELLA CINELLI COLOMBINI', 'Italia, Toscana', '', 28.9, 28, 'OrciaDOCCenerentola2016.png', 'Rosso rubino intenso e brillante.', 'Di particolare finezza, profondità e complessità. I richiami ai piccoli frutti rossi si uniscono a aromi più speziati e esotici.', 'Di grande armonia e eleganza. La struttura tannica è vellutata e ben equilibrata dal frutto, promette una grande longevità. La piacevolezza gustativa permane a lungo in bocca.', '65% Sangiovese 35% Foglia Tonda', '14.5', 'Ambiente', 'Secco', '2021-02-05', 8, 4),
(31, 'Toscana Rosso IGT Tespero 2016', 'CARUS VINI', 'Italia, Toscana', 'Tespero è un vino voluttuoso in cui la potenza è ben bilanciata con l’intensità, l’eleganza con l’equilibrio.', 21.5, 57, 'ToscanaRossoIGTTespero2016.png', 'Rosso rubino intenso con riflessi di porpora.', 'Intenso e complesso; profumi di frutto di bosco, more e cassis, lamponi con note speziate e minerali.', 'Intenso, ricco ed avvolgente con tannini vellutati che riempiono il palato confermando al gusto le sensazioni dell’olfatto.', '100% Syrah', '15', 'Ambiente', 'Secco', '2021-02-06', 8, 4),
(32, 'Nobile di Montepulciano DOCG Riserva 2013', 'PALAZZO VECCHIO', 'Italia, Toscana', 'Questo vino, storico per l\'azienda, viene prodotto solo in annate selezionate ed ha una produzione limitata. Durante la produzione affronta un passaggio in botti di rovere francese di minimo 36 mesi, a cui segue un affinamento in bottiglia di almeno 6 mesi.', 32.5, 15, 'NobileDiMontepulcianoDOCGRiserva2013.png', 'Molto consistente e limpido, di un rosso rubino intenso dai riflessi granati.', 'All\'olfatto rivela un bouquet di profumi ampio, intenso e complesso, caratterizzato da note persistenti di piccoli frutti rossi, sottobosco e vaniglia.', 'Un rosso secco, molto caldo e morbido, ottimamente strutturato e dalla trama tannica fitta e setosa. Il gusto è intenso, avvolgente e deciso, definito da note di frutta rossa matura e piccoli frutti di bosco, accompagnati da una leggera sfumatura speziata. Il finale è elegante e persistente.', '85% Prugnolo Gentile, 10% Canaiolo Nero e 5% Mammolo', '14', 'Ambiente', 'Corposo', '2021-02-06', 8, 2),
(33, 'Toscana IGT Sangiovese Inusuale BIO 2018', 'INSERRATA ORGANIC WINES', 'Italia, Toscana', 'Quest\'annata è stata premiata con il premio International Organic Wine Award con un premio di 87 punti Silver nella categoria vini bianchi.', 16.5, 11, 'ToscanaIGTSangioveseInusualeBIO2018.png', 'Vino bianco di media consistenza, color giallo paglierino intenso.', 'Al naso presenta un bouquet mediamente fine e persistente, caratterizzato da note citriche di limone e agrumi accompagnate da piacevoli sentori di freschi fiori gialli.', 'Al palato è secco, abbastanza caldo e morbido, con un ottimo equilibrio tra acidità e sapidità. Il gusto è pieno e ricco di sfumature, che variano dalla frutta gialla a lievi note erbacee e speziate. Finale teso e brioso, di media persistenza.', '100% Sangiovese', '12', 'Fresco', 'Biologico, Leggero, Vino Naturale', '2021-02-06', 8, 6),
(34, 'Chianti Classico DOCG 2019', 'CASTELLARE DI CASTELLINA', 'Italia, Toscana', 'Il vero Chianti Classico. Solo vitigni autoctoni, senza Cabernet né Merlot, per mantenere la formula più tipica del più famoso dei vini italiani.', 16.9, 46, 'ChiantiClassicoDOCG2019.png', 'Colore rosso rubino.', 'Al naso è fragrante, fresco con piacevoli sentori di frutta rossa, di liquirizia, ribes e un leggero tocco di vaniglia. Dominano le note tipiche del Sangiovese chiantigiano.', 'Al palato esprime una piacevole dolcezza, è rotondo, morbido e sapido. Acidità e persistenza caratterizzano un retro gusto molto elegante.', '90% Sangiovese 10% Canaiolo', '13.5', 'Ambiente', 'Secco, Morbido, Fruttato', '2021-02-06', 8, 7),
(35, 'Vermentino Maremma Toscana DOC 2019', 'SASSOREGALE', 'Italia, Toscana', 'L’uva è raccolta a piena maturazione tecnologica e aromatica allo scopo di esaltarne il potenziale di profumi e struttura. La vinificazione in bianco ha inizio con la pressatura soffice delle uve e prosegue con la fermentazione in vasca di acciaio, dove il vino poi rimane sui lieviti per circa cinque mesi allo scopo di acquisire ulteriori sfumature gustative.', 11, 31, 'VermentinoMaremmaToscanaDOC2019.png', 'Alla vista il vino si presenta di colore giallo paglierino brillante.', 'Al naso presenta un quadro aromatico intenso ed elegante, dove alle note dominanti di agrumi e frutta a polpa bianca si affiancano piacevoli sentori di erbe mediterranee.', 'Al palato si distende dinamico e prestante al tempo stesso, grazie a una interessante struttura gustativa resa agile e ritmica da una venatura sapida.', '100% Vermentino', '13.5', 'Freddo', 'Leggero', '2021-02-06', 8, 7),
(37, 'Maremma Toscana DOC Bianco Vivia 2019', 'FATTORIA LE MORTELLE', 'Italia, Toscana', 'Fattoria Le Mortelle si trova nel cuore della Maremma Toscana, a pochi chilometri da Castiglione della Pescaia, in una posizione straordinaria ed affascinante sia per la natura che per la storia dei luoghi che la circondano. La famiglia Antinori è presente da sempre in queste terre. Qui il Vermentino e l’Ansonica, vitigni tipici della zona costiera toscana, si uniscono al Viognier, messo a dimora dopo anni di paziente sperimentazione, per un bianco elegante, testimone degli aromi e dei colori della costa della Maremma.', 14.5, 27, 'MaremmaToscanaDOCBiancoVivia2019.png', 'Colore giallo paglierino con riflessi verdolini brillanti.', 'Al naso è intenso e molto gradevole. Ai profumi di limone e mela verde si uniscono gli aromi di frutta matura, come pera e ananas, creando un complesso e delicato bouquet, completato da sensazioni olfattive di erbe aromatiche, come salvia e rosmarino.', 'Al palato il vino è fresco e cremoso con un finale minerale dal retrogusto agrumato di limone e buccia di cedro.', 'Vermentino, Viognier, Ansonica', '12', 'Freddo', 'Fruttato, Leggero', '2021-02-06', 8, 7),
(39, 'Colli Maceratesi Ribona DOC Bio 2018', 'FONTEZOPPA', 'Italia, Marche', '', 15.6, 33, 'ColliMaceratesiRibonaDOCBio2018.png', 'Il vino si presenta giallo paglierino intenso.', 'Decisa impronta olfattiva e complessa, con trame di anice, buccia di agrumi, pesca, fiori gialli, uniti da una profonda nota minerale.', 'Bocca di importante equilibrio e consistenza, dal finale piacevolmente lungo e fresco.', 'Maceratino', '', 'Freddo', 'Morbido', '2021-02-07', 9, 6),
(41, 'Rosso Piceno Superiore DOC Roggio del Filare 2016', 'VELENOSI', 'Italia, Marche', '', 35, 8, 'RossoPicenoSuperioreDOCRoggioDelFilare2016.png', 'Rosso rubino intenso, dai riflessi lievemente granati.', 'Al naso il bouquet varia da un\'intensa nota di fruttata di prugna e frutti di bosco, ad un sentore tipico di spezie, il che lo rende un vino estremamente intrigante già all\'olfatto.', 'Un vino estremamente asciutto, caldo, dal tannino ben percettibile ma mai invasivo, sorretto da una struttura possente. Il gusto è ricco e pieno, vellutato, con forti richiami ai piccoli frutti di bosco e alla frutta sotto spirito, accompagnati da una piacevole sfumatura speziata. Rosso corposo, distinto e carismatico, dalla persistenza infinita.', '70% Montepulciano e 30% Sangiovese', '14.5', 'Ambiente', 'Secco', '2021-02-06', 9, 4),
(42, 'Verdicchio Dei Castelli di Jesi Classico Superiore DOC Grancasale 2017', 'CASALFARNETO', 'Italia, Marche', '', 13.9, 25, 'VerdicchioDeiCastelliDiJesiClassicoSuperioreDOCGrancasale2017.png', 'Giallo paglierino con riflessi dorati.', 'Aromi persistenti agrumati, geranio e pesca bianca.', 'Ben strutturato, elegante, armonico, con freschi aromi fruttati e floreali in primo piano e sentori di lieviti.', 'Verdicchio', '', 'Freddo', 'Fruttato', '2021-02-06', 9, 5),
(43, 'Marche IGT Passerina Jajà Bio 2020', 'FONTEZOPPA', 'Italia, Marche', '', 8.9, 67, 'MarcheIGTPasserinaJajaBio2020.png', 'Giallo paglierino cristallino.', 'Naso fine e delicato con sentori di fiori bianchi insieme a fruttato di banana, ananas ed agrumi.', 'In bocca morbido e fresco, dalla delicata nota minerale, molto equilibrato e stuzzicante, con i richiami floreali e fruttati che invogliano a riassaggiarlo per le sensazioni di freschezza e delicata sapidità.', 'Passerina', '', 'Freddo', 'Leggero, Fruttato', '2021-02-06', 9, 8),
(44, 'Pecorino Offida DOCG Villa Angela 2019', 'VELENOSI', 'Italia, Marche', '', 17, 45, 'PecorinoOffidaDOCGVillaAngela2019.png', 'Di un color giallo paglierino intenso, dai riflessi lievemente dorati.', 'Al naso rivela un bouquet di profumi piacevolmente fruttati e floreali, che ricordato la frutta matura a polpa gialla e fresche erbe aromatiche.', 'Un bianco secco, mediamente caldo e dalla buona morbidezza, estremamente fresco e con una nota sapida sul finale ben percettibile. Il gusto è un tripudio di frutta gialla matura, melone bianco e fiori di campo primaverili. Di buona persistenza, rimane fine ed elegante lungo tutto l\'assaggio.', '100% Pecorino', '13', 'Freddo', 'Fruttato', '2021-02-07', 9, 7),
(45, 'Marche IGT La Capinera 2019', 'AZIENDA AGRARIA FRATELLI CAPINERA', 'Italia, Marche', '', 12.3, 16, 'MarcheIGTLaCapinera2019.png', 'Colore giallo paglierino.', 'All\'olfatto richiama sentori di fiori d\'acacia, mela matura, frutti esotici.', 'Al gusto è fresco, sapido, con ottima rispondenza gusto-olfattiva.', 'Chardonnay, Sauvignon Blanc e piccole quantità di Pecorino e Fiano', '13', 'Freddo', 'Fruttato', '2021-02-07', 9, 7),
(46, 'Ribona Colli Maceratesi DOC 2019', 'AZIENDA AGRARIA FRATELLI CAPINERA', 'Italia, Marche', '', 11.9, 22, 'RibonaColliMaceratesiDOC2019.png', 'Alla vista appare giallo tenute con riflessi verdolini.', 'All\'olfatto si esprime con nnote floreali di rosa, pesca gialla, fiori di acacia, mela ed agrumi.', 'Al gusto è secco, con una vena fresca e piacevolmente sapida, di buona intensità e persistenza.', 'Maceratino', '13', 'Freddo', 'Fruttato', '2021-02-07', 9, 7),
(47, 'Verdicchio Dei Castelli di Jesi Classico DOC Spumante Primo 2020', 'CASALFARNETO', 'Italia, Marche', '', 7.9, 57, 'VerdicchioDeiCastelliDiJesiClassicoDOCSpumantePrimo.png', 'Giallo tenue con riflessi verdognoli, perlage fine e persistente.', 'Profumo di fiori e frutta bianca.', 'In bocca delicato fresco ed armonico.', 'Verdicchio', '', 'Freddo', 'Prosecco/Charmat', '2021-02-07', 9, 8),
(48, 'Rosso Piceno DOC 2019 Bio', 'VELENOSI', 'Italia, Marche', '', 14.9, 72, 'RossoPicenoDOC2019Bio.png', 'Rosso rubino brillante, di buona consistenza al calice.', 'All\'olfatto rivela un bouquet ampio e mediamente fine, caratterizzato da note di frutta rossa matura e freschi sentori floreali.', 'Un rosso secco, caldo e morbido, ben strutturato e dalla buona acidità, con una leggera sfumatura sapida sul finale. Il gusto è ricco e fruttato, caratterizzato da sentori di prugna, fragola e piccoli frutti di bosco. Il finale è piacevolmente persistente.', '70% Montepulciano e 30% Sangiovese', '13.5', 'Ambiente', 'Biologico, Corposo', '2021-02-07', 9, 7),
(49, 'Falerio Pecorino DOC Joco Bio 2019', 'FONTEZOPPA', 'Italia, Marche', '', 9.7, 46, 'FalerioPecorinoDOCJocoBio2019.png', 'Si presenta giallo paglierino, luminoso.', 'Al naso è fine con sentori floreali uniti ad un complesso fruttato di ananas, pompelmo e pesca.', 'In bocca fresco, equilibrato, per un finale piacevolmente persistente.', 'Pecorino', '', 'Freddo', 'Fruttato', '2021-02-07', 9, 7),
(50, 'Prosecco Rosé DOC Torresella 2019', 'SANTA MARGHERITA', 'Italia, Veneto', 'Il Prosecco DOC Rosé Torresella nasce dal blend di glera e pinot nero allevati secondo le più moderne tecniche viticole. Le varietà vengono vinificate separatamente: il glera, vinificato in bianco, fermenta per 7/8 gg alla temperatura di 16 °C, mentre il pinot nero viene fermentato in rosso per 6/7 gg. La cuvée viene assemblata poco prima della presa di spuma e la percentuale di pinot nero può variare dal 10 al 15% a seconda dell’annata. La seconda fermentazione e la sosta sui lieviti conseguente, richiedono almeno 60 gg. al fine di ottenere un perlage fine e persistente e un colore più stabile nel tempo.', 9.7, 39, 'ProseccoRoseDOCTorresella2019.png', 'Alla vista si presenta di un colore rosato tenue.', 'Fragrante bouquet di fiori, con note di agrumi e di piccoli frutti a bacca rossa.', 'L’eleganza dei profumi trova esaltazione al palato nella freschezza gustativa, tanto vibrante quanto avvolgente. Dal fluttuante gioco delle bollicine che ne esaltano la vocazione alla leggerezza e alla bevibilità, a un lungo e piacevole finale dai delicati sentori aromatici.', 'Glera, Pinot Nero', '11.5', 'Fresco', 'Vellutato, Fruttato, Prosecco/Charmat', '2021-02-07', 6, 7),
(51, 'Gran Cuvée Blanc de Blancs Extra Dry 2020', '900 WINE', 'Italia, Veneto', 'Cuvée Blanc de Blanc, ottenuto da un’accurata selezione delle migliori uve della zona maggiormente vocata della provincia di Treviso.', 7.8, 15, 'GranCurveeBlancDeBlancsExtraDry.png', 'Colore giallo paglierino.', 'Al naso, profumi intensi e fruttati immediatamente raggiunti da una inebriante sensazione minerale.', 'Al gusto si percepisce amabile, con sentori di fiori bianchi e fiori di acacia, morbido e pieno bilanciato da una piacevole nota acidula.', '70% Chardonnay 30% Glera', '12', 'Freddo', 'Prosecco/Charmat', '2021-02-08', 6, 8),
(52, 'Spumante Metodo Charmat Rosé Brut Paradise 2020', 'VALDO', 'Italia, Veneto', 'Paradise Rosè Brut è un prodotto intrigante, giovane, di tendenza, dal colore accattivante e dalla spiccata personalità. Il design è interamente realizzato a mano, dalla designer Newyorkese Ceci, con la tecnica dell\'acquarello, digitalizzata tramite specifici scanner e poi applicata alla bottiglia.', 9.9, 74, 'SpumanteMetodoCharmatRoseBrutParadise.png', 'Alla vista il vino si presenta color petalo di rosa con pennellate vermiglio.', 'Bouquet fine ed elegante, dalle note floreali, con presenza consistente di lampone.', 'Al palato il fine perlage di bollicine rotonde risulta solleticante, il sapore è morbido e caldo, l’aroma fruttato delizia il turbinio di sensazioni che questo eccellente Rosè riesce ad esprimere.', 'Nerello Mascalese dalla Sicilia e altri vitigni a bacca bianca.', '12', 'Fresco', 'Morbido, Fruttato, Prosecco/Charmat', '2021-02-08', 6, 8),
(53, 'Sauvignon IGT 2019', 'CASA DEFRÀ', 'Italia, Veneto', 'Oggi Casa Defrà firma la migliore selezione delle uve coltivate sulle colline limitrofe a Vicenza, i Colli Berici, da generazioni di vignaioli legati alle loro terre da una grande passione. Una tradizione che si rinnova in ogni vendemmia con la stessa cura di sempre.', 6.6, 61, 'SauvignonIGT2019.png', 'Alla vista si presenta giallo paglierino con sfumature verdoline.', 'All\'olfatto esprime tutti gli aromi varietali tipici del Sauvignon.', 'Al palato è fresco, aromatico e dal finale molto lungo.', 'Sauvignon', '12', 'Freddo', 'Secco', '2021-02-08', 6, 7),
(54, 'Amarone della Valpolicella DOCG 2016', 'LA COLLINA DEI CILIEGI', 'Italia, Veneto', '', 41, 63, 'AmaroneDellaValpolicellaDocG2016.png', 'Di un colore rosso granato intenso e denso.', 'Al naso si presenta con note di ciliegia in confettura e spezie dolci.', 'Al palato ha un attacco deciso, un eccellente volume a metà bocca ed un finale a tratti balsamico e persistente.', 'Corvina Veronese, Corvinone e Rondinella', '15', 'Ambiente', 'Corposo', '2021-02-08', 6, 4),
(55, 'Amarone della Valpolicella Classico DOCG 2013', 'RUBINELLI VAJOL', 'Italia, Veneto', 'La cantina Rubinelli Vajol solo negli ultimi quattro anni ha ricevuto oltre 85 riconoscimenti internazionali, tra i quali: 94 punti J. Suckling, tra i migliori vini rossi d\'Italia per la guida l\'Espresso, 92 punti Luca Maroni.', 49.5, 73, 'AmaroneDellaValpolicellaClassicoDOCG2013.png', 'Rosso purpureo con intensi riflessi brillanti di rubino.', 'Ciliegia, amarena, mirtillo, sottobosco, fiori e buccia d’arancia. Un’esplosione di profumi che si fonde con eleganti spezie, erbe aromatiche, note di tabacco, sontuosa cioccolata fondente, caffè, liquirizia e pepe.', 'Avvolgente, equilibrato, paradisiaco, regala al palato un’immediata sensazione calda e vellutata. Corpo pieno e sorso solido, sostenuti da una vena fresca e tannini eleganti. Ritornano di continuo il frutto integro e la spezia in polifonici accordi. Rotondo, invitante, lungo e profondo. Perfettamente armonico in tutte le componenti gustative. Un vino magistralmente solenne, che accarezza i sensi.', '40% Corvina, 40% Corvinone, 10% Rondinella, 5% Molinara, 5% Oseleta', '16', 'Ambiente', '', '2021-02-10', 6, 2),
(56, 'Valpolicella Ripasso Classico Superiore DOC 2017', 'LUCIANO ARDUINI', 'Italia, Veneto', '', 20.3, 19, 'ValpolicellaRipassoClassicoSuperioreDOC2017.png', 'Alla vista si presenta di colore rosso rubino con leggere sfumature violacee.', 'Al naso è fresco, floreale e caratterizzato da note di frutti di bosco.', 'In bocca risulta armonico ed asciutto.', '60% Corvina, 20% Corvinone, 20% Rondinella', '14.5', 'Ambiente', 'Corposo', '2021-02-08', 6, 5),
(57, 'Recioto della Valpolicella Classico DOCG 2015', 'RUBINELLI VAJOL', 'Italia, Veneto', 'Il recioto del Vajol è un vino di grande potenza evocativa. Un nettare senza tempo. La cantina RUBINELLI VAJOL ha ricevuto 85 riconoscimenti internazionali per i suoi vini negli ultimi 4 anni.', 35.9, 42, 'ReciotoDellaValpolicellaClassicoDOCG2015.png', 'Rosso rubino molto intenso e compatto.', 'Ricche note di frutta, in particolare fichi e ciliegie sotto spirito.', 'In bocca è morbido, caldo e avvolgente. Nonostante la notevole potenza e struttura, non risulta mai invadente. Sul finale la sua lunga persistenza crea un piacevole contrasto con un\' inaspettata freschezza.', '15% Rondinella, 5% Molinara, 5% Oseleta', '15', 'Ambiente', 'Dolce', '2021-02-08', 6, 3),
(58, 'Veneto Rosso IGT Bradisismo 2016', 'INAMA WINE', 'Italia, Veneto', '', 26.9, 64, 'VenetoRossoIGTBradisismo2016.png', 'Colore rosso cupo', 'Naso intenso di piccole bacche scure, spezie, pepe, ciliege passite e vaniglia.', 'Al palato morbido, rotondo, di aroma profondo, senza impedimenti acidi e tannici.', '70% Cabernet Sauvignon 30% Carmenere', '14', 'Ambiente', 'Morbido, Corposo', '2021-02-08', 6, 4),
(59, 'Amarone della Valpolicella DOCG Il Ciliegio 2015', 'LA COLLINA DEI CILIEGI', 'Italia, Veneto', '', 75, 129, 'AmaroneDellaValpolicellaDOCGIlCiliegio2015.png', 'Di un rosso granato fitto e denso, consistenza da grande vino.', 'Naso esplosivo di frutta in confettura, spezie e sul finale note di tabacco.', 'In bocca è intenso e corposo, il finale risulta balsamico con un\'ottima persistenza.', 'Corvina Veronese, Corvinone e Rondinella', '15.5', 'Ambiente', 'Corposo', '2021-02-08', 6, 3),
(60, 'Veneto IGT Stilwhite 2018', 'SANTA MARGHERITA', 'Italia, Veneto', '', 14.9, 71, 'VenetoIGTSilwhite2018.png', 'Alla vista appare giallo brillante con riflessi cangianti dal verde al paglierino.', 'Presenta un luminoso ventaglio di profumi che varia da agrumi e frutta a polpa bianca e tropicale a quelli di erbe aromatiche e di fiori di campo con nuance di spezie piccanti.', 'Al palato si distende preciso e plastico, con la freschezza vibrante e la succulente sapidità che lo rendono gustoso al sorso e, grazie a un finale di rara persistenza aromatica, goloso per il riassaggio.', 'Chardonnay, Pinot Bianco, Sauvignon, Tai', '13', 'Freddo', 'Fruttato', '2021-02-08', 6, 6),
(61, 'Amarone della Valpolicella DOCG Valpantena 2017', 'BERTANI', 'Italia, Veneto', 'Appassimento nei fruttai della cantina di Grezzana. Fermentazione con lunga macerazione sulle bucce per estrarre le note fruttate. Affinamento in barrique di rovere di Slavonia per circa 24 mesi. Amarone moderno, equilibrato ed elegante.', 37.9, 42, 'AmaroneDellaValpolicellaDOCGValpantena2017.png', 'Rosso rubino.', 'Note fruttate di ciliegia e mora, suadenti note speziate tipiche della Valpantena.', 'Palato morbido, setoso, cremoso.', '80% Corvina 20% Rondinella', '15.5', 'Freddo', 'Corposo, Morbido', '2021-02-10', 6, 5),
(62, 'Prosecco DOC Treviso Extra Dry Alnè 2019', 'LA TORDERA', 'Italia, Veneto', 'Il termine alnè identifica la “fonte d’acqua”. E’ il luogo del primo insediamento delle popolazioni di Vidor, grazie alla presenza di una fonte d’acqua ai piedi delle colline.', 8.9, 40, 'ProseccoDOCTrevisoExtraDryAlne2019.png', 'Colore brillante, giallo paglierino tendente al verdognolo.', 'Profumo fruttato, sentori di mela verde, note di pera. Floreale con sentori di glicine e fiori di vaniglia, complesso e persistente.', 'Al gusto è un vino ben equilibrato e molto fresco, gli aromi di frutta sono esaltati da una buona vivacità. Persistente e con un fine retrogusto elegante.', 'Glera e Chardonnay', '11.5', 'Freddo', 'Prosecco/Charmat', '2021-02-08', 6, 7),
(63, 'Asti DOCG Collezione Speciale', 'MARTINI & ROSSI', 'Italia, Piemonte', '', 18, 4, 'AstiDOCGCollezioneSpeciale.png', 'Colore giallo paglierino.\n', 'All\'olfatto è fine, intensamente aromatico con sentori di erbe aromatiche, agrumi e pesca gialla.', 'Al gusto è dolce, fresco, armonico, ricco, con sensazioni retro olfattive fruttate.', '100% Moscato Bianco', 'Freddo', '', 'Prosecco/Charmat, Dolce', '2021-02-10', 4, 0),
(64, 'Piemonte DOC Rosato 4 Bucce 2020', 'DEZZANI', 'Italia, Piemonte', 'Piemonte DOC Rosato “4 Bucce” è il risultato di uve Dolcetto, Barbera, Freisa e Nebbiolo. Un vino nato per valorizzare 4 dei vini autoctoni piemontesi. Le uve vengono raccolte a inizio settembre e sottoposte ad una pressatura soffice volta ad ottenere un rosé elegante, dall’attraente colore buccia di cipolla. Alla vendemmia manuale, segue una vinificazione in rosato caratterizzata da una macerazione a temperatura controllata durante la quale le uve rimangono a contatto con il mosto per poche ore. Freschezza olfattiva e buona aciditá al palato lo rendono un rosato caratteristico, semplice e rinfrescante, per questo apprezzato a livello internazionale.', 8.2, 27, 'PiemonteDOCRosato4Bucce2020.png', 'Color buccia di cipolla limpido ed elegante.', 'Il bouquet è fine e complesso con note di fiori freschi e frutta rossa.', 'L’impatto in bocca è delicato, il gusto è ricco, fresco e persistente.', 'Dolcetto, Barbera, Nebbiolo e Freisa', '12', 'Freddo', 'Fruttato, Leggero', '2021-02-09', 4, 8),
(65, 'Cisterna d\'Asti DOC Superiore 2015', 'BRICCO DELLA CAPPELLETTA', 'Italia, Piemonte', 'La fermentazione malolattica di questo vino avviene in barriques di rovere francese.', 48, 64, 'CisternaAstiDOCSuperiore2015.png', 'Color rosso rubino carico e intenso, di consistenza sanguinea.', 'Al naso presenta un profilo olfattivo fine e mediamente complesso, caratterizzato da sentori di frutta a bacca rossa matura e fiori rossi, accompagnati da sfumature speziate e leggermente balsamiche, che ricordano il pepe nero, l\'eucalipto, il cuoio e la noce moscata.', 'Secco, caldo e morbido al palato, leggermente acido e sapido. Il gusto è rotondo e vellutato, ben definito da sentori fruttati e dai ritorni lievemente floreali. Il finale è lungo e di buona persistenza.', '100% uve Croatina selezionate', '15', 'Ambiente', 'Fruttato', '2021-02-09', 4, 3),
(66, 'Monferrato Rosso Nebbiolo DOC Nebula 2016', 'UVAMATRIS', 'Italia, Piemonte', '', 16.9, 46, 'MonferratoRossoNebbioloDOCNebula2016.png', 'Rosso Rubino con riflessi aranciati.', 'Al naso di avvertono inizialmente sentori floreali come violetta e rosa. Successivamente emergono eleganti sentori speziati e balsamici come menta ed eucalipto.', 'Al palato è molto affascinante. Possiede una grande finezza ed un tannino vellutato che rende il sorso pieno ed armonico. Il finale è caratteristico con un leggero retrogusto speziato che concorre a rendere il retrogusto lunghissimo e memorabile.', '100% Nebbiolo', '14', 'Ambiente', 'Secco', '2021-02-09', 4, 4),
(67, 'Piemonte DOC Rosso 8 Bucce 2018', 'DEZZANI', 'Italia, Piemonte', 'La scelta del nome “Otto Bucce” vuole sottolineare l’unicità di questo prodotto: otto infatti, sono i tipi di uva che confluiscono nel blend: Barbera, Dolcetto, Freisa, Bonarda e Albarossa, emblema del panorama enologico piemontese ed i vitigni internazionali Merlot, Syrah e Cabernet Sauvignon, che contribuiscono a conferire rotondità e struttura a questo vino dal carattere innovativo. Questo prodotto, infatti, è stato creato per promuovere la filosofia produttiva della Dezzani che da sempre oscilla tra tradizione ed innovazione, mirando ad un pubblico sempre più internazionale.', 8.9, 29, 'PiemonteDOCRosso8Bucce2018.png', 'Il colore è rosso rubino intenso con brillanti riflessi violacei.', 'Il profumo è pieno ed armonico. Si distinguono piacevoli e delicate note di frutti rossi, vaniglia e spezie.', 'In bocca è equilibrato ed elegante, con un piacevole gusto vinoso.', 'Dolcetto, Barbera, Merlot, Cabernet Sauvignon, Bonarda, Albarossa, Freisa, Syrah', '13', 'Ambiente', 'Fruttato', '2021-02-09', 4, 6),
(68, 'Grignolino d\'Asti DOC Lanfora 2017', 'MONTALBERA', 'Italia, Piemonte', '', 17.5, 15, 'GrignolinoAstiDOCLanfora2017.png', 'Rosso brillante, con le riflessi aranciati.', 'Al naso è piacevolmente intenso e si esprime con sentori di lampone e fiori esotici, note di pepe nero e curry.', 'Al palato presenta una vena acida ben equilibrata e una trama tannica vellutata. Persistente.', '100% Grignolino', '14', 'Ambiente', 'Fruttato', '2021-02-09', 4, 5),
(69, 'Spumante Brut Soldati La Scolca', 'LA SCOLCA', 'Italia, Piemonte', '', 18.9, 26, 'SpumanteBrutSoldatiLaScolca.png', 'Colore vivo e brillante, spuma fitta e morbida con perlage minuto e continuo.', 'All\'olfatto è intenso e persistente. La prima impressione è fruttata, poi si passa a delicati sentori fioriti, che sfumano in un complesso morbido ed elegante.', 'Al gusto è molto equilibrato, delicatamente acidulo, con gradevole sentore di mandorla.', '100% Cortese', '12.5', 'Freddo', 'Metodo Classico', '2021-02-09', 4, 0),
(70, 'Nizza DOCG 2016', 'IL BOTOLO', 'Italia, Piemonte', 'La produzione di questo vino inizia con la raccolta manuale e in cassetta delle uve selezionate, fermenta poi in vinificatori in acciaio inox e affina immediatamente dopo la svinatura, in botti grandi e in barrique di rovere francese di media tostatura per almeno 12 mesi.', 23.9, 38, 'NizzaDOCG2016.png', 'Alla vista il vino si presenta di colore rosso rubino intenso con riflessi granati.', 'Al naso è intenso e caratteristico, con note di frutta sciroppata, spezie e tabacco.', 'Al palato è secco, corposo, armonico e rotondo, con finale caldo, lungo ed avvolgente.', '100% Barbera', '15', 'Ambiente', 'Secco, Corposo', '2021-02-09', 4, 4),
(71, 'Barolo DOCG Serralunga Alba 2015', 'FONTANAFREDDA', 'Italia, Piemonte', '', 35, 91, 'BaroloDOCGSerralungaAlba2015.png', 'Rosso rubino con riflessi granati.', 'Netto ed intenso con sentori di vaniglia, spezie, rosa appassita e sottobosco.', 'Il sapore è asciutto, morbido, pieno, vellutato, armonico e persistente.', 'Nebbiolo', '13.5', 'Ambiente', 'Secco', '2021-02-09', 4, 3),
(72, 'Monferrato DOC Freisa Zi \'Tania 2016', 'MONTARIOLO', 'Italia, Piemonte', 'Un vino piacevolissimo. Buono adesso ma perfettamente adatto anche a un discreto invecchiamento.', 16, 55, 'MonferratoDOCFreisaZiTania2016.png', 'Rosso rubino intenso.', 'Al naso risulta intenso e persistente, con note floreali di rosa, frutti di bosco e sentori speziati.', 'In bocca è piacevolmente asciutto e di corpo, dal gusto sapido e piacevolmente tannico.', '100% Freisa', '13.5', 'Ambiente', 'Corposo', '2021-02-09', 4, 4),
(73, 'Gattinara DOCG 2016', 'TORRACCIA DEL PIANTAVIGNA', 'Italia, Piemonte', 'Molte bottiglie, se conservate in maniera ottimale, al riparo dalla luce e a 16°C, anche dopo 20 anni dalla data di vendemmia regalano sensazioni uniche.', 33.5, 44, 'GattinaraDOCG2016.png', 'Colore rosso rubino tendente all’aranciato.', 'Profumo fine e gradevole, ricorda la viola.', 'Sapore asciutto, armonico, con caratteristico finale amarognolo.', 'Nebbiolo', '14', 'Ambiente', 'Corposo', '2021-02-09', 4, 4),
(74, 'Terre Alfieri DOC Nebbiolo 2015', 'BRICCO DELLA CAPPELLETTA', 'Italia, Piemonte', 'Vino rosso che compie la fermentazione malolattica in botti di rovere francese. Successivamente, affronta un affinamento in barriques tradizionali per 18 mesi.', 48, 38, 'TorreAlfieriDOCNebbiolo2015.png', 'Limpido e consistente, di un color rosso granato.', 'Al naso rivela un profilo olfattivo elegante e complesso, caratterizzato da note fruttate e floreali accompagnate da sfumature balsamiche e leggermente speziate.', 'Un rosso secco, caldo e morbido al palato, dalla buona acidità e sapidità e definito nel gusto da intense note di frutta rossa matura, viola mammola e piccoli frutti di bosco. Il finale è lungo e piacevolmente persistente.', '100% Nebbiolo', '14.5', 'Ambiente', 'Secco', '2021-02-09', 4, 3),
(75, 'Langhe DOC Nebbiolo Ebbio 2019', 'FONTANAFREDDA', 'Italia, Piemonte', '', 17.9, 63, 'LangheDOCNebbioloEbbio2019.png', 'Bevuto più in là negli anni assume colore granata con accentuate tonalità aranciate.', 'All\'olfatto esprime profumi intensi di fiori appassiti, come la rosa e soprattutto il geranio.', 'Il sapore è caldo, morbido, pieno, vellutato e armonico, di buona persistenza.', '100% Nebbiolo', '113.5', 'Ambiente', 'Secco', '2021-02-09', 4, 7),
(76, 'Spumante Blanc de Blancs Soldati La Scolca', 'LA SCOLCA', 'Italia, Piemonte', '', 29.9, 44, 'SpumanteBlancDeBlancsSoldatiLsScolca.png', 'Alla vista è paglierino con riflessi verdolini.', 'All\'olfatto è essenzialmente fresco e giovane, in evoluzione con la presenza di ossigeno nel bicchiere.', 'Al gusto è delicatamente citrino, anche sapido, in assoluto un bicchiere fresco e accattivante che invita a rinnovare l’assegno. Ci si possono trovare le sfumature dal pompelmo al mandarino.', '100% Cortese', '12.5', 'Freddo', 'Metodo Classico', '2021-02-09', 4, 0),
(77, 'Gavi dei Gavi DOCG Etichetta Nera 2019', 'LA SCOLCA', 'Italia, Piemonte', '', 25.7, 25, 'GaviDeiGaviDOCGEtichettaNera2019.png', 'Paglierino chiaro, con delicati riflessi verdolini.', 'All\'olfatto è intenso, lungo, continuo, persistente, da fruttato a fiorito a seconda dello stato di evoluzione.', 'Al gusto è ampio, sapido, con sentori di pietra focaia e sfumature dolci di mandorla e noce nel finale.', '100% Cortese', '12', 'Freddo', 'Morbido', '2021-02-09', 4, 7),
(78, 'Barbera d\'Asti DOCG Superiore 2016', 'BRICCO DELLA CAPPELLETTA', 'Italia, Piemonte', 'La fermentazione malolattica di questo vino avviene in barriques di rovere francese. Il vino matura poi in barriques di rovere francese tradizionali per 18 mesi.', 48, 47, 'BarberaAstiDOCGSuperiore2016.png', 'Color rosso rubino intenso, dall\'ottima consistenza al calice.', 'All\'olfatto rivela un bouquet di profumi fine e complesso, caratterizzato da note di grande balsamicità, frutta rossa matura, con una leggera sfumatura di cacao.', 'In bocca è secco, caldo e morbido, dall\'ottima acidità e con una leggera nota sapida. Il gusto, ricco e avvolgente, è definito da note di frutta rossa matura e piccoli frutti di bosco, con piacevoli ritorni di viola mammola e rosa canina. Il finale è lungo e persistente.', '100% uve Barbera selezionate', '14.5', 'Ambiente', 'Fruttato', '2021-02-10', 4, 4);

-- --------------------------------------------------------

--
-- Struttura della tabella `provenienza`
--

CREATE TABLE `provenienza` (
  `Id` int(11) NOT NULL,
  `Provenienza` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `provenienza`
--

INSERT INTO `provenienza` (`Id`, `Provenienza`) VALUES
(2, 'Italia, Emilia Romagna'),
(4, 'Italia, Piemonte'),
(6, 'Italia, Veneto'),
(8, 'Italia, Toscana'),
(9, 'Italia, Marche');

-- --------------------------------------------------------

--
-- Struttura della tabella `utente`
--

CREATE TABLE `utente` (
  `Id` int(11) NOT NULL,
  `Nome` varchar(30) NOT NULL,
  `Cognome` varchar(30) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Password` varchar(8000) NOT NULL,
  `Data` date NOT NULL,
  `Admin` tinyint(1) NOT NULL,
  `RandomSalt` varchar(8000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `utente`
--

INSERT INTO `utente` (`Id`, `Nome`, `Cognome`, `Email`, `Password`, `Data`, `Admin`, `RandomSalt`) VALUES
(1, 'Alfonso', 'Castrazzozza', 'alfonsocastrazzozza@gmail.com', 'ciao', '1999-05-29', 1, ''),
(3, 'Giovannino', 'Testasiti', 'giovanninotestasiti@gmail.com', 'ciao', '1999-06-16', 0, ''),
(6, 'Eddie', 'Belloni', 'vissani@eddie.sofi', 'efs', '2003-02-06', 0, ''),
(9, 'Gianfelice', 'Spagnagatti', 'gianfelicespagnagatti@gmail.com', 'ciao', '1964-10-14', 0, ''),
(20, 'Pippo', 'Pippi', 'pippopippi@gmail.com', 'c784fb94123ff573780cb0ac9a7ba0dddceae32b37e26628c556f2e443005b60435067dc0ac5cceaaea7d262201ab7f2544d0c32e9fc84bd70bd9271d6fc539f', '0002-02-02', 0, '3b06933decab8ae43252d881cc1fca4fa383d6ed3b12df57f07b80e59ea70638f70a43d1be81c3bea37d7b9061a8bf03c86e58e0179dc175528d5ca7951390c0'),
(21, 'Prova', 'Provina', 'provaprovina@gmail.com', '8f364a3549e0ad401d63ebd36f843eceb4ccf41be01768cd5e735ae9c28c2e0f2a999b7f328ff430aeca0caf48318a5d5063470039a935954cfa4fd5b518d263', '1999-03-02', 0, '52dcc22d008778152b323211232c171f25f026dac68ee18e2ecd1e8cc9cc992ecbe5b247c401dff1c13c84b7cbf5b83ae17db86df42e931bc1d9ea863a51b1e5'),
(22, 'Enrico', 'Brunetti', 'enricobrunetti99@gmail.com', 'e5dfec405407987c0f4ee246fb1a3dac8ddcafb3ff85f8e1aae847290cc12e0f87d98b340833bb5e34c5607cf9fa9dbb5cf8bcc36ae7f34cdf0cb699171c175b', '1999-07-29', 0, '0cf91570b19880b65dfbb165e32c804e0dac6ca93e3a00d95ad80f9dbf28351a5de6bc6a1f259fc5981b7302598b8fd5f734858e1414aea53833f72910739fd6'),
(23, 'Mattia', 'Lorenzini', 'mattialorenzini68@gmail.com', 'f9c1913d5179f90ffde1c42b29e375d5f2d32f5967461bcbc741e32ac59510b4b88feef5a89edbfef981715506443f19e78badc91a8b0be246eaae3e58be7ab7', '1999-07-10', 1, 'ea9f03f75dc80f5ae43a54caf34d8a4559497506e7874c432790717d6aa27733bde5715bcbe65d3c5139383b15f4b6ba927d7f62c9618c4b3a0a3f11a4f7c8f0'),
(24, 'barbara', 'pirini casadei', 'barbarapirinicasadei@alice.it', '56d513a6062ea08f332722b84c80bebe790ff4d707f8ad7dd51a65e391f8e4655d24d1f6633cc3aab182230a87a15dab5fd93f6e405aaddd6033057c8235af54', '1969-07-17', 1, 'df75e92319ca158f874487e41d05297b0e4c3b1d54d19a3413732680beb2df96eddfdf680afee6daaa85bd0ac57dd4f48b594f7c938114898bd76702700a9709');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `annata`
--
ALTER TABLE `annata`
  ADD PRIMARY KEY (`Id`);

--
-- Indici per le tabelle `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`Id`);

--
-- Indici per le tabelle `cupon`
--
ALTER TABLE `cupon`
  ADD PRIMARY KEY (`Codice`);

--
-- Indici per le tabelle `indirizzo`
--
ALTER TABLE `indirizzo`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `appartenenza` (`IdUtente`);

--
-- Indici per le tabelle `newsletter`
--
ALTER TABLE `newsletter`
  ADD PRIMARY KEY (`Email`);

--
-- Indici per le tabelle `notifica`
--
ALTER TABLE `notifica`
  ADD PRIMARY KEY (`Id`);

--
-- Indici per le tabelle `ordine`
--
ALTER TABLE `ordine`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `composizione` (`IdProdotto`),
  ADD KEY `possessione` (`IdUtente`),
  ADD KEY `derivazione` (`IdIndirizzo`);

--
-- Indici per le tabelle `ordinecupon`
--
ALTER TABLE `ordinecupon`
  ADD PRIMARY KEY (`CodiceCupon`,`IdUtente`),
  ADD KEY `proviene` (`IdUtente`);

--
-- Indici per le tabelle `prodotto`
--
ALTER TABLE `prodotto`
  ADD PRIMARY KEY (`Id`);

--
-- Indici per le tabelle `provenienza`
--
ALTER TABLE `provenienza`
  ADD PRIMARY KEY (`Id`);

--
-- Indici per le tabelle `utente`
--
ALTER TABLE `utente`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Email` (`Email`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `annata`
--
ALTER TABLE `annata`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT per la tabella `categoria`
--
ALTER TABLE `categoria`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `indirizzo`
--
ALTER TABLE `indirizzo`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT per la tabella `notifica`
--
ALTER TABLE `notifica`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT per la tabella `ordine`
--
ALTER TABLE `ordine`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;

--
-- AUTO_INCREMENT per la tabella `prodotto`
--
ALTER TABLE `prodotto`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT per la tabella `provenienza`
--
ALTER TABLE `provenienza`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT per la tabella `utente`
--
ALTER TABLE `utente`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `indirizzo`
--
ALTER TABLE `indirizzo`
  ADD CONSTRAINT `appartenenza` FOREIGN KEY (`IdUtente`) REFERENCES `utente` (`Id`);

--
-- Limiti per la tabella `ordine`
--
ALTER TABLE `ordine`
  ADD CONSTRAINT `composizione` FOREIGN KEY (`IdProdotto`) REFERENCES `prodotto` (`Id`),
  ADD CONSTRAINT `derivazione` FOREIGN KEY (`IdIndirizzo`) REFERENCES `indirizzo` (`Id`),
  ADD CONSTRAINT `possessione` FOREIGN KEY (`IdUtente`) REFERENCES `utente` (`Id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
