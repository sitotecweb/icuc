<?php

require_once("./bootstrap.php");

$templateParams["titolo"] = "Gestione|Modifica Prodotto";
$templateParams["nome"] = "form-gestione-modifica-prodotto.php";

$templateParams["currentProducts"] = $dbh->getCurrentProducts();

$templateParams["js"] = array("js/jquery-3.4.1.min.js", "js/gestione-modifica-prodotto.js");

require("template/base.php");

?>