<?php

require_once("./bootstrap.php");

$templateParams["titolo"] = "Notifiche";

if(isUserLoggedIn()){
    $templateParams["nome"] = "notifiche.php";
}
else{
    $templateParams["nome"] = "contenuto-notifiche-senza-login.php";
}

require("template/base.php");

?>