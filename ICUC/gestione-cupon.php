<?php

require_once("./bootstrap.php");

$templateParams["titolo"] = "Gestione|Cupon";
$templateParams["nome"] = "form-gestione-cupon.php";

$templateParams["currentCupons"] = $dbh->getCupons();

$templateParams["js"] = array("js/jquery-3.4.1.min.js", "js/gestione-cupon.js");


require("template/base.php");

?>