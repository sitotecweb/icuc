<?php

function registerLoggedUser($user){
    $_SESSION["idUtente"] = $user["Id"];
    $_SESSION["nome"] = $user["Nome"];
    $_SESSION["cognome"] = $user["Cognome"];
    $_SESSION["email"] = $user["Email"];
    $_SESSION["data"] = $user["Data"];
    $_SESSION["admin"] = $user["Admin"];
}

function unlogUser(){
    unset($_SESSION["idUtente"]);
    unset($_SESSION["nome"]);
    unset($_SESSION["cognome"]);
    unset($_SESSION["email"]);
    unset($_SESSION["data"]);
    unset($_SESSION["admin"]);
}

function isUserLoggedIn(){
    return !empty($_SESSION["idUtente"]);
}

?>