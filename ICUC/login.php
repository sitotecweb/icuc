<?php

require_once("./bootstrap.php");

if(isset($_POST["email"]) && isset($_POST["p"])){
    $login_result = $dbh->checkLogin($_POST["email"], $_POST["p"]);
    if(isset($login_result["loginError"])){
        $templateParams["erroreLogin"] = "Credenziali errate!";
    }
    else{
        registerLoggedUser($login_result);
    }
}

if(isset($_POST["logout"]) && $_POST["logout"]==TRUE){
    unlogUser();
}

if(isUserLoggedIn()){
    $templateParams["indirizziSalvati"] = $dbh->getAddresses($_SESSION["idUtente"]);
    $templateParams["storicoOrdini"] = $dbh->getOrdersHistory($_SESSION["idUtente"]);
    $templateParams["titolo"] = "Benvenuto";
    $templateParams["nome"] = "login-utente.php";
}
else{
    $templateParams["titolo"] = "Login";
    $templateParams["nome"] = "login-form.php";
}

$templateParams["js"] = array("js/jquery-3.4.1.min.js", "js/sha512.js", "js/hash-generator.js");


require("template/base.php");

?>