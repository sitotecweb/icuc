<?php

require_once("./bootstrap.php");


$templateParams["titolo"] = "Prodotti";
$templateParams["nome"] = "prodotti-categorie.php";

$templateParams["prezzi"] = $dbh->getMinAndMaxPrices();
$templateParams["provenienze"] = $dbh->getProvenienze();
$templateParams["annate"] = $dbh->getAnnate();

$templateParams["js"] = array("js/jquery-3.4.1.min.js", "js/prodotti.js", "js/filter-accordion.js");


require("template/base.php");

?>