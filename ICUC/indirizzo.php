<?php

require_once("./bootstrap.php");


$templateParams["titolo"] = "Indirizzo";
$templateParams["nome"] = "selezione-indirizzo.php";

$templateParams["indirizzi"] = $dbh->getAddresses($_SESSION["idUtente"]);

if(isset($_POST["submit"])){
    if(!empty($_POST["indirizzo"])){
        $templateParams["indirizzoSelezionato"] = $dbh->addressSelected($_SESSION["idUtente"], $_POST["indirizzo"]);
        header("Location: ./pagamento.php");
    } else {
        $templateParams["nessunIndirizzoSelezionato"] = "Seleziona un indirizzo!";
    }
}

$templateParams["js"] = array("js/jquery-3.4.1.min.js", "js/nuovo-indirizzo.js");

require("template/base.php");

?>