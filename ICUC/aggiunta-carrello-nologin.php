<?php

require_once("./bootstrap.php");

$templateParams["titolo"] = "Carrello";

if(isUserLoggedIn()){
    $templateParams["nome"] = "prodotti.php";
}
else{
    $templateParams["nome"] = "aggiungi-carrello-senza-login.php";
}

require("template/base.php");

?>