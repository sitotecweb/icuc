<?php

require_once("./bootstrap.php");

if(isset($_GET["Id"])){
    $dbh->removeFromCart($_SESSION["idUtente"], $_GET["Id"]);
}

$templateParams["titolo"] = "Gestione|Negozio";
$templateParams["nome"] = "azioni-gestione-negozio.php";

$templateParams["js"] = array("js/jquery-3.4.1.min.js");


require("template/base.php");

?>