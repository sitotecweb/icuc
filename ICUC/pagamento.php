<?php

require_once("./bootstrap.php");


$templateParams["titolo"] = "Pagamento";
$templateParams["nome"] = "selezione-carta.php";

$templateParams["totaleCarrello"] = $dbh->orderAmount($_SESSION["idUtente"]);

if(isset($_POST["submit"])){
    if(!empty($_POST["nome"]) && !empty($_POST["cognome"]) && !empty($_POST["numeroCarta"]) && !empty($_POST["dataScadenza"]) && !empty($_POST["cvv"])){
        $risultato = $dbh->placeOrder($_SESSION["idUtente"], $_POST["sconto"]);
        if(isset($risultato["successo"])){
            $templateParams["titolo"] = "Pagamento effettuato";
            $templateParams["nome"] = "pagamento-effettuato.php";
        } else {
            $templateParams["titolo"] = "Pagamento rifiutato";
            $templateParams["nome"] = "pagamento-rifiutato.php";
        }
    } else {
        $templateParams["campiIncompleti"] = "Non sono stati compilati tutti i campi!";
    }
}

$templateParams["js"] = array("js/jquery-3.4.1.min.js", "js/utilizza-cupon.js");

require("template/base.php");

?>