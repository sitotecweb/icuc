<?php

require_once("./bootstrap.php");

$templateParams["titolo"] = "Registrazione";
$templateParams["nome"] = "registrazione-form.php";

if(isset($_POST["nome"]) && isset($_POST["cognome"]) && isset($_POST["email"]) && isset($_POST["p"]) && isset($_POST["dataNascita"]) && isset($_POST["indirizzo"]) && isset($_POST["citta"]) && isset($_POST["provincia"]) && isset($_POST["cap"])){
    $anno = date("Y", strtotime($_POST["dataNascita"]));
    $mese = date("m", strtotime($_POST["dataNascita"]));
    $giorno = date("d", strtotime($_POST["dataNascita"]));
    $annoAttuale = date("Y");
    $meseAttuale = date("m");
    $giornoAttuale = date("d");
    if((($annoAttuale - $anno) > 18) || (($annoAttuale == ($anno + 18)) && ($mese < $meseAttuale)) || (($annoAttuale == ($anno +18)) && ($meseAttuale == $mese) && ($giorno <= $giornoAttuale))) {
        $registration_result = $dbh->registerUser($_POST["nome"], $_POST["cognome"], $_POST["email"], $_POST["p"], $_POST["dataNascita"], $_POST["indirizzo"], $_POST["citta"], $_POST["provincia"], $_POST["cap"]);
        if(isset($registration_result["utenteRegistrato"])){
            $login_result = $dbh->checkLogin($_POST["email"], $_POST["p"]);
            if(isset($login_result["loginError"])){
                $templateParams["erroreLogin"] = "Credenziali errate!";
            }
            else{
                registerLoggedUser($login_result);
                header("Location: ./login.php");
            }
        }
    } else {
        $templateParams["erroreRegistrazione"] = "Per registrarsi a questo sito è necessario essere maggiorenni!";
    }
    
    
}

$templateParams["js"] = array("js/jquery-3.4.1.min.js", "js/sha512.js", "js/hash-generator.js");

require("template/base.php");

?>