function addedToCart(productData){
    let modal = `
        <div class="modal fade" id="addedToCartModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <p class="modal-title fs-5 fw-bold" id="exampleModalLabel">${productData["titolo"]}</p>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                ${productData["risultato"]}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Continua con gli acquisti</button>
                    <a href="carrello.php" class="btn btn-danger">Vai al carrello</a>
                </div>
                </div>
            </div>
        </div>
    `;
    return modal;
}

function addToCart(product, quantity){
    $.getJSON("api-aggiungi-al-carrello.php", { product: product, quantity: quantity }, function(data){
        let modal = addedToCart(data);
        $("#addedToCart").append(modal); 
        let myModal = new bootstrap.Modal(document.getElementById("addedToCartModal"), {});
        myModal.show();
    });    
}

let risolvi = 0;

$(document).ready(function(){
    
    $("#addToCartClick").click(function(){
        console.log("ciao");
        if(idSetted == 1){
            risolvi++;
            let product = productId;
            let quantity = $("#productQuantity").val();
            addToCart(product, quantity);
        } else {
            console.log("1");
            window.location = './aggiunta-carrello-nologin.php';
        }
    });
})