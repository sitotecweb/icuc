function readURL(input) {
    if (input.files && input.files[0]) {
        let reader = new FileReader();

        reader.onload = function (e) {
            $('#selImg').attr('src', e.target.result).width(300).height(400);
            
            $('#nomeFile').val(input.files[0].name);
           
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function showProductInfo(selectedProduct){
    $.getJSON("api-seleziona-prodotto.php", {selectedProduct: selectedProduct} , function(data){
        $("#title").val(data[0]["Titolo"]);
        $("#subTitle").val(data[0]["Sottotitolo"]);
        $("#provenience").val(data[0]["Provenienza"]);
        $("#description").val(data[0]["Descrizione"]);
        $("#prezzo").val(data[0]["Prezzo"]);
        $("#quantita").val(data[0]["QuantitaDisponibile"]);
        $("#selImg").attr("src","./upload/"+data[0]["Immagine"]);
        $("#nomeFile").val(data[0]["Immagine"]);
        $("#color").val(data[0]["Colore"]);
        $("#smell").val(data[0]["Profumo"]);
        $("#taste").val(data[0]["Sapore"]);
        $("#vite").val(data[0]["Vitigno"]);
        $("#alcholicGrade").val(data[0]["Gradazione"]);
        $("#temp").val(data[0]["Temperatura"]);
        $("#features").val(data[0]["Caratteristiche"]);
        
    });    
}

$(document).ready(function(){
    $("#productSelector").change(function(){
        let selectedProduct = $("#productSelector option:selected").val();

        if(selectedProduct != 0){
            $("#formRow").removeClass("invisible");
            showProductInfo(selectedProduct);
        }else{
            $("#formRow").addClass("invisible");
        }
    });
});

/**********Funzioni per il commit delle modifiche */
function visualizeChangeProductResult(operationResult){
    
    let modal = `
        <div class="modal fade" id="visualizeChangeProductResult" tabindex="-1" aria-labelledby="visualizeChangeProductResult" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <p class="modal-title fs-5 fw-bold text-dark" id="visualizeChangeProductResult">Risultato operazione:</p>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body text-dark">
                    ${operationResult["risultato"]}
                </div>
            </div>
        </div>
    `;

    return modal;
}

function changeProduct(title, subTitle, provenience, description, prezzo, quantita, nomeFile, color, smell, taste, vite, alcholicGrade, temp, features, oggi, idUtente){
    console.log("addProduct.");
    $.getJSON("api-modifica-prodotto.php", {title: title, subTitle:subTitle, provenience: provenience, description: description, prezzo: prezzo, quantita: quantita, nomeFile: nomeFile, color: color, smell: smell, taste: taste, vite: vite, alcholicGrade: alcholicGrade, temp: temp, features: features, oggi: oggi, idUtente: idUtente} , function(data){
        getSelectorProducts();
        let modal = visualizeChangeProductResult(data);
        $("#appendChangeProductModal").empty();
        $("#appendChangeProductModal").append(modal); 
        let mymodal = new bootstrap.Modal(document.getElementById("visualizeChangeProductResult"), {});
        mymodal.show();
    });    
}

$(document).ready(function(){
    $("#commitChanges").click(function(){
        let title = $("#title").val();
        console.log(title);
        let subTitle = $("#subTitle").val();
        console.log(subTitle);
        let provenience = $("#provenience").val();
        console.log(provenience);
        let description = $("#description").val();
        console.log(description);
        let prezzo = $("#prezzo").val();
        console.log(prezzo);
        let quantita = $("#quantita").val();
        console.log(quantita);
        let nomeFile = $("#nomeFile").val();
        console.log(nomeFile);
        let color = $("#color").val();
        console.log(color);
        let smell = $("#smell").val();
        console.log(smell);
        let taste = $("#taste").val();
        console.log(taste);
        let vite = $("#vite").val();
        console.log(vite);
        let alcholicGrade = $("#alcholicGrade").val();
        console.log(alcholicGrade);
        let temp = $("#temp").val();
        console.log(temp);
        let features = $("#features").val();
        console.log(features);

        let today = new Date();
        let dd = String(today.getDate()).padStart(2, '0');
        let mm = String(today.getMonth() + 1).padStart(2, '0');
        let yyyy = today.getFullYear();
        let oggi = yyyy + '/' + mm + '/' + dd;
        console.log(oggi);

        let idUtente = $("#productSelector option:selected").val();

        changeProduct(title, subTitle, provenience, description, prezzo, quantita, nomeFile, color, smell, taste, vite, alcholicGrade, temp, features, oggi, idUtente);
    });
})

/*************Funzione per rigenerare la tendina dopo ogni modifica */

function refreshSelectorProducts(productSelector, selectedProduct){
    let products = `<option value="0">CLICCA E SELEZIONA IL PRODOTTO DA MODIFICARE</option>`;
    for(let i=0; i < productSelector.length; i++){
        let product;
        if(productSelector[i]["Id"] == selectedProduct){
            product = `    
                <option value="${productSelector[i]["Id"]}" selected>${productSelector[i]["Titolo"]}</option>
            `;
        }else{
            product = `    
                <option value="${productSelector[i]["Id"]}">${productSelector[i]["Titolo"]}</option>
            `;
        }
        
        products += product;
    }

    return products;
}

function getSelectorProducts(){
    $.getJSON("api-selettore-modifica-prodotto.php", function(data){
        let selectedProduct = $("#productSelector option:selected").val();
        let productSelector = refreshSelectorProducts(data, selectedProduct);
        $(".productSelector").empty();
        $(".productSelector").append(productSelector);
        
    });   
}