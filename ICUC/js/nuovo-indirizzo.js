function getNewAddress(productData){
    let address = `
        <hr class="bg-secondary">

        <div class="d-flex justify-content-between radio">
            <input type="radio" class="mt-1" name="indirizzo" value="${productData["Id"]}">
            <p class="mb-0">${productData["Nome"]} ${productData["Cognome"]}, ${productData["ViaCivico"]}, ${productData["Citta"]}(${productData["Provincia"]}), ${productData["Cap"]}</p>
        </div>
    `;

    return address;
}

function addNewAddress(name, surname, address, city, state, zip){
    $.getJSON("api-nuovo-indirizzo.php", { name: name, surname: surname, address: address, city: city, state: state, zip: zip }, function(data){ 
        let newAddress = getNewAddress(data);
        $("#newAddressToAddHere").append(newAddress);
    });    
}

$(document).ready(function(){
    $("#newAddressClick").click(function(){
        let name = $("#inputName").val();
        let surname = $("#inputSurname").val();
        let address = $("#inputAddress").val();
        let city = $("#inputCity").val();
        let state = $("#inputState").val();
        let zip = $("#inputZip").val();
        addNewAddress(name, surname, address, city, state, zip);
    });
})