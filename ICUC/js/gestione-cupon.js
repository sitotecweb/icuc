/*******************************Funzioni per aggiunta nuovo Tag modale */

function visualizeNewCuponResult(operationResult){
    
    let modal = `
        <div class="modal fade" id="visualizeNewCuponResult" tabindex="-1" aria-labelledby="addCuponResultModal" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <p class="modal-title fs-5 fw-bold text-dark" id="addCuponResultModal">Risultato operazione:</p>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body text-dark">
                    ${operationResult["risultato"]}
                </div>
            </div>
        </div>
    `;

    return modal;
}

function addNewCuponCall(cupon, price){
    console.log("addCuponCall");
    $.getJSON("api-aggiungi-cupon.php", {cupon: cupon, price: price} , function(data){
        getSelectorCupons();
        let modal = visualizeNewCuponResult(data);
        $("#appendAddCuponModal").empty(); 
        $("#appendAddCuponModal").append(modal); 
        let mymodal = new bootstrap.Modal(document.getElementById("visualizeNewCuponResult"), {});
        mymodal.show();
    });    
}

$(document).ready(function(){
    $("#addNewCuponClick").click(function(){
        let cupon = $("#newCupon").val();
        let price = $('#newPrice').val();
        
        addNewCuponCall(cupon, price);
    });
})

/*********************************Funzioni per l'eliminazione di un cupon esistente */

function visualizeDeleteCuponResult(operationResult){
    
    let modal = `
        <div class="modal fade" id="visualizeDeleteCuponResult" tabindex="-1" aria-labelledby="visualizeDeleteCuponResult" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <p class="modal-title fs-5 fw-bold text-dark" id="visualizeDeleteCuponResult">Risultato operazione:</p>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body text-dark">
                    ${operationResult["risultato"]}
                </div>
            </div>
        </div>
    `;

    return modal;
}

function deleteCuponCall(selectedCupon){
    console.log("addCuponCall");
    $.getJSON("api-elimina-cupon.php", {selectedCupon: selectedCupon} , function(data){
        getSelectorCupons();
        let modal = visualizeDeleteCuponResult(data);
        $("#appendDeleteCuponModal").empty();
        $("#appendDeleteCuponModal").append(modal); 
        let mymodal = new bootstrap.Modal(document.getElementById("visualizeDeleteCuponResult"), {});
        mymodal.show();
    });    
}

$(document).ready(function(){
    $("#deleteCuponClick").click(function(){
        let selectedCupon = $("#selCupon option:selected").val();
        
        deleteCuponCall(selectedCupon);
    });
})


/*********************************Funzioni per la generazione a tendina dei cupon esistenti */
function refreshSelectorCupons(selectorCupons){
    let cupons = `<option value="0" selected>Clicca e seleziona il cupon da eliminare</option>`;
    for(let i=0; i < selectorCupons.length; i++){
        let cupon = `    
        <option value="${selectorCupons[i]["Codice"]}">Codice: ${selectorCupons[i]["Codice"]}, Sconto: ${selectorCupons[i]["Prezzo"]}%</option>
        `;
        cupons += cupon;
    }

    return cupons;
}

function getSelectorCupons(){
    $.getJSON("api-selettore-cupons.php", function(data){
        
        let selectorCupons = refreshSelectorCupons(data);
        $(".cuponSelector").empty();
        $(".cuponSelector").append(selectorCupons);
    });   
}

