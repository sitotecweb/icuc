function updatedQuantity(productData){
    let summary = `
        <p class="fs-4 text-uppercase mt-5">L'ordine ammonta a:</p>
        <div class="d-flex justify-content-between">
            <p>Totale parziale</p>
            <p>${productData["prezzoFinale"]}€</p>
        </div>
        <div class="d-flex justify-content-between">
            <p>Spedizione</p>
            <p>Gratis</p>
        </div>
        <hr class="bg-secondary mt-1">
        <div class="d-flex justify-content-between">
            <p class="fw-bold">Totale</p>
            <p class="fw-bold">${productData["prezzoFinale"]}€</p>
        </div>
    `;

    return summary;
}

function updateQuantity(product, quantity){
    $.getJSON("api-modifica-quantita.php", { product: product, quantity: quantity }, function(data){ 
        let summary = updatedQuantity(data);
        $("#updateCartId").append(summary);
    });    
}

$(document).ready(function(){
    $(".changeCartQuantity").change(function(){
        let productId = $(this).attr("id");
        let quantity = $(this).val();
        let maxQuantity = $("#"+productId+"MaxQuantita").val();
        console.log(quantity);
        console.log(maxQuantity);
        if(parseInt(quantity) <= parseInt(maxQuantity)){
            $("#updateCartId").empty();
            $("#"+productId+"Quantita").text(quantity);
            updateQuantity(productId, quantity);
        } else {
            $("#"+productId).val(maxQuantity);
        }
    });
})