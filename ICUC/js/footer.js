/*******************************Funzioni per iscrizione newsletter modale */

function visualizeSubscribeResult(operationResult){
    
    let modal = `
        <div class="modal fade" id="visualizeSubscribeResult" tabindex="-1" aria-labelledby="visualizeSubscribeResult" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <p class="modal-title fs-5 fw-bold text-dark" id="visualizeSubscribeResult">Risultato operazione:</p>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body text-dark">
                    ${operationResult["risultato"]}
                </div>
            </div>
        </div>
    `;

    return modal;
}

function subscribeCall(email){
    console.log("addCuponCall");
    $.getJSON("api-iscrizione-newsletter.php", {email: email} , function(data){
        let modal = visualizeSubscribeResult(data);
        $("#appendIscriviti").empty();
        $("#appendIscriviti").append(modal); 
        let mymodal = new bootstrap.Modal(document.getElementById("visualizeSubscribeResult"), {});
        mymodal.show();
    });    
}

$(document).ready(function(){
    $("#iscrivitiClick").click(function(){
        let email = $("#iscrivitiEmail").val();
        $("#iscrivitiEmail").val('');
        subscribeCall(email);
    });
})

/*******************************Funzioni per contattaci modale */

function visualizeContattaciResult(operationResult){
    
    let modal = `
        <div class="modal fade" id="visualizeContattaciResult" tabindex="-1" aria-labelledby="visualizeContattaciResult" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <p class="modal-title fs-5 fw-bold text-dark" id="visualizeContattaciResult">Risultato operazione:</p>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body text-dark">
                    ${operationResult["risultato"]}
                </div>
            </div>
        </div>
    `;

    return modal;
}

function contactsCall(nome, email, message){
    $.getJSON("api-contattaci.php", {nome: nome, email: email, message: message} , function(data){
        console.log("chiamata fatta");
        let modal = visualizeContattaciResult(data);
        $("#appendContattaci").empty();
        $("#appendContattaci").append(modal); 
        let mymodal = new bootstrap.Modal(document.getElementById("visualizeContattaciResult"), {});
        mymodal.show();
    });    
}

$(document).ready(function(){
    $("#contattaciClick").click(function(){
        let nome = $("#contattaciNome").val();
        console.log(nome);
        let email = $("#contattaciEmail").val();
        console.log(email);
        let message = $("#contattaciMessaggio").val();
        console.log(message);

        $("#contattaciNome").val('');
        $("#contattaciEmail").val('');
        $("#contattaciMessaggio").val('');
        console.log("Dati Raccolti");
        contactsCall(nome, email, message);
    });
})