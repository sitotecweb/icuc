function visualizeUseCuponResult(operationResult){
    
    let modal = `
        <div class="modal fade" id="visualizeUseCuponResult" tabindex="-1" aria-labelledby="visualizeUseCuponResult" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <p class="modal-title fs-5 fw-bold text-dark" id="visualizeUseCuponResult">Risultato operazione:</p>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body text-dark">
                    ${operationResult["risultato"]}
                </div>
            </div>
        </div>
    `;

    return modal;
}

function verifyCupon(cupon){
    $.getJSON("api-utilizza-cupon.php", { cupon: cupon }, function(data){
        console.log("chiamata fatta");
        let modal = visualizeUseCuponResult(data);
        $("#appendCuponResult").empty(); 
        $("#appendCuponResult").append(modal); 
        let mymodal = new bootstrap.Modal(document.getElementById("visualizeUseCuponResult"), {});
        if(data.risultato == "Il cupon è stato applicato e il totale finale è stato aggiornato."){
            let vecchioTotale = $("#totale").text();
            $("#sconto").text(data.sconto);
            console.log($("#sconto").text());
            let nuovoTotale = vecchioTotale-parseInt(parseInt(vecchioTotale)*parseInt(data.sconto)/100);
            $("#totale").text(nuovoTotale);
            console.log(vecchioTotale);
        }else{
            console.log("diverso");
        }
        mymodal.show();
    });   
}

$(document).ready(function(){
    $("#useCupon").click(function(e){
        let cupon = $("#nomeCupon").val();
       
        verifyCupon(cupon);
    });
})