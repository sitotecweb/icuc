function readURL(input) {
    if (input.files && input.files[0]) {
        let reader = new FileReader();

        reader.onload = function (e) {
            $('#selImg').attr('src', e.target.result).width(300).height(400);
            
            $('#nomeFile').val(input.files[0].name);
           
        };

        reader.readAsDataURL(input.files[0]);
    }
}


/* Funzioni per visualizza prodotti in modale*/
function visualizeProductsModal(datiProdotti){
    let tableHeader = `
    <div class="modal fade" id="showProductModal" tabindex="-1">
            <div class="modal-dialog modal-xl modal-dialog-scrollable">
                <div class="modal-content">
                    <div class="modal-header">
                        <p class="modal-title text-dark h1">Prodotti presenti in negozio:</p>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body overflow-auto">
                        <table class="table table-bordered border-dark table-light table-striped table-hover mt-1 text-start w-auto table-responsive" id="tableUncompleted">
                            <thead>
                            <tr>
                                <th scope="col">Titolo</th>
                                <th scope="col">Sottotitolo</th>
                                <th scope="col">Provenienza</th>
                                <th scope="col">Descrizione</th>
                                <th class="invisible" scope="col">jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj</th>
                                <th scope="col">Prezzo</th>
                                <th scope="col">Quantità Disponibile</th>
                                <th scope="col">Immagine</th>
                                <th scope="col">Colore</th>
                                <th scope="col">Profumo</th>
                                <th class="invisible" scope="col">jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj</th>
                                <th scope="col">Sapore</th>
                                <th class="invisible" scope="col">jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj</th>
                                <th scope="col">Vitigno</th>
                                <th scope="col">Gradazione Alcolica</th>
                                <th scope="col">Temperatura di Servizio</th>
                                <th scope="col">Caratteristiche</th>
                                <th scope="col">Data Inserimento</th>
                            </tr>
                            </thead>
                            <tbody>
    `;

    let products = "";
    for(let i=0; i < datiProdotti.length; i++){
        let product = `
                                <tr>
                                    <th scope="row">${datiProdotti[i]["Titolo"]}</th>
                                    <td>${datiProdotti[i]["Sottotitolo"]}</td>
                                    <td>${datiProdotti[i]["Provenienza"]}</td>
                                    <td colspan="2">${datiProdotti[i]["Descrizione"]}</td>
                                    <td>${datiProdotti[i]["Prezzo"]}€</td>
                                    <td>${datiProdotti[i]["QuantitaDisponibile"]}€</td>
                                    <td><img class="img-thumbnail"src="${datiProdotti[i]["Immagine"]}"></td>
                                    <td>${datiProdotti[i]["Colore"]}</td>
                                    <td colspan="2">${datiProdotti[i]["Profumo"]}</td>
                                    <td colspan="2">${datiProdotti[i]["Sapore"]}</td>
                                    <td>${datiProdotti[i]["Vitigno"]}</td>
                                    <td>${datiProdotti[i]["Gradazione"]}</td>
                                    <td>${datiProdotti[i]["Temperatura"]}</td>
                                    <td>${datiProdotti[i]["Caratteristiche"]}</td>
                                    <td>${datiProdotti[i]["DataInserimento"]}</td>
                                </tr>
        `;
        products += product;
    }
    

    let endTable = `
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    `;

    return tableHeader+products+endTable;
}

function triggerModal(){
    $.getJSON("api-visualizza-prodotti-alfabeticamente.php", function(data){
        let modal = visualizeProductsModal(data);
        $("#visualizeProductsModal").empty(); 
        $("#visualizeProductsModal").append(modal); 
        let mymodal = new bootstrap.Modal(document.getElementById("showProductModal"), {});
        mymodal.show();
    });    
}

$(document).ready(function(){
    $("#showProductsModalClick").click(function(){
        triggerModal();
    });
})


/*******************************Funzioni per aggiunta nuovo prodotto modale */

function visualizeAddProductResult(productData){
    
    let modal = `
        <div class="modal fade" id="visualizeAddProductResultModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <p class="modal-title fs-5 fw-bold text-dark" id="exampleModalLabel">Risultato operazione:</p>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body text-dark">
                    ${productData["risultato"]}
                </div>
            </div>
        </div>
    `;

    return modal;
}

function triggerModalAddProduct(title, subTitle, provenience, description, prezzo, quantita, nomeFile, color, smell, taste, vite, alcholicGrade, temp, features, oggi){
    console.log("addProduct.");
    $.getJSON("api-aggiungi-nuovo-prodotto.php", {title: title, subTitle:subTitle, provenience: provenience, description: description, prezzo: prezzo, quantita: quantita, nomeFile: nomeFile, color: color, smell: smell, taste: taste, vite: vite, alcholicGrade: alcholicGrade, temp: temp, features: features, oggi: oggi } , function(data){
        let modal = visualizeAddProductResult(data);
        $("#appendAddProductModal").empty(); 
        $("#appendAddProductModal").append(modal); 
        let mymodal = new bootstrap.Modal(document.getElementById("visualizeAddProductResultModal"), {});
        mymodal.show();
    });    
}

$(document).ready(function(){
    $("#addButton").click(function(){
        let title = $("#title").val();
        console.log(title);
        let subTitle = $("#subTitle").val();
        console.log(subTitle);
        let provenience = $("#provenience").val();
        console.log(provenience);
        let description = $("#description").val();
        console.log(description);
        let prezzo = $("#prezzo").val();
        console.log(prezzo);
        let quantita = $("#quantita").val();
        console.log(quantita);
        let nomeFile = $("#nomeFile").val();
        console.log(nomeFile);
        let color = $("#color").val();
        console.log(color);
        let smell = $("#smell").val();
        console.log(smell);
        let taste = $("#taste").val();
        console.log(taste);
        let vite = $("#vite").val();
        console.log(vite);
        let alcholicGrade = $("#alcholicGrade").val();
        console.log(alcholicGrade);
        let temp = $("#temp").val();
        console.log(temp);
        let features = $("#features").val();
        console.log(features);

        let today = new Date();
        let dd = String(today.getDate()).padStart(2, '0');
        let mm = String(today.getMonth() + 1).padStart(2, '0');
        let yyyy = today.getFullYear();
        let oggi = yyyy + '/' + mm + '/' + dd;
        console.log(oggi);

        triggerModalAddProduct(title, subTitle, provenience, description, prezzo, quantita, nomeFile, color, smell, taste, vite, alcholicGrade, temp, features, oggi);
    });
})