const STANDARD = 0;
const ALPHA = 1;
const PREZZO_ALTO = 2;
const PREZZO_BASSO = 3;
const NO_CATEGORY = -1;
const PROVENIENZA = 0;
const ANNATA = 1;
const NO_VALUE = 0;
const NO_SEARCH = -1;
const NO_MAX_PRICE = -1;
let actualType = STANDARD;
let actualCategory = NO_CATEGORY;
let actualValue = NO_VALUE;
let actualSearch = NO_SEARCH;
let actualMaxPrice = NO_MAX_PRICE;
let start = 0;
let increment = 6;
let check = 0;


function generaProdotti(datiProdotti){
    let prodotti = "";
    for(let i=0; i < datiProdotti.length; i++){
        let prodotto = `
        <div class="col">
            <a href="prodotto.php?Id=${datiProdotti[i]["Id"]}" class="card-vino text-decoration-none text-dark">
                <div class="card product-card shadow scale-on-click h-100">
                    <img src="${datiProdotti[i]["Immagine"]}" class="card-img-top mt-2" alt="">
                    <div class="card-body">
                        <div>
                            <p class="fs-5 fw-bold">${datiProdotti[i]["Titolo"]}</p>
                            <p class="fs-6 text-uppercase">${datiProdotti[i]["Sottotitolo"]}</p>      
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="d-flex flex-row justify-content-between">
                            <div>
                                <p class="fw-bold mt-1 mr-2">€ ${datiProdotti[i]["Prezzo"]}</p>
                            </div>
                            <div>
                                <button type="button" class="btn btn-outline-danger p-1 add-to-cart">Aggiungi al carrello</button>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        `;
        prodotti += prodotto;
    }

    return prodotti;
}

function getStepProduct(start, increment, type, category, value, search, maxPrice){
    check = 1;
    $.getJSON("api-prodotto.php", { start: start, increment: increment, type: type, category: category, value: value, search: search, maxPrice: maxPrice }, function(data){
        console.log(start + "  " + increment)
        console.log(data);
        let prodotti = generaProdotti(data);
        $(".elencoProdotti").append(prodotti);
        
        check = 0;
    });   
}

function priceRangeHelper(data){
    $(".selectedPrice").text("0 - " + data + " €");
}

$(document).ready(function(){
    getStepProduct(start, increment, STANDARD, NO_CATEGORY, NO_VALUE, NO_SEARCH, NO_MAX_PRICE);

    $(window).scroll(function() {
        let top_of_element = $("footer").offset().top;
        let bottom_of_element = $("footer").offset().top + $("footer").outerHeight();
        let bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();
        let top_of_screen = $(window).scrollTop();
    
        if ((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element) && check==0){
            start += increment;
            getStepProduct(start, increment, actualType, actualCategory, actualValue, actualSearch, actualMaxPrice);
        }
    });

    $(".alpha").click(function(e){
        e.preventDefault();
        start = 0;
        increment = 6;
        actualType = ALPHA;
        $(".elencoProdotti").empty();
        getStepProduct(start, increment, actualType, actualCategory, actualValue, actualSearch, actualMaxPrice);
    });
    $(".prezzoAlto").click(function(e){
        e.preventDefault();
        start = 0;
        increment = 6;
        actualType = PREZZO_ALTO;
        $(".elencoProdotti").empty();
        getStepProduct(start, increment, actualType, actualCategory, actualValue, actualSearch, actualMaxPrice);
    });
    $(".prezzoBasso").click(function(e){
        e.preventDefault();
        start = 0;
        increment = 6;
        actualType = PREZZO_BASSO;
        $(".elencoProdotti").empty();
        getStepProduct(start, increment, actualType, actualCategory, actualValue, actualSearch, actualMaxPrice);
    });
    $(".ricercaParola").click(function(e){
        start = 0;
        increment = 6;
        actualSearch = $(".parolaScelta").val();
        $(".elencoProdotti").empty();
        getStepProduct(start, increment, actualType, actualCategory, actualValue, actualSearch, actualMaxPrice);
    });
    $("#filtroPrezzoT").click(function(e){
        e.preventDefault();
        start = 0;
        increment = 6;
        actualMaxPrice = $("#maxPriceT").val();
        console.log(actualMaxPrice);
        $(".elencoProdotti").empty();
        getStepProduct(start, increment, actualType, actualCategory, actualValue, actualSearch, actualMaxPrice);
    });
    $("#filtroPrezzoD").click(function(e){
        e.preventDefault();
        start = 0;
        increment = 6;
        actualMaxPrice = $("#maxPriceD").val();
        console.log(actualMaxPrice);
        $(".elencoProdotti").empty();
        getStepProduct(start, increment, actualType, actualCategory, actualValue, actualSearch, actualMaxPrice);
    });
    $(".provenienza").click(function(e){
        e.preventDefault();
        start = 0;
        increment = 6;
        actualCategory = PROVENIENZA;
        actualValue = e.currentTarget.children[1].defaultValue;
        $(".elencoProdotti").empty();
        getStepProduct(start, increment, actualType, actualCategory, actualValue, actualSearch, actualMaxPrice);
    });
    $(".annata").click(function(e){
        e.preventDefault();
        start = 0;
        increment = 6;
        actualCategory = ANNATA;
        actualValue = e.currentTarget.children[1].defaultValue;
        $(".elencoProdotti").empty();
        getStepProduct(start, increment, actualType, actualCategory, actualValue, actualSearch, actualMaxPrice);
    });
    $(".rimuoviFiltri").click(function(e){
        e.preventDefault();
        start = 0;
        increment = 6;
        actualType = STANDARD;
        actualCategory = NO_CATEGORY;
        actualValue = NO_VALUE;
        actualSearch = NO_SEARCH;
        actualMaxPrice = NO_MAX_PRICE;
        $(".elencoProdotti").empty();
        getStepProduct(start, increment, actualType, actualCategory, actualValue, actualSearch, actualMaxPrice);
    });
    
})
