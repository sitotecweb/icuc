<?php
    function utf8ize( $mixed ) {
        if (is_array($mixed)) {
            foreach ($mixed as $key => $value) {
                $mixed[$key] = utf8ize($value);
            }
        } elseif (is_string($mixed)) {
            return mb_convert_encoding($mixed, "UTF-8", "UTF-8");
        }
        return $mixed;
    }

    require_once("bootstrap.php");

    $indirizzo = $dbh->newAddress($_SESSION["idUtente"], $_GET["name"], $_GET["surname"], $_GET["address"], $_GET["city"], $_GET["state"], $_GET["zip"]);
    
    header("Content-Type: application/json");
    echo json_encode(utf8ize($indirizzo));
?>