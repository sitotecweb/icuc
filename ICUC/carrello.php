<?php

require_once("./bootstrap.php");

$templateParams["titolo"] = "Carrello";

if(isUserLoggedIn()){
    $templateParams["nome"] = "contenuto-carrello.php";
    if(isset($_GET["Id"])){
        $dbh->removeFromCart($_SESSION["idUtente"], $_GET["Id"]);
    }
    
    $templateParams["contenutoCarrello"] = $dbh->compileCart($_SESSION["idUtente"]);
    $templateParams["totaleCarrello"] = $dbh->orderAmount($_SESSION["idUtente"]);
}
else{
    $templateParams["nome"] = "carrello-senza-login.php";
}

$templateParams["js"] = array("js/jquery-3.4.1.min.js", "js/aggiorna-quantita.js");

require("template/base.php");

?>