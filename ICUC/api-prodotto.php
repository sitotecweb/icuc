<?php
    function utf8ize( $mixed ) {
        if (is_array($mixed)) {
            foreach ($mixed as $key => $value) {
                $mixed[$key] = utf8ize($value);
            }
        } elseif (is_string($mixed)) {
            return mb_convert_encoding($mixed, "UTF-8", "UTF-8");
        }
        return $mixed;
    }

    require_once("bootstrap.php");

    $prodotti = $dbh->getProducts($_GET["start"], $_GET["increment"], $_GET["type"], $_GET["category"], $_GET["value"], $_GET["search"], $_GET["maxPrice"]);

    for($i = 0; $i < count($prodotti); $i++){
        $prodotti[$i]["Immagine"] = UPLOAD_DIR.$prodotti[$i]["Immagine"];
    }
    
    header("Content-Type: application/json");
    echo json_encode(utf8ize($prodotti));
?>