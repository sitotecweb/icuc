<?php

class DatabaseHelper{

    private $db;

    public function __construct($servername, $username, $password, $dbname, $port){
        $this->db = new mysqli($servername, $username, $password, $dbname, $port);
        $this->db->set_charset("utf8");
        if($this->db->connect_error){
            die("Connessione al db fallita;");
        }

    }

    public function getLatestProducts($n=12){
        $stmt = $this->db->prepare("SELECT Id, Titolo, Sottotitolo, Prezzo, Immagine FROM prodotto ORDER BY DataInserimento Desc LIMIT ?");
        $stmt->bind_param("i", $n);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEmilianoRomagnoli($n=12){
        $stmt = $this->db->prepare("SELECT Id, Titolo, Sottotitolo, Prezzo, Immagine FROM prodotto WHERE IdProvenienza = 2 ORDER BY DataInserimento Desc LIMIT ?");
        $stmt->bind_param("i", $n);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getAnnata2020($n=12){
        $stmt = $this->db->prepare("SELECT Id, Titolo, Sottotitolo, Prezzo, Immagine FROM prodotto WHERE IdAnnata = 8 ORDER BY DataInserimento Desc LIMIT ?");
        $stmt->bind_param("i", $n);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getProductById($id){
        $stmt = $this->db->prepare("SELECT * FROM prodotto WHERE Id = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function checkLogin($email, $password){
        $stmt = $this->db->prepare("SELECT Id, Nome, Cognome, Email, Admin, Password, RandomSalt, Data FROM utente WHERE Email = ? LIMIT 1");
        $stmt->bind_param('s', $email);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($user_id, $nome, $cognome, $email, $admin, $db_password, $salt, $data); 
        $stmt->fetch();
        $password = hash('sha512', $password.$salt); 
        if($stmt->num_rows == 1) {
            if($db_password == $password) { 
                $result['Id']=$user_id;
                $result['Nome']=$nome;
                $result['Cognome']=$cognome;
                $result['Email']=$email;
                $result['Data']=$data;
                $result['Admin']=$admin;
                return $result;
            }
        }
        $result['loginError']="Errore di login";

        return $result;       
    }

    public function getProvenienze(){
        $stmt = $this->db->prepare("SELECT * FROM provenienza");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getAnnate(){
        $stmt = $this->db->prepare("SELECT * FROM annata");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getMinAndMaxPrices(){
        $stmt = $this->db->prepare("SELECT MIN(Prezzo) AS PrezzoMin, MAX(Prezzo) AS PrezzoMax FROM prodotto");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getProducts($start, $increment, $type, $category, $value, $search, $maxPrice){
        $query = "SELECT * FROM prodotto ";

        if($maxPrice != -1){
            $query .= "WHERE Prezzo < ? ";
        }

        if($search != -1 && $maxPrice != -1){
            $query .= "AND (LOWER(Descrizione) LIKE LOWER('%".$search."%') OR LOWER(Titolo) LIKE LOWER('%".$search."%') OR LOWER(Sottotitolo) LIKE LOWER('%".$search."%')) ";
        } elseif($search != -1 && $maxPrice == -1){
            $query .= "WHERE LOWER(Descrizione) LIKE LOWER('%".$search."%') OR LOWER(Titolo) LIKE LOWER('%".$search."%') OR LOWER(Sottotitolo) LIKE LOWER('%".$search."%') ";
        } elseif($category == 0 && $maxPrice != -1){
            $query .= "AND IdProvenienza = ? ";
        } elseif($category == 0 && $maxPrice == -1){
            $query .= "WHERE IdProvenienza = ? ";
        } elseif($category == 1 && $maxPrice != -1){
            $query .= "AND IdAnnata = ? ";
        } elseif($category == 1 && $maxPrice == -1){
            $query .= "WHERE IdAnnata = ? ";
        }
        
        switch($type){
            case 0:
                $query .= "ORDER BY DataInserimento Desc LIMIT ?, ?";
                break;
            case 1:
                $query .= "ORDER BY Titolo LIMIT ?, ?";
                break;
            case 2:
                $query .= "ORDER BY Prezzo DESC LIMIT ?, ?";
                break;
            case 3:
                $query .= "ORDER BY Prezzo LIMIT ?, ?";
                break;
        }

        if($maxPrice != -1 && $category == -1){
            $stmt = $this->db->prepare($query);
            $stmt->bind_param("iii", $maxPrice, $start, $increment);
        } elseif($maxPrice != -1 && $category != -1){
            $stmt = $this->db->prepare($query);
            $stmt->bind_param("iiii", $maxPrice, $value, $start, $increment);
        } elseif($maxPrice == -1 && $category != -1){
            $stmt = $this->db->prepare($query);
            $stmt->bind_param("iii", $value, $start, $increment);
        } else {
            $stmt = $this->db->prepare($query);
            $stmt->bind_param("ii", $start, $increment);
        }
        
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getProductsAlphaOrdered($start, $increment, $type){
        $stmt = $this->db->prepare("SELECT * FROM prodotto ORDER BY Titolo LIMIT ?, ?");
        $stmt->bind_param("iii", $start, $increment);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function addToCart($userId, $productId, $quantity){
        $stmt1 = $this->db->prepare("SELECT Titolo, QuantitaDisponibile FROM prodotto WHERE Id = ?");
        $stmt1->bind_param("i", $productId);
        $stmt1->execute();
        $result1 = $stmt1->get_result();
        $value1 = $result1->fetch_all(MYSQLI_ASSOC);
        
        $quantitaDisponibile = $value1[0]["QuantitaDisponibile"];
        $titolo = $value1[0]["Titolo"];

        $risultato["titolo"] = $titolo;

        if($quantitaDisponibile == 0){
            $risultato["risultato"] = "Questo prodotto è esaurito";

            return $risultato;
        } elseif($quantity > $quantitaDisponibile){
            $risultato["risultato"] = "Spiacente, sono rimaste solamente " . $quantitaDisponibile . " bottiglie";

            return $risultato;
        }

        $stmt2 = $this->db->prepare("SELECT Id FROM indirizzo WHERE IdUtente = ? LIMIT 1");
        $stmt2->bind_param("i", $userId);
        $stmt2->execute();
        $result2 = $stmt2->get_result();
        $value2 = $result2->fetch_all(MYSQLI_ASSOC);

        $idIndirizzo = $value2[0]["Id"];

        $stmt3 = $this->db->prepare("SELECT Quantita FROM ordine WHERE IdUtente = ? AND IdProdotto = ? AND Stato = 0");
        $stmt3->bind_param("ii", $userId, $productId);
        $stmt3->execute();
        $result3 = $stmt3->get_result();
        $value3 = $result3->fetch_all(MYSQLI_ASSOC);

        $stmt4 = $this->db->prepare("SELECT Prezzo FROM prodotto WHERE Id = ?");
        $stmt4->bind_param("i", $productId);
        $stmt4->execute();
        $result4 = $stmt4->get_result();
        $value4 = $result4->fetch_all(MYSQLI_ASSOC);

        $prezzo = $value4[0]["Prezzo"];
        $prezzoFinale = $prezzo * $quantity;

        $data = date("Y-m-d");

        if(count($value3) == 0){
            $stmt5 = $this->db->prepare("INSERT INTO ordine (IdUtente, IdIndirizzo, IdProdotto, Quantita, PrezzoParziale, Stato, UltimoAggiornamento) VALUES (?, ?, ?, ?, ?, 0, ?)");
            $stmt5->bind_param("iiiids", $userId, $idIndirizzo, $productId, $quantity, $prezzoFinale, $data);
            $stmt5->execute();
        } else {
            $quantitaPrecedente = $value3[0]["Quantita"];
            $quantitaAttuale = $quantitaPrecedente + $quantity;
            $prezzoFinale = $prezzo * $quantitaAttuale;

            $stmt6 = $this->db->prepare("UPDATE ordine SET Quantita = ?, PrezzoParziale = ?, UltimoAggiornamento = ? WHERE IdUtente = ? AND IdProdotto = ? AND Stato = 0");
            $stmt6->bind_param("idsii", $quantitaAttuale, $prezzoFinale, $data, $userId, $productId);
            $stmt6->execute();
        } 

        $risultato["risultato"] = $quantity . " x " . $titolo ." aggiunto al carrello!";

        return $risultato;
    }

    public function compileCart($userId){
        $stmt = $this->db->prepare("SELECT prodotto.Id AS Id, prodotto.Titolo AS Titolo, prodotto.Sottotitolo AS Sottotitolo, prodotto.QuantitaDisponibile AS QuantitaDisponibile, prodotto.Immagine AS Immagine, prodotto.Prezzo AS Prezzo, ordine.Quantita AS Quantita
        FROM prodotto
        INNER JOIN ordine ON prodotto.Id = ordine.IdProdotto WHERE ordine.Stato = 0 AND ordine.IdUtente = ?");
        $stmt->bind_param("i", $userId);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);;
    }

    public function orderAmount($userId){
        $stmt = $this->db->prepare("SELECT PrezzoParziale FROM ordine WHERE IdUtente = ? AND Stato = 0");
        $stmt->bind_param("i", $userId);
        $stmt->execute();
        $result = $stmt->get_result();
        $value = $result->fetch_all(MYSQLI_ASSOC);

        $prezzoTotale = 0;
        foreach($value as $parziale){
            $prezzoTotale += $parziale["PrezzoParziale"];
        }

        return $prezzoTotale;
    }

    public function removeFromCart($userId, $productId){
        $stmt = $this->db->prepare("DELETE FROM ordine WHERE IdUtente = ? AND IdProdotto = ? AND Stato = 0");
        $stmt->bind_param("ii", $userId, $productId);
        return $stmt->execute();
    }

    public function updateCart($userId, $productId, $quantity){
        $stmt1 = $this->db->prepare("SELECT Prezzo FROM prodotto WHERE Id = ?");
        $stmt1->bind_param("i", $productId);
        $stmt1->execute();
        $result1 = $stmt1->get_result();
        $value1 = $result1->fetch_all(MYSQLI_ASSOC);

        $prezzo = $value1[0]["Prezzo"];
        $prezzoFinale = $prezzo * $quantity;

        $data = date("Y-m-d");

        $stmt2 = $this->db->prepare("UPDATE ordine SET Quantita = ?, PrezzoParziale = ?, UltimoAggiornamento = ? WHERE IdUtente = ? AND IdProdotto = ? AND Stato = 0");
        $stmt2->bind_param("idsii", $quantity, $prezzoFinale, $data, $userId, $productId);
        $stmt2->execute();

        $stmt3 = $this->db->prepare("SELECT PrezzoParziale FROM ordine WHERE IdUtente = ? AND Stato = 0");
        $stmt3->bind_param("i", $userId);
        $stmt3->execute();
        $result3 = $stmt3->get_result();
        $value3 = $result3->fetch_all(MYSQLI_ASSOC);

        $prezzoFinale = 0;
        foreach($value3 as $parziale){
            $prezzoFinale += $parziale["PrezzoParziale"];
        }

        $risultato["prezzoFinale"] = $prezzoFinale;

        return $risultato;
    }

    public function getAddresses($userId){
        $stmt = $this->db->prepare("SELECT * FROM indirizzo WHERE IdUtente = ?");
        $stmt->bind_param("i", $userId);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);;
    }

    public function newAddress($userId, $nome, $cognome, $viaCivico, $citta, $provincia, $cap){
        $stmt1 = $this->db->prepare("INSERT INTO indirizzo (IdUtente, ViaCivico, Citta, Provincia, Cap, Nome, Cognome) VALUES (?, ?, ?, ?, UPPER(?), ?, ?)");
        $stmt1->bind_param("issssss", $userId, $viaCivico, $citta, $provincia, $cap, $nome, $cognome);
        $stmt1->execute();

        $stmt2 = $this->db->prepare("SELECT Id FROM indirizzo WHERE IdUtente = ? ORDER BY Id DESC LIMIT 1");
        $stmt2->bind_param("i", $userId);
        $stmt2->execute();
        $result2 = $stmt2->get_result();
        $value2 = $result2->fetch_all(MYSQLI_ASSOC);

        $risultato["Id"] = $value2[0]["Id"];
        $risultato["Nome"] = $nome;
        $risultato["Cognome"] = $cognome;
        $risultato["ViaCivico"] = $viaCivico;
        $risultato["Citta"] = $citta;
        $risultato["Provincia"] = $provincia;
        $risultato["Cap"] = $cap;

        return $risultato;
    }

    public function addressSelected($userId, $addressId){
        $data = date("Y-m-d");

        $stmt = $this->db->prepare("UPDATE ordine SET IdIndirizzo = ?, UltimoAggiornamento = ? WHERE IdUtente = ? AND Stato = 0");
        $stmt->bind_param("isi", $addressId, $data, $userId);

        return $stmt->execute();
    }

    public function placeOrder($userId, $sconto){
        $stmt1 = $this->db->prepare("SELECT Id, IdProdotto, Quantita, PrezzoParziale FROM ordine WHERE IdUtente = ? AND Stato = 0");
        $stmt1->bind_param("i", $userId);
        $stmt1->execute();
        $result1 = $stmt1->get_result();
        $value1 = $result1->fetch_all(MYSQLI_ASSOC);

        foreach($value1 as $prodotto){
            $orderId = $prodotto["Id"];
            $productId = $prodotto["IdProdotto"];
            $quantity = $prodotto["Quantita"];
            $prezzoParziale = $prodotto["PrezzoParziale"];

            $stmt2 = $this->db->prepare("SELECT Titolo, QuantitaDisponibile FROM prodotto WHERE Id = ?");
            $stmt2->bind_param("i", $productId);
            $stmt2->execute();
            $result2 = $stmt2->get_result();
            $value2 = $result2->fetch_all(MYSQLI_ASSOC);

            $titolo = $value2[0]["Titolo"];
            $quantitaDisponibile = $value2[0]["QuantitaDisponibile"];
            if($quantity > $quantitaDisponibile){
                $risultato["errore"] = "pezzi insufficienti di" . $productId;
                return $risultato;
            } else{
                $quantitaAggiornata = $quantitaDisponibile - $quantity;

                $stmt3 = $this->db->prepare("UPDATE prodotto SET QuantitaDisponibile = ? WHERE Id = ?");
                $stmt3->bind_param("ii", $quantitaAggiornata, $productId);
                $stmt3->execute();

                $text = "#".$orderId.": l'ordine contenente ".$titolo." x ".$quantity." è stato ricevuto correttamente. Riceverai ulteriori notifiche quando sarà spedito.";
                $color = "warning";
                $time = date("Y-m-d H:i:s");

                $stmt5 = $this->db->prepare("INSERT INTO notifica (IdUtente, IdOrdine, Titolo, Testo, Colore, Timestamp, Admin) VALUES (?, ?, ?, ?, ?, ?, 0)");
                $stmt5->bind_param("iissss", $userId, $orderId, $titolo, $text, $color, $time);
                $stmt5->execute();

                $text = "#".$orderId.": l'utente #".$userId." ha ordinato ".$titolo." x ".$quantity.".";

                $stmt6 = $this->db->prepare("INSERT INTO notifica (IdUtente, IdOrdine, Titolo, Testo, Colore, Timestamp, Admin) VALUES (?, ?, ?, ?, ?, ?, 1)");
                $stmt6->bind_param("iissss", $userId, $orderId, $titolo, $text, $color, $time);
                $stmt6->execute();

                if($quantitaAggiornata == 0){
                    $titoloNotifica = "Prodotto esaurito";
                    $text = $titoloNotifica.": sono terminate le scorte per il prodotto ".$titolo.", è necessario il rifornimento.";
                    $color = "secondary";

                    $stmt6 = $this->db->prepare("INSERT INTO notifica (IdUtente, IdOrdine, Titolo, Testo, Colore, Timestamp, Admin) VALUES (?, ?, ?, ?, ?, ?, 2)");
                    $stmt6->bind_param("iissss", $userId, $orderId, $titoloNotifica, $text, $color, $time);
                    $stmt6->execute();
                }

                $data = date("Y-m-d");

                $nuovoParziale = $prezzoParziale;
                if(isset($sconto)){
                    $nuovoParziale = $prezzoParziale-($prezzoParziale*$sconto/100);
                }

                $stmt5 = $this->db->prepare("UPDATE ordine SET Stato = 1, UltimoAggiornamento = ?, PrezzoParziale = ? WHERE Id = ?");
                $stmt5->bind_param("sdi", $data,$nuovoParziale, $orderId);
                $stmt5->execute();
        
                $risultato["successo"] = "Ordine effettuato";

            }

        }
        $risultato["successo"] = "Ordine effettuato";
       

        return  $risultato;
    }

    public function registerUser($nome, $cognome, $email, $password, $data, $viaCivico, $citta, $provincia, $cap){        
        $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
        $password = hash('sha512', $password.$random_salt);
        
        $stmt1 = $this->db->prepare("INSERT INTO utente (Nome, Cognome, Email, Password, Data, Admin, RandomSalt) VALUES (?, ?, ?, ?, ?, 0, ?)");
        $stmt1->bind_param("ssssss", $nome, $cognome, $email, $password, $data, $random_salt);
        $stmt1->execute();

        $stmt2 = $this->db->prepare("SELECT Id FROM utente ORDER BY Id DESC LIMIT 1");
        $stmt2->execute();
        $result2 = $stmt2->get_result();
        $value2 = $result2->fetch_all(MYSQLI_ASSOC);

        $userId = $value2[0]["Id"];

        $stmt3 = $this->db->prepare("INSERT INTO indirizzo (IdUtente, ViaCivico, Citta, Provincia, Cap, Nome, Cognome) VALUES (?, ?, ?, ?, ?, ?, ?)");
        $stmt3->bind_param("issssss", $userId, $viaCivico, $citta, $provincia, $cap, $nome, $cognome);
        $stmt3->execute();

        $risultato["utenteRegistrato"] = $userId;

        return $risultato;
    }

    public function getOrdinationsUsers($userId){
        $stmt = $this->db->prepare("SELECT ordine.Id AS Id, notifica.Titolo AS Titolo FROM ordine, notifica WHERE ordine.IdUtente = ? AND ordine.Id = notifica.IdOrdine GROUP BY ordine.Id ORDER BY Id DESC");
        $stmt->bind_param("i", $userId);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getOrdinationsAdmins(){
        $stmt = $this->db->prepare("SELECT ordine.Id AS Id, notifica.Titolo AS Titolo FROM ordine, notifica WHERE ordine.Id = notifica.IdOrdine GROUP BY ordine.Id ORDER BY Id DESC");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getNotificationsUsers($orderId){
        $stmt = $this->db->prepare("SELECT * FROM notifica WHERE IdOrdine = ? AND Admin = 0 ORDER BY Timestamp DESC");
        $stmt->bind_param("i", $orderId);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getNotificationsAdmins($orderId){
        $stmt = $this->db->prepare("SELECT * FROM notifica WHERE IdOrdine = ? AND Admin = 1 ORDER BY Timestamp DESC");
        $stmt->bind_param("i", $orderId);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getActualProductsOrdered(){
        $stmt = $this->db->prepare("SELECT * FROM prodotto ORDER BY Titolo");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function addProductModal($titolo, $sottotitolo, $provenienza, $descrizione, $prezzo, $quantitaDisponibile, $immagine, $colore, $profumo, $sapore, $vitigno, $gradazione, $temperatura, $caratteristiche, $dataInserimento){
       
        $risultato["risultato"] = "";

        if($titolo != "" && $sottotitolo != "" && $provenienza != "" && $prezzo > 0 && $quantitaDisponibile > 0 && $immagine != "" && $colore != "" && $profumo != "" && $sapore != ""){
            $stmt = $this->db->prepare("INSERT INTO prodotto (Titolo, Sottotitolo, Provenienza, Descrizione, Prezzo, QuantitaDisponibile, Immagine, Colore, Profumo, Sapore, Vitigno, Gradazione, Temperatura, Caratteristiche, DataInserimento) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            $stmt->bind_param("ssssdisssssssss", $titolo, $sottotitolo, $provenienza, $descrizione, $prezzo, $quantitaDisponibile, $immagine, $colore, $profumo, $sapore, $vitigno, $gradazione, $temperatura, $caratteristiche, $dataInserimento);
            $stmt->execute();

            $risultato["risultato"] = "Prodotto inserito correttamente.";
     
        }else{
            $risultato["risultato"] = "Errore nell'inserimento dei dati. Controllare di aver inserito correttamente tutti i campi obbligatori(*) poi riprovare.";
        } 

        return $risultato;       
    }

    public function addNewTag($tag){

        $operationResult["risultato"] = "";

        if($tag == ""){
            $operationResult["risultato"] = "Errore: il campo Nuovo Tag non può essere lasciato vuoto.";
        } else{
            $stmt1 = $this->db->prepare("SELECT count(*) AS counter FROM tag WHERE Tag = ?");
            $stmt1->bind_param("s", $tag);
            $stmt1->execute();
            $result = $stmt1->get_result();
            $counts = $result->fetch_all(MYSQLI_ASSOC);

            if($counts[0]["counter"] == 1){
                $operationResult["risultato"] = "Errore: il nuovo tag inserito esiste già.";
            } else{
                $stmt = $this->db->prepare("INSERT INTO tag (Tag) VALUES (?)");
                $stmt->bind_param("s", $tag);
                $stmt->execute();

                $operationResult["risultato"] = "Il tag è stato inserito con successo.";
            }
        }
        
        return $operationResult;

    }

    public function addNewCupon($cupon, $price){

        $operationResult["risultato"] = "";

        if($cupon == "" || $price == ""){
            $operationResult["risultato"] = "Errore: i campi Nuovo Cupon e il numero percentuale di sconto non possono essere lasciati vuoti.";
        } else {
            $stmt1 = $this->db->prepare("SELECT count(*) AS counter FROM cupon WHERE Codice = ?");
            $stmt1->bind_param("s", $cupon);
            $stmt1->execute();
            $result = $stmt1->get_result();
            $counts = $result->fetch_all(MYSQLI_ASSOC);

            if($counts[0]["counter"] == 1){
                $operationResult["risultato"] = "Errore: Il codice cupon inserito esiste già, scriverne uno nuovo e riprovare.";
            } else {
                $stmt = $this->db->prepare("INSERT INTO cupon (Codice, Prezzo) VALUES (?,?)");
                $stmt->bind_param("sd", $cupon, $price);
                $stmt->execute();

                $operationResult["risultato"] = "Il Cupon è stato inserito con successo ed è ora utilizzabile.";
            }
        }
        
        return $operationResult;

    }

    public function deleteCupon($selectedCupon){

        $operationResult["risultato"] = "";

        if($selectedCupon == "0"){
            $operationResult["risultato"] = "Errore: selezionare un cupon da eliminare se presente.";
        }else{
            $stmt1 = $this->db->prepare("DELETE FROM cupon WHERE Codice = ?");
            $stmt1->bind_param("s", $selectedCupon);
            $stmt1->execute();
            
            $operationResult["risultato"] = "Il Cupon è stato eliminato con successo e da ora è inutilizzabile.";
            
        }
        
        return $operationResult;

    }

    public function getCupons(){
        $stmt = $this->db->prepare("SELECT Codice, Prezzo FROM cupon ORDER BY Prezzo Desc");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getCurrentProducts(){
        $stmt = $this->db->prepare("SELECT Id, Titolo FROM prodotto ORDER BY Titolo ASC");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function updateProductById($titolo, $sottotitolo, $provenienza, $descrizione, $prezzo, $quantitaDisponibile, $immagine, $colore, $profumo, $sapore, $vitigno, $gradazione, $temperatura, $caratteristiche, $dataInserimento, $idUtente){
       
        $risultato["risultato"] = "";

        $stmt1 = $this->db->prepare("SELECT notifica.Id AS Id, COUNT(*) AS Conto FROM notifica, ordine WHERE ordine.Id = notifica.IdOrdine AND ordine.IdProdotto = ? AND notifica.Admin = 2");
        $stmt1->bind_param("i", $idUtente);
        $stmt1->execute();
        $result1 = $stmt1->get_result();
        $value1 = $result1->fetch_all(MYSQLI_ASSOC);

        $notificaId = $value1[0]["Id"];
        $conto = $value1[0]["Conto"];

        if($titolo != "" && $sottotitolo != "" && $provenienza != "" && $prezzo > 0 && $quantitaDisponibile > 0 && $immagine != "" && $colore != "" && $profumo != "" && $sapore != ""){
            $stmt2 = $this->db->prepare("UPDATE prodotto SET Titolo = ?, Sottotitolo = ?, Provenienza = ?, Descrizione = ?, Prezzo = ?, QuantitaDisponibile = ?, Immagine = ?, Colore = ?, Profumo = ?, Sapore = ?, Vitigno = ?, Gradazione = ?, Temperatura = ?, Caratteristiche = ?, DataInserimento = ? WHERE Id = ?");
            $stmt2->bind_param("ssssdisssssssssi", $titolo, $sottotitolo, $provenienza, $descrizione, $prezzo, $quantitaDisponibile, $immagine, $colore, $profumo, $sapore, $vitigno, $gradazione, $temperatura, $caratteristiche, $dataInserimento, $idUtente);
            $stmt2->execute();

            if($conto == 1){
                $stmt3 = $this->db->prepare("UPDATE notifica SET Admin = 3 WHERE Id = ? AND Admin = 2");
                $stmt3->bind_param("i", $notificaId);
                $stmt3->execute();
            }

            $risultato["risultato"] = "Prodotto Modificato correttamente.";
     
        }else{
            $risultato["risultato"] = "Errore nell'inserimento dei dati. Controllare di aver inserito correttamente tutti i campi obbligatori(*) poi riprovare.";
        } 

        return $risultato;       
    }

    public function getIdTitleProducts(){
        $stmt = $this->db->prepare("SELECT Id, Titolo FROM prodotto ORDER BY Titolo ASC");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getOrdersFromState($stato){
        $stmt = $this->db->prepare("SELECT Id, IdUtente, IdIndirizzo, IdProdotto, Quantita, PrezzoParziale, Stato, UltimoAggiornamento FROM ordine WHERE Stato = ? ORDER BY UltimoAggiornamento DESC");
        $stmt->bind_param("i", $stato);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result = $result->fetch_all(MYSQLI_ASSOC);

    }

    public function getOutOfStockNotifications(){
        $stmt = $this->db->prepare("SELECT * FROM notifica WHERE Admin = 2");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function advanceOrderStatus($id, $state, $oggi){
        $newState = $state+1;

        $stmt1 = $this->db->prepare("UPDATE ordine SET Stato = ?, UltimoAggiornamento = ? WHERE Id = ?");
        $stmt1->bind_param("isi", $newState, $oggi, $id);
        $stmt1->execute();

        $stmt2 = $this->db->prepare("SELECT IdUtente FROM ordine WHERE Id = ?");
        $stmt2->bind_param("i", $id);
        $stmt2->execute();
        $result2 = $stmt2->get_result();
        $value2 = $result2->fetch_all(MYSQLI_ASSOC);

        $userId = $value2[0]["IdUtente"];

        $time = date("Y-m-d H:i:s");
        if($newState == 2){
            $titolo = "Ordine spedito.";
            $text = "#".$id.": l'ordine è stato spedito. Lo riceverai all'indirizzo indicato entro tre giorni lavorativi.";
            $color = "success";
            

            $stmt3 = $this->db->prepare("INSERT INTO notifica (IdUtente, IdOrdine, Titolo, Testo, Colore, Timestamp, Admin) VALUES (?, ?, ?, ?, ?, ?, 0)");
            $stmt3->bind_param("iissss", $userId, $id, $titolo, $text, $color, $time);
            $stmt3->execute();

            $text = "#".$id.": hai spedito quest'ordine.";

            $stmt4 = $this->db->prepare("INSERT INTO notifica (IdUtente, IdOrdine, Titolo, Testo, Colore, Timestamp, Admin) VALUES (?, ?, ?, ?, ?, ?, 1)");
            $stmt4->bind_param("iissss", $userId, $id, $titolo, $text, $color, $time);
            $stmt4->execute();
        } elseif($newState == 3){
            $titolo = "Ordine consegnato.";
            $text = "#".$id.": l'ordine è stato consegnato. Facci sapere com'è andata sui nostri canali social.";
            $color = "light";

            $stmt5 = $this->db->prepare("INSERT INTO notifica (IdUtente, IdOrdine, Titolo, Testo, Colore, Timestamp, Admin) VALUES (?, ?, ?, ?, ?, ?, 0)");
            $stmt5->bind_param("iissss", $userId, $id, $titolo, $text, $color, $time);
            $stmt5->execute();

            $text = "#".$id.": hai consegnato quest'ordine.";

            $stmt6 = $this->db->prepare("INSERT INTO notifica (IdUtente, IdOrdine, Titolo, Testo, Colore, Timestamp, Admin) VALUES (?, ?, ?, ?, ?, ?, 1)");
            $stmt6->bind_param("iissss", $userId, $id, $titolo, $text, $color, $time);
            $stmt6->execute();
        }

        $risultato["successo"] = "Operazione eseguita correttamente";
        return $risultato;
    }

    public function checkCupon($userId, $cupon){
        $operationResult["risultato"] = "";
        $operationResult["sconto"] = "";

        if($cupon == ""){
            $operationResult["risultato"] = "Il campo Cupon non può essere lasciato vuoto.";
        }else{
            
            $stmt2 = $this->db->prepare("SELECT COUNT(*) AS exist FROM cupon WHERE Codice = ?");
            $stmt2->bind_param("s", $cupon);
            $stmt2->execute();
            $result2 = $stmt2->get_result();
            $counts2 = $result2->fetch_all(MYSQLI_ASSOC);

           

            if($counts2[0]["exist"] == 1){
                $stmt1 = $this->db->prepare("SELECT COUNT(*) AS find FROM ordinecupon WHERE CodiceCupon = ? AND IdUtente = ?");
                $stmt1->bind_param("si", $cupon, $userId);
                $stmt1->execute();
                $result1 = $stmt1->get_result();
                $counts = $result1->fetch_all(MYSQLI_ASSOC);

                $stmt3 = $this->db->prepare("SELECT Prezzo FROM cupon WHERE Codice = ?");
                $stmt3->bind_param("s", $cupon);
                $stmt3->execute();
                $result3 = $stmt3->get_result();
                $counts3 = $result3->fetch_all(MYSQLI_ASSOC);

                $operationResult["sconto"] = $counts3[0]["Prezzo"];

                if($counts[0]["find"] == 1){
                    $operationResult["risultato"] = "Il cupon è già stato utilizzato.";
                }else{
                    $stmt = $this->db->prepare("INSERT INTO ordinecupon (CodiceCupon, IdUtente) VALUES (?,?)");
                    $stmt->bind_param("si", $cupon, $userId);
                    $stmt->execute();

                    $operationResult["risultato"] = "Il cupon è stato applicato e il totale finale è stato aggiornato.";
                }
            }else{
                $operationResult["risultato"] = "Il cupon inserito non esiste.";
            } 
        }

        return $operationResult;

    }

    public function getOrdersHistory($userId){
        $stmt1 = $this->db->prepare("SELECT prodotto.Id AS Id, prodotto.Titolo AS Titolo, prodotto.Sottotitolo AS Sottotitolo, prodotto.Immagine AS Immagine, prodotto.Prezzo AS Prezzo, ordine.Quantita AS Quantita, ordine.UltimoAggiornamento AS UltimoAggiornamento
        FROM prodotto
        INNER JOIN ordine ON prodotto.Id = ordine.IdProdotto WHERE ordine.Stato > 0 AND ordine.IdUtente = ?");
        $stmt1->bind_param("i", $userId);
        $stmt1->execute();
        $result1 = $stmt1->get_result();

        return $result1->fetch_all(MYSQLI_ASSOC);
    }

    
    public function subscribeNewsletter($email){

        $operationResult["risultato"] = "";

        if($email == ""){
            $operationResult["risultato"] = "Errore: il campo Nuovo Email non può essere lasciato vuoto.";
        } else {
            $stmt1 = $this->db->prepare("SELECT count(*) AS counter FROM newsletter WHERE Email = ?");
            $stmt1->bind_param("s", $email);
            $stmt1->execute();
            $result = $stmt1->get_result();
            $counts = $result->fetch_all(MYSQLI_ASSOC);

            if($counts[0]["counter"] == 1){
                $operationResult["risultato"] = "Errore: La email inserita esiste già.";
            } else {
                $stmt = $this->db->prepare("INSERT INTO newsletter (Email) VALUES (?)");
                $stmt->bind_param("s", $email);
                $stmt->execute();

                $operationResult["risultato"] = "L'iscrizione è avvenuta con successo.";
            }
        }
        
        return $operationResult;

    }

    public function contattaci($nome, $email, $message){

        $operationResult["risultato"] = "metodo bd";

        if($nome == "" || $email == "" || $message == ""){
            $operationResult["risultato"] = "Errore: i cmapi Nome, Email o Testo non può essere lasciato vuoto.";
        }else{
            $sendTo = "imbarieghcomunacioza@gmail.com";
            $subject = "Contatto da " . $nome . " mail: " .$email;
            $headers = "From: " . $email;
 
            if (mail($sendTo, $subject, $message, $headers)) {
                $operationResult["risultato"] = "Abbiamo ricevuto il tuo messaggio, ti risponderemo il prima possibile.";
            } else {
                $operationResult["risultato"] = "Errore: si è verificato un errore nell'invio della mail, verificare di aver inserito una mail valida.";
            }
        }
        
        return $operationResult;

    }
}
?>