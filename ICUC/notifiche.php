<?php

require_once("./bootstrap.php");


$templateParams["titolo"] = "Notifiche";

if(isUserLoggedIn()){
    $templateParams["nome"] = "contenuto-notifiche.php";
}
else{
    header("Location: ./notifiche-nologin.php");
}

if(isset($_SESSION["admin"]) && $_SESSION["admin"] == 1){
    $templateParams["outOfStock"] = $dbh->getOutOfStockNotifications();
    $templateParams["notificheOrdini"] = $dbh->getOrdinationsAdmins();

    foreach($templateParams["notificheOrdini"] as $ordine){
        $idOrdine = $ordine["Id"];
        $templateParams[$idOrdine] = $dbh->getNotificationsAdmins($idOrdine);
    }
} else {
    $templateParams["notificheOrdini"] = $dbh->getOrdinationsUsers($_SESSION["idUtente"]);

    foreach($templateParams["notificheOrdini"] as $ordine){
        $idOrdine = $ordine["Id"];
        $templateParams[$idOrdine] = $dbh->getNotificationsUsers($idOrdine);
    }

}

require("template/base.php");

?>