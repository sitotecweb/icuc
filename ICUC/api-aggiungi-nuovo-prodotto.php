<?php
    function utf8ize( $mixed ) {
        if (is_array($mixed)) {
            foreach ($mixed as $key => $value) {
                $mixed[$key] = utf8ize($value);
            }
        } elseif (is_string($mixed)) {
            return mb_convert_encoding($mixed, "UTF-8", "UTF-8");
        }
        return $mixed;
    }

    require_once("bootstrap.php");
    
    $risultato = $dbh->addProductModal($_GET["title"], $_GET["subTitle"], $_GET["provenience"], $_GET["description"], $_GET["prezzo"], $_GET["quantita"], $_GET["nomeFile"], $_GET["color"], $_GET["smell"], $_GET["taste"], $_GET["vite"], $_GET["alcholicGrade"], $_GET["temp"], $_GET["features"], $_GET["oggi"]);
    
    header("Content-Type: application/json");
    echo json_encode(utf8ize($risultato));
?>