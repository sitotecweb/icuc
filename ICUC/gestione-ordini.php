<?php

require_once("./bootstrap.php");

if(isset($_GET["Id"])){
    $dbh->removeFromCart($_SESSION["idUtente"], $_GET["Id"]);
}

$templateParams["titolo"] = "Gestione|Ordini";
$templateParams["nome"] = "form-gestione-ordini.php";

$templateParams["ordiniNelCarrello"] = $dbh->getOrdersFromState(0);
$templateParams["ordiniDaSpedire"] = $dbh->getOrdersFromState(1);
$templateParams["ordiniInTransito"] = $dbh->getOrdersFromState(2);
$templateParams["ordiniConsegnati"] = $dbh->getOrdersFromState(3);

$templateParams["js"] = array("js/jquery-3.4.1.min.js", "js/gestione-ordini.js");


require("template/base.php");

?>