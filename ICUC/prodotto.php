<?php

require_once("./bootstrap.php");


$templateParams["titolo"] = "Home - Prodotto";
$templateParams["nome"] = "singolo-prodotto.php";

$idprodotto = -1;
if(isset($_GET["Id"])){
    $idprodotto = $_GET["Id"];
}
$templateParams["prodotto"] = $dbh->getProductById($idprodotto)[0];

$templateParams["js"] = array("js/jquery-3.4.1.min.js", "js/aggiungi-al-carrello.js");


require("template/base.php");

?>