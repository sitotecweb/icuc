        <div class="container-fluid px-0 mb-5">

            <div class="card bg-dark text-dark border-0 rounded-0">
                <img src="<?php echo UPLOAD_DIR?>hero.png" class="card-img img-fluid" style="height: 100%; width: auto;" alt="...">
            </div>

            <div class="my-0 divisory"></div>

            <div class="row m-0">
                <div class="col-12 text-center text-white text-uppercase my-3">
                    <p class="fw-bold mb-0 text-dark fs-1">Ultimi arrivi</p>
                </div>
            </div>
            <div class="row m-0">
                <div class="col-md-2"></div>
                <div class="col-12 col-md-8">
                    <div id="carouselExampleControls1" class="carousel slide d-none d-md-block" data-bs-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="row row-cols-1 row-cols-md-4">

                                    <?php for($i = 0; $i < 4; $i++): 
                                        $prodotto = $templateParams["ultimiArrivi"][$i]; ?>

                                    <div class="col">
                                        <a href="prodotto.php?Id=<?php echo $prodotto["Id"];?>" class="card-vino text-decoration-none text-dark">
                                            <div class="card product-card shadow h-100">
                                                <img src="<?php echo UPLOAD_DIR.$prodotto["Immagine"];?>" class="card-img-top mt-2" alt="...">
                                                <div class="card-body">
                                                    <div>
                                                        <p class="fs-5 fw-bold"><?php echo $prodotto["Titolo"]?></p>
                                                        <p class="fs-6 text-uppercase"><?php echo $prodotto["Sottotitolo"]?></p>
                                                    </div>
                                                </div>
                                                <div class="card-footer">
                                                    <div class="d-flex flex-row justify-content-between">
                                                        <div>
                                                            <p class="fw-bold mt-1 mr-2">€ <?php echo $prodotto["Prezzo"]?></p>
                                                        </div>
                                                        <div>
                                                            <div class="btn btn-outline-danger p-1 add-to-cart">Aggiungi al carrello</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <?php endfor; ?>

                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="row row-cols-1 row-cols-md-4">

                                    <?php for($i = 4; $i < 8; $i++): 
                                        $prodotto = $templateParams["ultimiArrivi"][$i]; ?>

                                    <div class="col">
                                        <a href="prodotto.php?Id=<?php echo $prodotto["Id"];?>" class="card-vino text-decoration-none text-dark">
                                            <div class="card product-card shadow h-100">
                                                <img src="<?php echo UPLOAD_DIR.$prodotto["Immagine"];?>" class="card-img-top mt-2" alt="...">
                                                <div class="card-body">
                                                    <div>
                                                        <p class="fs-5 fw-bold"><?php echo $prodotto["Titolo"]?></p>
                                                        <p class="fs-6 text-uppercase"><?php echo $prodotto["Sottotitolo"]?></p>
                                                    </div>
                                                </div>
                                                <div class="card-footer">
                                                    <div class="d-flex flex-row justify-content-between">
                                                        <div>
                                                            <p class="fw-bold mt-1 mr-2">€ <?php echo $prodotto["Prezzo"]?></p>
                                                        </div>
                                                        <div>
                                                            <div class="btn btn-outline-danger p-1 add-to-cart">Aggiungi al carrello</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <?php endfor; ?>

                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="row row-cols-1 row-cols-md-4">
                                    <?php for($i = 8; $i < 12; $i++): 
                                        $prodotto = $templateParams["ultimiArrivi"][$i]; ?>

                                    <div class="col">
                                        <a href="prodotto.php?Id=<?php echo $prodotto["Id"];?>" class="card-vino text-decoration-none text-dark">
                                            <div class="card product-card shadow h-100">
                                                <img src="<?php echo UPLOAD_DIR.$prodotto["Immagine"];?>" class="card-img-top mt-2" alt="...">
                                                <div class="card-body">
                                                    <div>
                                                        <p class="fs-5 fw-bold"><?php echo $prodotto["Titolo"]?></p>
                                                        <p class="fs-6 text-uppercase"><?php echo $prodotto["Sottotitolo"]?></p>
                                                    </div>
                                                </div>
                                                <div class="card-footer">
                                                    <div class="d-flex flex-row justify-content-between">
                                                        <div>
                                                            <p class="fw-bold mt-1 mr-2">€ <?php echo $prodotto["Prezzo"]?></p>
                                                        </div>
                                                        <div>
                                                            <div class="btn btn-outline-danger p-1 add-to-cart">Aggiungi al carrello</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <?php endfor; ?>
                                </div>
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls1" role="button" data-bs-slide="prev">
                            <span class="carousel-control-prev-icon color-secondary" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls1" role="button" data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                        </a>
                    </div>

                    <div id="carouselExampleControls2" class="carousel slide d-md-none" data-bs-ride="carousel">
                        <div class="carousel-inner">

                            <?php for($i = 0; $i < 12; $i++): 

                                $prodotto = $templateParams["ultimiArrivi"][$i]; ?>

                                    <?php if($i == 0): ?>

                                        <div class="carousel-item active">

                                    <?php else: ?>

                                        <div class="carousel-item">

                                    <?php endif; ?>

                                    <div class="row row-cols-1 row-cols-md-4">
                                        <div class="col">
                                            <a href="prodotto.php?Id=<?php echo $prodotto["Id"];?>" class="card-vino text-decoration-none text-dark">
                                                <div class="card product-card shadow">
                                                    <img src="<?php echo UPLOAD_DIR?><?php echo $prodotto["Immagine"];?>" class="card-img-top mt-2" alt="...">
                                                    <div class="card-body">
                                                        <div>
                                                            <p class="fs-5 fw-bold"><?php echo $prodotto["Titolo"];?></p>
                                                            <p class="fs-6 text-uppercase"><?php echo $prodotto["Sottotitolo"];?></p>
                                                            <div class="d-flex flex-row justify-content-between">
                                                                <div>
                                                                    <p class="fw-bold mt-1 mr-2">€ <?php echo $prodotto["Prezzo"];?></p>
                                                                </div>
                                                                <div>
                                                                    <div class="btn btn-outline-danger p-1 add-to-cart">Aggiungi al carrello</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </a>
                                        </div>
                                    </div>
                                </div>

                            <?php endfor; ?>
                            
                            <a class="carousel-control-prev" href="#carouselExampleControls2" role="button" data-bs-slide="prev">
                                <span class="carousel-control-prev-icon color-secondary" aria-hidden="true"></span>
                                <span class="visually-hidden">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleControls2" role="button" data-bs-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Next</span>
                            </a>
                        </div>
                    </div>
                <div class="col-md-2"></div>
            </div>

            <div class="row m-0">
                <div class="col-md-2"></div>
                <div class="col-12 col-md-8">
                    <div class="card mb-1 mt-4 product-card">
                        <div class="row g-0">
                          <div class="col-md-4">
                            <img src="<?php echo UPLOAD_DIR?>3.jpg" class="img-fluid" alt="...">
                          </div>
                          <div class="col-md-8">
                            <div class="card-body">
                              <h5 class="card-title">I nostri sapori</h5>
                              <p class="card-text">Dalla terra alla tavola, la cura che da sempre noi emiliano-romagnoli mettiamo in tutto ciò che ci circonda, tramite il vino ritorna al mittente! Tutte le coltivazioni da vino da cui derivano i prodotti presenti nel sito, sono state controllate e testate dalla nostra equipe specializzata in vinicultura. Siamo orgogliosi di potervi presentare solo il meglio di tutto quello che cresce sulla nostra terra.</p>
                              <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>

            <div class="row m-0">
                <div class="col-12 text-center text-white text-uppercase my-3">
                    <p class="fw-bold mb-0 text-dark fs-1">Vini Emiliano-Romagnoli</p>
                </div>
            </div>
            <div class="row m-0">
                <div class="col-md-2"></div>
                <div class="col-12 col-md-8">
                    <div id="carouselExampleControls3" class="carousel slide d-none d-md-block" data-bs-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="row row-cols-1 row-cols-md-4">

                                    <?php for($i = 0; $i < 4; $i++): 
                                        $prodotto = $templateParams["emilianoRomagnoli"][$i]; ?>

                                    <div class="col">
                                        <a href="prodotto.php?Id=<?php echo $prodotto["Id"];?>" class="card-vino text-decoration-none text-dark">
                                            <div class="card product-card shadow h-100">
                                                <img src="<?php echo UPLOAD_DIR.$prodotto["Immagine"];?>" class="card-img-top mt-2" alt="...">
                                                <div class="card-body">
                                                    <div>
                                                        <p class="fs-5 fw-bold"><?php echo $prodotto["Titolo"]?></p>
                                                        <p class="fs-6 text-uppercase"><?php echo $prodotto["Sottotitolo"]?></p>
                                                    </div>
                                                </div>
                                                <div class="card-footer">
                                                    <div class="d-flex flex-row justify-content-between">
                                                        <div>
                                                            <p class="fw-bold mt-1 mr-2">€ <?php echo $prodotto["Prezzo"]?></p>
                                                        </div>
                                                        <div>
                                                            <div class="btn btn-outline-danger p-1 add-to-cart">Aggiungi al carrello</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <?php endfor; ?>

                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="row row-cols-1 row-cols-md-4">

                                    <?php for($i = 4; $i < 8; $i++): 
                                        $prodotto = $templateParams["emilianoRomagnoli"][$i]; ?>

                                    <div class="col">
                                        <a href="prodotto.php?Id=<?php echo $prodotto["Id"];?>" class="card-vino text-decoration-none text-dark">
                                            <div class="card product-card shadow h-100">
                                                <img src="<?php echo UPLOAD_DIR.$prodotto["Immagine"];?>" class="card-img-top mt-2" alt="...">
                                                <div class="card-body">
                                                    <div>
                                                        <p class="fs-5 fw-bold"><?php echo $prodotto["Titolo"]?></p>
                                                        <p class="fs-6 text-uppercase"><?php echo $prodotto["Sottotitolo"]?></p>
                                                    </div>
                                                </div>
                                                <div class="card-footer">
                                                    <div class="d-flex flex-row justify-content-between">
                                                        <div>
                                                            <p class="fw-bold mt-1 mr-2">€ <?php echo $prodotto["Prezzo"]?></p>
                                                        </div>
                                                        <div>
                                                            <div class="btn btn-outline-danger p-1 add-to-cart">Aggiungi al carrello</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <?php endfor; ?>

                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="row row-cols-1 row-cols-md-4">
                                    <?php for($i = 8; $i < 12; $i++): 
                                        $prodotto = $templateParams["emilianoRomagnoli"][$i]; ?>

                                    <div class="col">
                                        <a href="prodotto.php?Id=<?php echo $prodotto["Id"];?>" class="card-vino text-decoration-none text-dark">
                                            <div class="card product-card shadow h-100">
                                                <img src="<?php echo UPLOAD_DIR.$prodotto["Immagine"];?>" class="card-img-top mt-2" alt="...">
                                                <div class="card-body">
                                                    <div>
                                                        <p class="fs-5 fw-bold"><?php echo $prodotto["Titolo"]?></p>
                                                        <p class="fs-6 text-uppercase"><?php echo $prodotto["Sottotitolo"]?></p>
                                                    </div>
                                                </div>
                                                <div class="card-footer">
                                                    <div class="d-flex flex-row justify-content-between">
                                                        <div>
                                                            <p class="fw-bold mt-1 mr-2">€ <?php echo $prodotto["Prezzo"]?></p>
                                                        </div>
                                                        <div>
                                                            <div class="btn btn-outline-danger p-1 add-to-cart">Aggiungi al carrello</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <?php endfor; ?>

                                </div>
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls3" role="button" data-bs-slide="prev">
                            <span class="carousel-control-prev-icon color-secondary" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls3" role="button" data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                        </a>
                    </div>

                    <div id="carouselExampleControls4" class="carousel slide d-md-none" data-bs-ride="carousel">
                        <div class="carousel-inner">
                        
                            <?php for($i = 0; $i < 12; $i++): 

                                $prodotto = $templateParams["emilianoRomagnoli"][$i]; ?>

                                    <?php if($i == 0): ?>

                                        <div class="carousel-item active">

                                    <?php else: ?>

                                        <div class="carousel-item">

                                    <?php endif; ?>

                                    <div class="row row-cols-1 row-cols-md-4">
                                        <div class="col">
                                            <a href="prodotto.php?Id=<?php echo $prodotto["Id"];?>" class="card-vino text-decoration-none text-dark">
                                                <div class="card product-card shadow">
                                                    <img src="<?php echo UPLOAD_DIR?><?php echo $prodotto["Immagine"];?>" class="card-img-top mt-2" alt="...">
                                                    <div class="card-body">
                                                        <div>
                                                            <p class="fs-5 fw-bold"><?php echo $prodotto["Titolo"];?></p>
                                                            <p class="fs-6 text-uppercase"><?php echo $prodotto["Sottotitolo"];?></p>
                                                            <div class="d-flex flex-row justify-content-between">
                                                                <div>
                                                                    <p class="fw-bold mt-1 mr-2">€ <?php echo $prodotto["Prezzo"];?></p>
                                                                </div>
                                                                <div>
                                                                    <div class="btn btn-outline-danger p-1 add-to-cart">Aggiungi al carrello</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </a>
                                        </div>
                                    </div>
                                </div>

                            <?php endfor; ?>

                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls4" role="button" data-bs-slide="prev">
                            <span class="carousel-control-prev-icon color-secondary" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls4" role="button" data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                        </a>
                        </div>
                    </div>
                <div class="col-md-2"></div>
            </div>

            <div class="row m-0">
                <div class="col-md-2"></div>
                <div class="col-12 col-md-8">
                    <div class="card mb-1 mt-4 product-card">
                        <div class="row g-0">
                            <div class="col-md-8">
                                <div class="card-body">
                                    <h5 class="card-title">Le nostre cantine</h5>
                                    <p class="card-text">Da generazioni, con la tradizione sulle spalle e il futuro alla guida, le nostre cantine si prodigano al mantenimento di un clima ai massimi livelli tecnologici, in modo da garantire un prodotto di qualità sempre migliore. Da ormai 10 anni il nsotro e-commerce in collaborazione con alcune banche locali promuove con ingenti incentivi lo sviluppo delle cantine destinate alla produzione di vino. Per essere sempre al vostro fianco!</p>
                                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <img src="<?php echo UPLOAD_DIR?>4.jpg" class="img-fluid" alt="...">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>

            <div class="row m-0">
                <div class="col-12 text-center text-white text-uppercase my-3">
                    <p class="fw-bold mb-0 text-dark fs-1">Annata 2020</p>
                </div>
            </div>
            <div class="row m-0">
                <div class="col-md-2"></div>
                <div class="col-12 col-md-8">
                    <div id="carouselExampleControls5" class="carousel slide d-none d-md-block" data-bs-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="row row-cols-1 row-cols-md-4">

                                    <?php for($i = 0; $i < 4; $i++): 
                                        $prodotto = $templateParams["annata2020"][$i]; ?>

                                    <div class="col">
                                        <a href="prodotto.php?Id=<?php echo $prodotto["Id"];?>" class="card-vino text-decoration-none text-dark">
                                            <div class="card product-card shadow h-100">
                                                <img src="<?php echo UPLOAD_DIR.$prodotto["Immagine"];?>" class="card-img-top mt-2" alt="...">
                                                <div class="card-body">
                                                    <div>
                                                        <p class="fs-5 fw-bold"><?php echo $prodotto["Titolo"]?></p>
                                                        <p class="fs-6 text-uppercase"><?php echo $prodotto["Sottotitolo"]?></p>
                                                    </div>
                                                </div>
                                                <div class="card-footer">
                                                    <div class="d-flex flex-row justify-content-between">
                                                        <div>
                                                            <p class="fw-bold mt-1 mr-2">€ <?php echo $prodotto["Prezzo"]?></p>
                                                        </div>
                                                        <div>
                                                            <div class="btn btn-outline-danger p-1 add-to-cart">Aggiungi al carrello</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <?php endfor; ?>

                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="row row-cols-1 row-cols-md-4">

                                    <?php for($i = 4; $i < 8; $i++): 
                                        $prodotto = $templateParams["annata2020"][$i]; ?>

                                    <div class="col">
                                        <a href="prodotto.php?Id=<?php echo $prodotto["Id"];?>" class="card-vino text-decoration-none text-dark">
                                            <div class="card product-card shadow h-100">
                                                <img src="<?php echo UPLOAD_DIR.$prodotto["Immagine"];?>" class="card-img-top mt-2" alt="...">
                                                <div class="card-body">
                                                    <div>
                                                        <p class="fs-5 fw-bold"><?php echo $prodotto["Titolo"]?></p>
                                                        <p class="fs-6 text-uppercase"><?php echo $prodotto["Sottotitolo"]?></p>
                                                    </div>
                                                </div>
                                                <div class="card-footer">
                                                    <div class="d-flex flex-row justify-content-between">
                                                        <div>
                                                            <p class="fw-bold mt-1 mr-2">€ <?php echo $prodotto["Prezzo"]?></p>
                                                        </div>
                                                        <div>
                                                            <div class="btn btn-outline-danger p-1 add-to-cart">Aggiungi al carrello</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <?php endfor; ?>

                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="row row-cols-1 row-cols-md-4">
                                    <?php for($i = 8; $i < 12; $i++): 
                                        $prodotto = $templateParams["annata2020"][$i]; ?>

                                    <div class="col">
                                        <a href="prodotto.php?Id=<?php echo $prodotto["Id"];?>" class="card-vino text-decoration-none text-dark">
                                            <div class="card product-card shadow h-100">
                                                <img src="<?php echo UPLOAD_DIR.$prodotto["Immagine"];?>" class="card-img-top mt-2" alt="...">
                                                <div class="card-body">
                                                    <div>
                                                        <p class="fs-5 fw-bold"><?php echo $prodotto["Titolo"]?></p>
                                                        <p class="fs-6 text-uppercase"><?php echo $prodotto["Sottotitolo"]?></p>
                                                    </div>
                                                </div>
                                                <div class="card-footer">
                                                    <div class="d-flex flex-row justify-content-between">
                                                        <div>
                                                            <p class="fw-bold mt-1 mr-2">€ <?php echo $prodotto["Prezzo"]?></p>
                                                        </div>
                                                        <div>
                                                            <div class="btn btn-outline-danger p-1 add-to-cart">Aggiungi al carrello</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <?php endfor; ?>
                                </div>
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls5" role="button" data-bs-slide="prev">
                            <span class="carousel-control-prev-icon color-secondary" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls5" role="button" data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                        </a>
                    </div>

                    <div id="carouselExampleControls6" class="carousel slide d-md-none" data-bs-ride="carousel">
                        <div class="carousel-inner">

                            <?php for($i = 0; $i < 12; $i++): 

                                $prodotto = $templateParams["annata2020"][$i]; ?>

                                    <?php if($i == 0): ?>

                                        <div class="carousel-item active">

                                    <?php else: ?>

                                        <div class="carousel-item">

                                    <?php endif; ?>

                                    <div class="row row-cols-1 row-cols-md-4">
                                        <div class="col">
                                            <a href="prodotto.php?Id=<?php echo $prodotto["Id"];?>" class="card-vino text-decoration-none text-dark">
                                                <div class="card product-card shadow">
                                                    <img src="<?php echo UPLOAD_DIR?><?php echo $prodotto["Immagine"];?>" class="card-img-top mt-2" alt="...">
                                                    <div class="card-body">
                                                        <div>
                                                            <p class="fs-5 fw-bold"><?php echo $prodotto["Titolo"];?></p>
                                                            <p class="fs-6 text-uppercase"><?php echo $prodotto["Sottotitolo"];?></p>
                                                            <div class="d-flex flex-row justify-content-between">
                                                                <div>
                                                                    <p class="fw-bold mt-1 mr-2">€ <?php echo $prodotto["Prezzo"];?></p>
                                                                </div>
                                                                <div>
                                                                    <div class="btn btn-outline-danger p-1 add-to-cart">Aggiungi al carrello</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </a>
                                        </div>
                                    </div>
                                </div>

                            <?php endfor; ?>

                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls6" role="button" data-bs-slide="prev">
                            <span class="carousel-control-prev-icon color-secondary" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls6" role="button" data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                        </a>
                        </div>
                    </div>
                <div class="col-md-2"></div>
            </div>
            </div>
        </div>