<div class="container-fluid px-0 py-5 mb-4">
    <div class="row m-0 pt-5">
        <div class="col-12 text-center text-white text-uppercase my-3">
            <h1 class="fw-bold mb-0 text-dark">CREA NUOVI</h1><h1 class="fw-bold mb-0 text-danger">TAG DI RICERCA</h1>
        </div>
    </div>
    <div class="row m-0 pt-5">
        <div class="col-12 col-md-2"></div>
        <div class="col-12 col-md-8">
            <div class="row">
                <form class=" col-12 col-md-6 border border-3 border-danger px-3 py-4">
                    <div class="form-group">
                        <label for="newTag" class="mt-2 h5">NUOVO TAG:</label>
                        <input class="form-control mt-1" id="newTag"  name="newTag" placeholder="Inserisci quì il nuovo tag da aggiungere.">
                    </div>
                </form>

                <div class="col-12 col-md-6 text-center">
                    <div class="form-group d-grid gap-2 text-center text-white h-50 mt-5">
                        <button type="button" id="addNewTagClick" class="btn btn-danger btn-lg btn-block">Aggiungi</button>
                        <div id="appendAddTagModal"></div>
                    </div>
                </div>
            </div>
            
        </div>
        
        <div class="col-12 col-md-8"></div>
    </div>
</div>