<div class="container-fluid px-0 py-5 mb-4">
    <div class="row m-0 pt-5">
        <div class="col-12 text-center text-white text-uppercase my-3">
            <h1 class="fw-bold mb-0 text-dark">GESTIONE</h1><h1 class="fw-bold mb-0 text-danger">CUPON</h1>
        </div>
    </div>
    <div class="row m-0 pt-5">
        <div class="col-12 col-md-2"></div>
        <div class="col-12 col-md-8">
            <div class="row justify-content-between">
                <form class=" col-12 col-md-5 border border-3 border-danger px-3 py-4">
                    <div class="row">
                        <div class="col-12 col-lg-6">
                            <label for="newCupon" class="mt-2 h5">Nuovo Cupon:</label>
                            <input class="form-control mt-1" id="newCupon" placeholder="Inserire codice cupon.">
                        </div>
                        <div class="col-12 col-lg-6">
                            <label for="newPrice" class="mt-2 h5">Sconto Cupon in %:</label>
                            <input class="form-control mt-1" id="newPrice" placeholder="es.10">
                        </div>
                    </div>
                    
                    <div class="text-center">
                        <div class="form-group d-grid gap-2 text-center text-white h-50 mt-5">
                            <button type="button" id="addNewCuponClick" class="btn btn-danger btn-lg btn-block">Aggiungi</button>
                            <div id="appendAddCuponModal"></div>
                        </div>
                    </div>
                </form>
                <div class="col-12 col-md-2 p-0 mt-2 mb-2"></div>
                <form class=" col-12 col-md-5 border border-3 border-danger px-3 py-4">
                
                    <label for="selCupon" class="mt-2 h5">Elimina Cupon:</label>
                    <select class="form-select cuponSelector" id="selCupon" aria-label="Delete cupon selector">
                        <option value="0" selected>Clicca e seleziona il cupon da eliminare</option>
                        <?php for($i = 0; $i < count($templateParams["currentCupons"]); $i++): 
                            $cupon = $templateParams["currentCupons"][$i]; ?>
                        <option value="<?php echo $cupon["Codice"];?>">Codice: <?php echo $cupon["Codice"];?>, Sconto: <?php echo $cupon["Prezzo"];?>%</option>
                        <?php endfor; ?>
                       
                    </select>
                    
                    <div class="text-center">
                        <div class="form-group d-grid gap-2 text-center text-white h-50 mt-5">
                            <button type="button" id="deleteCuponClick" class="btn btn-danger btn-lg btn-block">Elimina</button>
                            <div id="appendDeleteCuponModal"></div>
                        </div>
                    </div>
                </form>

                
            </div>
            
        </div>
        
        <div class="col-12 col-md-8"></div>
    </div>
</div>