        <div class="container-fluid px-0">
            <div class="row m-0">
                <div class="col-12 p-0">
                    <div class="card bg-dark text-white border-0">
                        <img src="<?php echo UPLOAD_DIR?>login.jpg" class="card-img" alt="...">
                        <div class="card-img-overlay d-flex align-items-center justify-content-center">
                            <form class="border rounded border-dark bg-light" action="#" method="POST">
                                <div class="tab-pane fade show active" id="pills-home" role="tabpanel">
                                    <div class="mb-3">
                                        <div class="form-text text-uppercase text-center text-dark"><h1>Log In</h1></div>
                                    </div>

                                    <div class="mb-3">
                                        <label for="exampleInputEmail1" class="form-text">Email:</label>
                                        <div class="input-group">
                                            <div class="input-group-text"><span class="fa fa-user"></span></div>
                                            <input type="email" class="form-control" id="exampleInputEmail1" name="email" required>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <label for="exampleInputPassword1" class="form-text">Password:</label>
                                        <div class="input-group">
                                            <div class="input-group-text"><span class="fa fa-lock"></span></div>
                                            <input type="password" class="form-control" id="exampleInputPassword1" name="password" required>
                                        </div>
                                    </div>
                                    <div class="mb-3 form-check">
                                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                        <label class="text-muted form-check-label" for="exampleCheck1">Ricordami</label>
                                    </div>
                                    <button type="button" class="btn btn-danger" onclick="formhash(this.form, this.form.password);">Accedi</button>

                                    <?php if(isset($templateParams["erroreLogin"])): ?>
                                        <div class="form-text"><strong><?php echo $templateParams["erroreLogin"]; ?></strong></div>
                                    <?php endif;?>
                                    
                                    <div class="form-text">Non hai ancora un account? <a href="./registrazione.php" class="text-decoration-none form-text fw-bold">Clicca qui</a> per <br> registrarti!</div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
