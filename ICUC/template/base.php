<!doctype html>
<html lang="it">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">

    <!-- Font -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Quicksand:wght@400;700&display=swap">

   <?php
    if(isset($templateParams["js"])):
        foreach($templateParams["js"] as $script):
    ?>
    <script src="<?php echo $script;?>"></script>
    <?php
        endforeach;
    endif;
    ?>

    <title>Imbariêgh com una ciöza - <?php  echo $templateParams["titolo"]?></title>
</head>
<body>
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark">
        <div class="container-fluid">
            <a class="navbar-brand text-uppercase fw-bold d-none d-md-block navbar-hov" href="index.php">Imbariêgh com una ciöza</a>
            <a class="navbar-brand text-uppercase fw-bold d-md-none fs-6 m-0 navbar-hov" href="index.php">Imbariêgh com una ciöza</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                    <li class="nav-item px-2">
                        <a class="nav-link active fw-bold navbar-hov" aria-current="page" href="index.php">Home</a>
                    </li>
                    <?php if(isset($_SESSION["admin"]) && $_SESSION["admin"] == 1): ?>
                        <li class="nav-item px-2">
                            <a class="nav-link active fw-bold navbar-hov" aria-current="page" href="gestione-negozio.php">Amministrazione</a>
                        </li>
                    <?php endif; ?>
                    <li class="nav-item px-2">
                        <a class="nav-link active fw-bold navbar-hov" aria-current="page" href="prodotti.php">Prodotti</a>
                    </li>
                    <li class="nav-item px-2">
                        <a class="nav-link active fw-bold navbar-hov" aria-current="page" href="notifiche.php">Notifiche</a>
                    </li>
                    <li class="nav-item px-2 d-none d-md-block py-1">
                        <a class="nav-link active fw-bold navbar-hov py-0" aria-current="page" href="login.php"><span class="fa fa-user mt-2"></span></a>
                    </li>
                    <li class="nav-item px-2 d-md-none">
                        <a class="nav-link active fw-bold navbar-hov" aria-current="page" href="login.php">Account</a>
                    </li>
                    <li class="nav-item px-2 d-none d-md-block py-1">
                        <a class="nav-link active fw-bold navbar-hov py-0" aria-current="page" href="carrello.php"><span class="fa fa-shopping-cart mt-2"></span></a>
                    </li>
                    <li class="nav-item px-2 d-md-none">
                        <a class="nav-link active fw-bold navbar-hov" aria-current="page" href="carrello.php">Carrello</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <main>
    <?php 
    if(isset($templateParams["nome"])){
        require($templateParams["nome"]);
    }
    ?>
    
    </main>

    <div class="my-0 divisory"></div>

<footer class="page-footer font-small m-0 p-0">
    <div class="container-fluid">
        <div class="row pt-5">
            <div class="col-12 col-md-2"></div>
            <div class="col-12 col-md-8 nav text-white mx-0 px-2">
                <div class="nav-item col-12 col-md-6 text-center mt-2">
                    <h2 class="text-uppercase fw-bold mb-5">Social:</h2>
                    <ul class="nav flex-column">
                        <li class="nav-item mb-4">
                            <a class="text-white text-decoration-none h3" href="#">Facebook</a>
                        </li>
                        <li class="nav-item mb-4">
                            <a class="text-white text-decoration-none h3" href="#">Instagram</a>
                        </li>
                        <li class="nav-item mb-4">
                            <a class="text-white text-decoration-none h3" href="#">Twitter</a>
                        </li>
                        <li class="nav-item mb-4">
                            <a class="text-white text-decoration-none h3" href="#">Linkedin</a>
                        </li>
                    </ul>
                </div>
                <div class="nav-item col-12 col-md-6 text-center text-white px-0 mx-0">
                    <h2 class="text-uppercase fw-bold">Contattaci:</h2>
                    <form>
                    <div class="mb-3">
                            <label for="contattaciNome" class="form-label">Nome</label>
                            <input type="text" class="form-control" id="contattaciNome" placeholder="Nome">
                        </div>
                        <div class="mb-3">
                            <label for="contattaciEmail" class="form-label">Email address</label>
                            <input type="email" class="form-control" id="contattaciEmail" placeholder="Email">
                        </div>
                        <div class="mb-3">
                            <label for="contattaciMessaggio">Messaggio:</label>
                            <textarea class="form-control" id="contattaciMessaggio" rows="3" placeholder="Scrivi quì il tuo messaggio per noi"></textarea>
                        </div>
                        <button type="button" id="contattaciClick" class="btn btn-danger btn-lg btn-block">Invia</button>
                        <div id="appendContattaci"></div>
                    </form>
                </div>
            </div>
            <div class="col-12 col-md-2"></div>
        </div>
        <div class="row mb-4">
            <div class="col-md-2"></div>
            <div class="col-12 col-md-8 nav p-0 mt-3">
                <div class="nav-item col-12 col-md-6 text-center text-white">
                    <h2 class="text-uppercase fw-bold">Chi Siamo:</h2>
                    <p>Siamo l'azienda romagnola che da oltre 50 anni rifornisce tutto il territorio Emiliano-Romagnolo con i migliori vini italiani. Dopo tanti anni vogliamo continuare a portare sulle vostre tavole solo il meglio, per questo sul nostro sito vogliamo mostrarvi solo i prodotti migliori della nostra terra. 
                    </p>
                </div>
                <div class="nav-item col-12 col-md-6 text-center text-white">
                    <h2 class="text-uppercase fw-bold">Newsletter:</h2>
                    <div class="nav">
                        <div class="col-1"></div>
                        <div class="nav-item col-7 my-2 px-2">
                            <label for="iscrivitiEmail" hidden>Email:</label>
                            <input type="text" id="iscrivitiEmail" class="form-control" placeholder="Email">
                        </div>
                        <div class="nav-item col-3 my-2">
                        <button type="button" id="iscrivitiClick" class="btn btn-danger btn-lg btn-block">Iscriviti</button>
                        <div id="appendIscriviti"></div>
                        </div>
                        <div class="col-1"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
        <div class="row">
            <div class="col-12 footer-background">
                <div class="footer-copyright text-center text-white py-3">© 2021 Copyright:
                    <a class="text-white" href=""> imbariêghcomunaciöza.com</a>
                </div>
            </div>
        </div>
    </div>
</footer>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</body>
</html>
