        <div class="container-fluid mb-5">
            <div class="row">
                <p class="fs-3 text-uppercase text-center mt-5">Informazioni di pagamento</p>
            </div>
            

            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8 px-5">
                    <form method="post">
                        <div class="row">
                            <div class="col-12 col-md-8">
                                <div class="position-relative m-4">
                                    <div class="progress" style="height: 1px;">
                                        <div class="progress-bar bg-danger" role="progressbar" style="width: 100%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <a href="./carrello.php" class="position-absolute top-0 start-0 translate-middle btn btn-sm btn-danger rounded-pill" style="width: 2rem; height:2rem;">1</a>
                                    <a href="./indirizzo.php" class="position-absolute top-0 start-50 translate-middle btn btn-sm btn-danger rounded-pill" style="width: 2rem; height:2rem;">2</a>
                                    <button type="button" class="position-absolute top-0 start-100 translate-middle btn btn-sm btn-danger rounded-pill" style="width: 2rem; height:2rem;">3</button>
                                </div>
                                <p class="fs-4 text-uppercase text-center mt-5">Aggiungi i dati della carta</p>

                                <div class="d-flex flex-column flex-md-row mt-1">
                                    <div class="col-md-5">
                                        <label for="inputName" class="form-label">Nome</label>
                                        <input name="nome" type="text" class="form-control" id="inputName" required>
                                    </div>
                                    <div class="col-md-1"></div>
                                    <div class="col-md-5">
                                        <label for="inputSurname" class="form-label">Cognome</label>
                                        <input name="cognome" type="text" class="form-control" id="inputSurname" required>
                                    </div>
                                    <div class="col-md-1"></div>
                                </div> 
                                <div class="mt-1">
                                    <div class="col-md-11">
                                        <label for="inputAddress" class="form-label">Numero della carta</label>
                                        <input name="numeroCarta" type="text" class="form-control" id="inputAddress" required>
                                    </div>
                                </div>
                                <div class="d-flex flex-column flex-md-row mt-1">
                                    <div class="col-md-5">
                                        <label for="inputCity" class="form-label">Data di scadenza</label>
                                        <input name="dataScadenza" type="date" class="form-control" id="inputCity" required>
                                    </div>
                                    <div class="col-md-1"></div>
                                    <div class="col-md-1">
                                        <label for="inputZip" class="form-label">CVV</label>
                                        <input name="cvv" type="text" class="form-control" id="inputZip" required>
                                    </div>
                                    <div class="col-md-5"></div>
                                </div>
                                <div class="my-5">
                                    <div class="col-12">
                                        <?php if(isset($templateParams["campiIncompleti"])): ?>
                                        <div class="form-text mb-2"><strong><?php echo $templateParams["campiIncompleti"]; ?></strong></div>
                                        <?php endif;?>
                                        <button name="submit" type="submit" class="btn btn-danger fw-bold">Acquista ora</button>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-12 col-md-4 px-5">
                                <p class="fs-4 text-uppercase mt-5">L'ordine ammonta a:</p>
                                <div class="d-flex justify-content-between">
                                    <p>Totale parziale</p>
                                    <p><?php echo $templateParams["totaleCarrello"];?>€</p>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <p>Spedizione</p>
                                    <p>Gratis</p>
                                </div>
                                <hr class="bg-secondary mt-1">
                                <div class="d-flex justify-content-between">
                                    <p>Totale (IVA inclusa)</p>
                                    <div class="d-flex justify-content-end">
                                        <p id="totale" class="fw-bold"><?php echo $templateParams["totaleCarrello"]; ?></p><p class="fw-bold">€</p>
                                        <label for="sconto" hidden>Sconto:</label>
                                        <input name="sconto" id="sconto" hidden>
                                    </div>
                                </div>

                                <button name="submit" type="submit" class="btn btn-danger text-uppercase fw-bold checkout-button mb-4">Acquista ora</button>
                                
                                <p>Aggiungi un codice sconto (opzionale):</p>
                                <label for="nomeCupon" hidden>NomeCupon:</label>
                                <input type="text" id="nomeCupon" class="form-control mt-4">
                                <button type="submit" id="useCupon" class="btn btn-outline-danger text-uppercase fw-bold checkout-button mt-4">Riscatta ora</button>
                                <div id="appendCuponResult"></div>
                            </div>
                            

                        </div>
                        
                    </form>
                </div>
                
                <div class="col-md-2"></div>
            </div>
            
        </div>