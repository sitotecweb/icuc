<div class="container-fluid px-0 py-5 mb-5 mt-5">
            <div class="row m-0 pt-5">
                <div class="col-12 text-center text-white text-uppercase my-3">
                    <p class="fw-bold mb-0 text-dark fs-1">GESTIONE </p><p class="fw-bold mb-0 text-danger fs-1">Negozio </p>
                </div>
            </div>
            <div class="row m-0">
                <div class="col-md-2"></div>
                <div class="col-12 col-md-8 mt-5 py-5 mb-5">

                    <div class="row row-cols-1 row-cols-md-2 g-4">
                        <div class="col">
                          <div class="card border-danger h-100">
                            <div class="card-body text-center">
                                <p class="card-title text-danger fs-3">GESTIONE PRODOTTI</p>
                                <p class="card-text fs-5">Area dedicata alla gestione della merce del negozio.</p><br>
                            </div>
                            <div class="card-footer">
                                <div class="d-grid gap-2 align-bottom">
                                    <a href="gestione-catalogo.php" class="btn btn-danger btn-lg btn-block">Accedi</a> 
                                </div>
                            </div>
                          </div>
                        </div>
                        <div class="col">
                          <div class="card border-danger h-100">
                            <div class="card-body text-center">
                                <p class="card-title text-danger fs-3">GESTIONE ORDINI</p>
                                <p class="card-text fs-5">Area dedicata alla gestione degli ordini effettuati dai clienti.</p><br>
                            </div>
                            <div class="card-footer">
                                <div class="d-grid gap-2 align-bottom">
                                    <a href="gestione-ordini.php" class="btn btn-danger btn-lg btn-block">Accedi</a> 
                                </div>
                            </div>
                          </div>
                        </div>
                    </div> 
                </div>     
                <div class="col-md-2"></div>
            </div>

        </div>