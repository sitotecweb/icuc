<div class="container-fluid px-0 py-5 mb-4">
    <div class="row m-0 pt-5">
        <div class="col-12 text-center text-white text-uppercase my-3">
            <h1 class="fw-bold mb-0 text-dark">MODIFICA</h1><h1 class="fw-bold mb-0 text-danger">PRODOTTO</h1>
        </div>
    </div>
    <div class="row m-0 pt-5">
        <div class="col-12 col-md-2"></div>
        <div class="col-12 col-md-8">
            <form class="border border-3 border-danger px-4 py-4">
                <label for="productSelector" hidden>Select:</label>
                <select class="form-select productSelector" id="productSelector" aria-label="Default select example">
                    <option value="0" selected>CLICCA E SELEZIONA IL PRODOTTO DA MODIFICARE</option>
                    <?php for($i = 0; $i < count($templateParams["currentProducts"]); $i++): 
                        $prodotto = $templateParams["currentProducts"][$i]; ?>
                        <option value="<?php echo $prodotto["Id"];?>"><?php echo $prodotto["Titolo"];?></option>
                    <?php endfor; ?>
                </select>
                <div id="formRow" class="row mt-5 invisible appendProduct">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12 text-center">
                                <p class="text-muted text-danger">I campi contrassegnati con * sono obbligatori e non possono essere lasciati vuoti.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label for="title" class="mt-2 h5">Nome Prodotto: *</label>
                                    <input class="form-control mt-1" id="title" name="title" placeholder="es.Vino Rosso di Montalcino">
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label for="subTitle" class="mt-2 h5">Cantina Produttrice: *</label>
                                    <input class="form-control mt-1" id="subTitle" name="subTitle" placeholder="es.BOLE">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label for="provenience" class="mt-2 h5">Provenienza: *</label>
                                    <input class="form-control mt-1" id="provenience" name="provenience" placeholder="es.Italia, Toscana">
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label for="description" class="mt-2 h5">Descrizione prodotto:</label>
                                    <textarea class="form-control mt-1" id="description" name="description" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                    
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label for="prezzo" class="mt-2 h5">Prezzo: *</label>
                                    <input class="form-control mt-1" id="prezzo" name="prezzo" placeholder="Inserisci il prezzo a bottiglia">
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label for="quantita" class="mt-2 h5">Quantità: *</label>
                                    <input class="form-control mt-1" id="quantita" name="quantita" placeholder="Inserisci il numero di bottiglie disponibili">
                                </div>
                            </div>
                        </div>
                    
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label for="image" class="mt-2 h5" >Immagine: *</label>
                                    <input type="file" class="form-control mt-1" id="image" onchange="readURL(this);">
                                </div>
                            </div>

                            <div class="col-12 col-md-6">
                                <div class="form-group text-center">
                                    <img id="selImg" src="#" class="mt-5 img-thumbnail" alt="Seleziona un file per visualizzarlo." />
                                </div>
                            </div>

                            <div class="form-group invisible">
                                <label for="nomeFile" hidden>NomeFile:</label>
                                <input class="form-control" id="nomeFile" name="nomeFile">
                            </div>
                            
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label for="color" class="mt-2 h5">Colore: *</label>
                                    <textarea class="form-control mt-1" id="color" name="color" rows="3"></textarea>
                                </div>
                            </div>

                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label for="smell" class="mt-2 h5">Profumo: *</label>
                                    <textarea class="form-control mt-1" id="smell" name="smell" rows="3"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label for="taste" class="mt-2 h5">Sapore: *</label>
                                    <textarea class="form-control mt-1" id="taste" name="taste" rows="3"></textarea>
                                </div>
                            </div>

                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label for="vite" class="mt-2 h5">Vitigno:</label>
                                    <textarea class="form-control mt-1" id="vite" name="vite" rows="3"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label for="alcholicGrade" class="mt-2 h5">Gradazione:</label>
                                    <textarea class="form-control mt-1" id="alcholicGrade" name="alcholicGrade" rows="3"></textarea>
                                </div>
                            </div>

                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label for="temp" class="mt-2 h5">Temperatura di servizio:</label>
                                    <textarea class="form-control mt-1" id="temp" name="temp" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="features" class="mt-2 h5">Caratteristiche:</label>
                                    <textarea class="form-control mt-1" id="features" name="features" rows="3"></textarea>
                                </div>
                            </div>
                        </div>               
                    </div>
                
                        <div class="col-12 text-center">
                            <div class="form-group d-grid gap-2 text-center text-white h-50 mt-5">
                                <button type="button" class="btn btn-danger btn-lg btn-block" id="commitChanges">Conferma le modifiche</button>
                                <div id="appendChangeProductModal"></div>
                            </div>
                        </div> 
                </div>
            </form>
        </div>
        
        <div class="col-12 col-md-2"></div>
    </div>
</div>