        <div class="container-fluid">
            <div class="row">
                <div class="col-1 col-md-2"></div>
                <div class="col-10 col-md-8">
                    <p class="text-center text-uppercase fs-5 fw-bold mt-5 mb-0">Notifiche</p>
                    <?php if(count($templateParams["notificheOrdini"]) == 0): ?>

                        <div class="alert alert-primary text-center my-5" role="alert">
                            Non ci sono ancora notifiche!
                        </div>

                    <?php else: ?>

                        <div class="accordion mt-4 mb-5" id="accordionNotifications">

                            <?php if(isset($_SESSION["admin"]) && $_SESSION["admin"] == 1): ?>

                                <?php if(count($templateParams["outOfStock"]) > 0): ?>

                                    <?php foreach($templateParams["outOfStock"] as $terminato):?>
                                
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="heading<?php echo $terminato["Id"]; ?>">
                                                <button class="accordion-button collapsed fw-bold" type="button" data-bs-toggle="collapse" data-bs-target="#collapse<?php echo $terminato["Id"]; ?>" aria-expanded="false" aria-controls="collapse<?php echo $terminato["Id"]; ?>">
                                                    <?php echo $terminato["Titolo"]; ?>
                                                </button>
                                            </h2>
                                            <div id="collapse<?php echo $terminato["Id"]; ?>" class="accordion-collapse collapse" aria-labelledby="heading<?php echo $terminato["Id"]; ?>" data-bs-parent="#accordionNotifications">
                                                <div class="accordion-body">
                                                    <div class="alert alert-<?php echo $terminato["Colore"]; ?>" role="alert">
                                                        <p><?php echo $terminato["Testo"]; ?></p>
                                                        <p class="text-end"><?php echo $terminato["Timestamp"]; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    <?php endforeach; ?>

                                <?php endif; ?>
                            
                            <?php endif; ?>

                            <?php foreach($templateParams["notificheOrdini"] as $ordine):?>

                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="heading<?php echo $ordine["Id"]; ?>">
                                        <button class="accordion-button collapsed fw-bold" type="button" data-bs-toggle="collapse" data-bs-target="#collapse<?php echo $ordine["Id"]; ?>" aria-expanded="false" aria-controls="collapse<?php echo $ordine["Id"]; ?>">
                                            #<?php echo $ordine["Id"]; ?>: <?php echo $ordine["Titolo"]; ?>
                                        </button>
                                    </h2>
                                    <div id="collapse<?php echo $ordine["Id"]; ?>" class="accordion-collapse collapse" aria-labelledby="heading<?php echo $ordine["Id"]; ?>" data-bs-parent="#accordionNotifications">
                                        <div class="accordion-body">

                                            <?php foreach($templateParams[$ordine["Id"]] as $notifica):?>

                                                <div class="alert alert-<?php echo $notifica["Colore"]; ?>" role="alert">
                                                    <p><?php echo $notifica["Testo"]; ?></p>
                                                    <p class="text-end"><?php echo $notifica["Timestamp"]; ?></p>
                                                </div>

                                            <?php endforeach; ?>

                                        </div>
                                    </div>
                                </div>

                            <?php endforeach; ?>

                        </div>

                    <?php endif; ?>
                <div class="col-1 col-md-2"></div>
            </div>
            </div>
        </div>