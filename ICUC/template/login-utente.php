    <div class="container-fluid">
        <div class="row">
            <div class="col-12 mt-5 mb-3">
                <p class="text-center text-uppercase fs-6 d-md-none">Benvenuto <strong><?php echo $_SESSION["nome"];?> <?php echo $_SESSION["cognome"];?></strong>!</p>
                <p class="text-center text-uppercase fs-5 d-none d-md-block">Benvenuto <strong><?php echo $_SESSION["nome"];?> <?php echo $_SESSION["cognome"];?></strong>!</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-12 col-md-4 mb-5">
                <p class="text-uppercase text-center fs-6">Informazioni account</p>
                <hr class="bg-secondary">
                <p>Nome: <strong><?php echo $_SESSION["nome"]?></strong></p>
                <p>Cognome: <strong><?php echo $_SESSION["cognome"]?></strong></p>
                <p>Email: <strong><?php echo $_SESSION["email"]?></strong></p>
                <p>Data di nascita: <strong><?php echo $_SESSION["data"]?></strong></p>
                <form action="./login.php" method="post">
                    <input class="collapse" type="text" name="logout" value="TRUE" readonly="readonly" id="logout">
                    <label for="logout" hidden>Logout:</label>
                    <button type="submit" class="btn btn-outline-danger">Logout</button>
                </form>
                <hr class="bg-secondary">
                <p class="text-uppercase text-center fs-6">Informazioni di spedizione</p>

                <?php foreach($templateParams["indirizziSalvati"] as $indirizzo):?>

                <hr class="bg-secondary">
                <p><?php echo $indirizzo["ViaCivico"]?></p>
                <p><?php echo $indirizzo["Citta"]?>(<?php echo $indirizzo["Provincia"]?>), <?php echo $indirizzo["Cap"]?></p>

                <?php endforeach; ?>
            </div>
            <div class="col-12 col-md-4">
                <p class="text-uppercase text-center fs-6">Storico ordini</p>

                <?php foreach($templateParams["storicoOrdini"] as $prodotto):?>

                <hr class="bg-secondary">

                <div class="row d-flex flex-row">
                    <div class="col-3">
                        <img src="<?php echo UPLOAD_DIR?><?php echo $prodotto["Immagine"]; ?>" alt="" class="img-fluid shadow rounded">
                    </div>
                    <div class="col-9">
                        <p class="fw-bold"><?php echo $prodotto["Titolo"]; ?> x <?php echo $prodotto["Quantita"]; ?></p>
                        <div class="d-flex justify-content-between">
                            <div>
                                <p><?php echo $prodotto["UltimoAggiornamento"]; ?></p>
                            </div>
                            <div>
                                <p class="fs-6"><strong>€ <?php echo $prodotto["Quantita"] * $prodotto["Prezzo"]; ?></strong></p>
                            </div>
                        </div>
                    </div>
                </div>

                <?php endforeach?>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
