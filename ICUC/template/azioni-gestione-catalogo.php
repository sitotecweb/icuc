<div class="container-fluid px-0 py-5 mb-4">
    <div class="row m-0 pt-5">
        <div class="col-12 text-center text-white text-uppercase my-3">
            <p class="fw-bold mb-0 text-dark fs-1">GESTIONE</p><p class="fw-bold mb-0 text-danger fs-1">PRODOTTI</p>
        </div>
    </div>
    <div class="row m-0 pt-5">
        <div class="col-md-2"></div>
        <div class="col-12 col-md-8">
            <div class="row">
                <div class="col-12 col-md-4">
                    <div class="card border-danger h-100">
                        <div class="card-body text-center">
                            <p class="card-title text-danger fs-3">AGGIUNGI PRODOTTO</p>
                            <p class="card-text fs-5">Area dedicata all'aggiunta di nuovi prodotti, non ancora presenti nel negozio.</p>
                        </div>
                        <div class="card-footer">
                            <div class="d-grid gap-2 align-bottom">
                                    <a href="gestione-aggiunta-prodotto.php" class="btn btn-danger btn-lg btn-block align-bottom" id="bottomBtn1">Accedi</a> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="card border-danger h-100">
                        <div class="card-body text-center">
                            <p class="card-title text-danger fs-3">MODIFICA PRODOTTO</p>
                            <p class="card-text fs-5">Area dedicata alla modifica di prodotti attualmente presenti nel negozio.</p>
                        </div>
                        <div class="card-footer">
                            <div class="d-grid gap-2 align-bottom">
                                <a href="gestione-modifica-prodotto.php" class="btn btn-danger btn-lg btn-block align-bottom" id="bottomBtn2">Accedi</a> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="card border-danger h-100">
                        <div class="card-body text-center">
                            <p class="card-title text-danger fs-3">GESTIONE CUPON</p>
                            <p class="card-text text-dark fs-5">Area dedicata alla gestione dei codici alfanumerici inseribili in fase di acquisto, per ottenere un corrispondente sconto sull'ordine in oggetto. Un codice può essere usato una volta per account.</p>
                        </div>
                        <div class="card-footer">
                            <div class="d-grid gap-2 align-bottom">
                                    <a href="gestione-cupon.php" class="btn btn-danger btn-lg btn-block align-bottom" id="bottomBtn4">Accedi</a> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>     
        <div class="col-md-2"></div>
    </div>
</div>