        <div class="container-fluid">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-12 col-md-8">
                    <h1 class="text-center my-5">Impossibile caricare il carrello!</h1>
                    <hr>
                    <div class="alert alert-danger my-5" role="alert">
                        Stai cercando di accedere al carrello senza aver effettuato il login.
                        <a href="./login.php" class="alert-link">Torna alla pagina di login.</a>
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>