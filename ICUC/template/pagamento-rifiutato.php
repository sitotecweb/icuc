        <div class="container-fluid">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-12 col-md-8">
                    <h1 class="text-center my-5">Pagamento rifiutato!</h1>
                    <hr>
                    <div class="alert alert-danger my-5" role="alert">
                        L'ordine è stato annullato, hai cercato di acquistare una quantità maggiore di quella presente in magazzino! Scegli una quantità valida nel carrello.
                        <a href="./carrello.php" class="alert-link">Torna al carrello.</a>
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>