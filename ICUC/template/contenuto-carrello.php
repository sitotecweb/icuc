        <div class="container-fluid">
            <div class="row">
                <p class="fs-3 text-uppercase text-center mt-5">Carrello</p>
            </div>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-5 px-5">
                    <div class="position-relative m-4">
                        <div class="progress" style="height: 1px;">
                            <div class="progress-bar bg-secondary" role="progressbar" style="width: 100%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <button type="button" class="position-absolute top-0 start-0 translate-middle btn btn-sm btn-danger rounded-pill" style="width: 2rem; height:2rem;">1</button>
                        <button type="button" class="position-absolute top-0 start-50 translate-middle btn btn-sm btn-secondary rounded-pill" style="width: 2rem; height:2rem;">2</button>
                        <button type="button" class="position-absolute top-0 start-100 translate-middle btn btn-sm btn-secondary rounded-pill" style="width: 2rem; height:2rem;">3</button>
                    </div>
                    <p class="fs-4 text-uppercase text-center mt-5">Riepilogo dell'ordine</p>

                    <?php foreach($templateParams["contenutoCarrello"] as $prodotto):?>

                    <hr class="bg-secondary">

                    <div class="row d-flex flex-row mb-5">
                        <div class="col-3">
                            <img src="<?php echo UPLOAD_DIR?><?php echo $prodotto["Immagine"]; ?>" alt="" class="img-fluid shadow rounded">
                        </div>
                        <div class="col-9">
                            <div class="d-flex justify-content-between">
                                <div>
                                    <p class="fw-bold"><?php echo $prodotto["Titolo"]; ?></p>
                                    <p><?php echo $prodotto["Sottotitolo"]; ?></p>
                                    <p>Disponibili: <?php echo $prodotto["QuantitaDisponibile"]; ?></p>
                                </div>
                                <div>
                                    <label for="<?php echo $prodotto["Id"]; ?>" class="fs-6">Quantità:</label>
                                    <input type="number" name="quan" id="<?php echo $prodotto["Id"]; ?>" class="form-control text-center changeCartQuantity" value="<?php echo $prodotto["Quantita"]; ?>" min="1" max="<?php echo $prodotto["QuantitaDisponibile"]; ?>">
                                    <label for="productID" hidden>ProductID:</label>
                                    <input id="productID" class="form-control text-center getId" value="<?php echo $prodotto["Id"]; ?>" hidden>
                                    <label for="<?php echo $prodotto["Id"]."MaxQuantita";?>" hidden>ProductMaxQantity:</label>
                                    <input id="<?php echo $prodotto["Id"]."MaxQuantita";?>" class="form-control text-center getId" value="<?php echo $prodotto["QuantitaDisponibile"]; ?>" hidden>
                                </div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div>
                                    <a href="carrello.php?Id=<?php echo $prodotto["Id"];?>" class="fw-bold text-secondary text-decoration-none">Rimuovi</a>
                                </div>
                                <div class="d-flex flwx-row">
                                    <p id="<?php echo $prodotto["Id"]."Quantita";?>"><?php echo $prodotto["Quantita"]; ?></p>
                                    <p class="px-2">x</p>
                                    <p><strong>€<?php echo $prodotto["Prezzo"]; ?></strong></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php endforeach?>

                </div>
                <div class="col-md-3 px-5 mb-5">
                    <div id="updateCartId">
                        <p class="fs-4 text-uppercase mt-5">L'ordine ammonta a:</p>
                        <div class="d-flex justify-content-between">
                            <p>Totale parziale</p>
                            <p><?php echo $templateParams["totaleCarrello"]; ?>€</p>
                        </div>
                        <div class="d-flex justify-content-between">
                            <p>Spedizione</p>
                            <p>Gratis</p>
                        </div>
                        <hr class="bg-secondary mt-1">
                        <div class="d-flex justify-content-between">
                            <p class="fw-bold">Totale</p>
                            <p class="fw-bold"><?php echo $templateParams["totaleCarrello"]; ?>€</p>
                        </div>
                    </div>
                    <a href="./indirizzo.php" class="btn btn-danger text-uppercase fw-bold checkout-button mb-4">Vai al checkout</a>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>