        <div class="container-fluid product-background">
            <div class="row mb-4 pt-5">
                <div class="col-md-2"></div>
                <div class="col-12 col-md-8 d-flex flex-column flex-md-row">
                    <div class="col-md-5 text-center align-self-center pr-0 product-detail-image my-5">
                        <img src="<?php echo UPLOAD_DIR.$templateParams["prodotto"]["Immagine"]; ?>" alt="" class="img-fluid">
                    </div>
                    <div class="col-md-7">
                        <p class="fs-2 mb-1 mt-2 fw-bold"><?php echo $templateParams["prodotto"]["Titolo"]; ?></p>
                        <p class="fs-5 text-uppercase"><?php echo $templateParams["prodotto"]["Sottotitolo"]; ?></p>
                        <hr class="my-3">
                        <div class="d-flex flex-row">
                            <div>
                                <img src="<?php echo UPLOAD_DIR?>italia.png" alt="">
                            </div>
                            <div>
                                <p class="px-3 fs-5"><?php echo $templateParams["prodotto"]["Provenienza"]; ?></p>
                            </div>
                        </div>
                        <p class="my-2"><?php echo $templateParams["prodotto"]["Descrizione"]; ?></p>
                        <p class="fs-2 fw-bold my-4">€<?php echo $templateParams["prodotto"]["Prezzo"]; ?></p>
                        <label for="productQuantity" class="form-label">Quantità:</label>
                        <select id="productQuantity" class="form-select">
                            <option selected>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                            <option>9</option>
                            <option>10</option>
                        </select>
                        <button type="button" id="addToCartClick" class="btn btn-danger btn-lg mt-5 mb-5">Aggiungi al carrello</button>
                        
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>

            <div class="row mb-4 mt-5">
                <div class="col-md-2"></div>
                <div class="col-12 col-md-8 d-flex flex-column flex-md-row">
                    <div class="col-md-8 mb-5">
                        <p class="fw-bold mb-1">Colore</p>
                        <p class="mb-1"><?php echo $templateParams["prodotto"]["Colore"]; ?></p>
                        <hr>
                        <p class="fw-bold mb-1">Profumo</p>
                        <p class="mb-1"><?php echo $templateParams["prodotto"]["Profumo"]; ?></p>
                        <hr>
                        <p class="fw-bold mb-1">Sapore</p>
                        <p class="mb-1"><?php echo $templateParams["prodotto"]["Sapore"]; ?></p>
                    </div>
                    <div class="col-md-4">
                        <div class="m-5 mt-0">
                            <div class="bg-light p-4">
                                <p class="text-uppercase text-secondary">Caratteristiche</p>
                                <hr class="bg-secondary">
                                <p class="fs-6 fw-bold mb-1">Vitigno:</p>
                                <p class="mb-1"><?php echo $templateParams["prodotto"]["Vitigno"]; ?></p>
                                <p class="fw-bold mb-1">Gradazione:</p>
                                <p class="mb-1"><?php echo $templateParams["prodotto"]["Gradazione"]; ?></p>
                                <p class="fw-bold mb-1">Temperatura di servizio:</p>
                                <p class="mb-1"><?php echo $templateParams["prodotto"]["Temperatura"]; ?></p>
                                <p class="fw-bold mb-1">Caratteristiche:</p>
                                <p class="mb-1"><?php echo $templateParams["prodotto"]["Caratteristiche"]; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>

        <?php if(isset($_SESSION["idUtente"])){
            $idPresente = 1;
        } else {
            $idPresente = 0;
        }?>
                    
        <script>
            let productId = <?php echo $templateParams["prodotto"]["Id"]; ?>;
            let idSetted = <?php echo $idPresente; ?>;
        </script>

        <div id="addedToCart">
            
        </div>