        <div class="container-fluid">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-12 col-md-8">
                    <h1 class="text-center my-5">Pagamento effettuato!</h1>
                    <hr>
                    <div class="alert alert-warning my-5" role="alert">
                        Il pagamento è avvenuto con successo, riceverai una notifica quando il tuo ordine sarà spedito.
                        <a href="./index.php" class="alert-link">Torna alla home.</a>
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>