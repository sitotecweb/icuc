<div class="container-fluid px-0 py-5 mb-4">
    <div class="row m-0 pt-5">
        <div class="col-12 text-center text-white text-uppercase my-3">
            <h1 class="fw-bold mb-0 text-dark">GESTIONE</h1><h1 class="fw-bold mb-0 text-danger">ORDINI</h1>
        </div>
    </div>
    <div class="row m-0 pt-5">
        <div class="col-12 col-md-2"></div>
        <div class="col-12 col-md-8">
            <form class="border border-3 border-danger px-4 py-4 text-center">

                <label class="mt-1 h5">ORDINI NON COMPLETATI:</label>
                <div class="table-responsive">
                    <table class="table table-danger table-striped table-hover mt-1" id="tableUncompleted">
                        <thead>
                        <tr>
                            <th id="idOrdine" scope="col">Id Oridne</th>
                            <th id="idCliente" scope="col">Id Cliente</th>
                            <th id="idIndirizzo" scope="col">Id Indirizzo</th>
                            <th id="idProdotto" scope="col">Id Prodotto</th>
                            <th id="quantita" scope="col">Quantita</th>
                            <th id="ultimoAggiornamento" scope="col">Ultimo Aggiornamento</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php for($i = 0; $i < count($templateParams["ordiniNelCarrello"]); $i++): 
                                    $ordine = $templateParams["ordiniNelCarrello"][$i]; ?>
                                    <tr>
                                        <th id="ordineId<?php echo $ordine["Id"];?>" scope="row"><?php echo $ordine["Id"];?></th>
                                        <td headers="ordineId<?php echo $ordine["Id"];?> idCliente"><?php echo $ordine["IdUtente"];?></td>
                                        <td headers="ordineId<?php echo $ordine["Id"];?> idIndirizzo"><?php echo $ordine["IdIndirizzo"];?></td>
                                        <td headers="ordineId<?php echo $ordine["Id"];?> idProdotto"><?php echo $ordine["IdProdotto"];?></td>
                                        <td headers="ordineId<?php echo $ordine["Id"];?> quantita"><?php echo $ordine["Quantita"];?></td>
                                        <td headers="ordineId<?php echo $ordine["Id"];?> ultimoAggiornamento"><?php echo $ordine["UltimoAggiornamento"];?></td>
                                    </tr>
                            <?php endfor; ?>
                        </tbody>
                    </table>
                </div>
                <hr class="bg-secondary mt-1">

                <label class="mt-1 h5">ORDINI DA SPEDIRE:</label>
                <div class="table-responsive">
                    <table class="table table-warning table-striped table-hover mt-1 responsive" id="tableToSend">
                        <thead>
                        <tr>
                            <th id="idOrdine1" scope="col">Id Oridne</th>
                            <th id="idCliente1" scope="col">Id Cliente</th>
                            <th id="idIndirizzo1" scope="col">Id Indirizzo</th>
                            <th id="idProdotto1" scope="col">Id Prodotto</th>
                            <th id="quantita1" scope="col">Quantita</th>
                            <th id="ultimoAggiornamento1" scope="col">Ultimo Aggiornamento</th>
                            <th id="azioniDisponibili1" scope="col">Azioni Disponibili</th>

                        </tr>
                        </thead>
                        <tbody>
                            <?php for($i = 0; $i < count($templateParams["ordiniDaSpedire"]); $i++): 
                                    $ordine = $templateParams["ordiniDaSpedire"][$i]; ?>
                                    <tr>
                                        <th id="ordineId1<?php echo $ordine["Id"];?>" scope="row"><?php echo $ordine["Id"];?></th>
                                        <td headers="ordineId1<?php echo $ordine["Id"];?> idCliente1"><?php echo $ordine["IdUtente"];?></td>
                                        <td headers="ordineId1<?php echo $ordine["Id"];?> idIndirizzo1"><?php echo $ordine["IdIndirizzo"];?></td>
                                        <td headers="ordineId1<?php echo $ordine["Id"];?> idProdotto1"><?php echo $ordine["IdProdotto"];?></td>
                                        <td headers="ordineId1<?php echo $ordine["Id"];?> quantita1"><?php echo $ordine["Quantita"];?></td>
                                        <td headers="ordineId1<?php echo $ordine["Id"];?> ultimoAggiornamento1"><?php echo $ordine["UltimoAggiornamento"];?></td>
                                        <td headers="ordineId1<?php echo $ordine["Id"];?> azioniDisponibili1"><button type="button" class="btn btn-success btn-sm spedisci" id="<?php echo $ordine["Id"];?>">Spedisci</button></td>
                                    </tr>
                            <?php endfor; ?>
                        </tbody>
                    </table>
                </div>
                <hr class="bg-secondary mt-1">

                <label class="mt-1 h5">ORDINI IN TRANSITO:</label>
                <div class="table-responsive">
                    <table class="table table-success table-striped table-hover mt-1 responsive" id="tableTransit">
                        <thead>
                        <tr>
                            <th id="idOrdine2" scope="col">Id Oridne</th>
                            <th id="idCliente2" scope="col">Id Cliente</th>
                            <th id="idIndirizzo2" scope="col">Id Indirizzo</th>
                            <th id="idProdotto2" scope="col">Id Prodotto</th>
                            <th id="quantita2" scope="col">Quantita</th>
                            <th id="ultimoAggiornamento2" scope="col">Ultimo Aggiornamento</th>
                            <th id="azioniDisponibili2" scope="col">Azioni Disponibili</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php for($i = 0; $i < count($templateParams["ordiniInTransito"]); $i++): 
                                    $ordine = $templateParams["ordiniInTransito"][$i]; ?>
                                    <tr>
                                        <th id="ordineId2<?php echo $ordine["Id"];?>" scope="row"><?php echo $ordine["Id"];?></th>
                                        <td headers="ordineId2<?php echo $ordine["Id"];?> idCliente2"><?php echo $ordine["IdUtente"];?></td>
                                        <td headers="ordineId2<?php echo $ordine["Id"];?> idIndirizzo2"><?php echo $ordine["IdIndirizzo"];?></td>
                                        <td headers="ordineId2<?php echo $ordine["Id"];?> idProdotto2"><?php echo $ordine["IdProdotto"];?></td>
                                        <td headers="ordineId2<?php echo $ordine["Id"];?> quantita2"><?php echo $ordine["Quantita"];?></td>
                                        <td headers="ordineId2<?php echo $ordine["Id"];?> ultimoAggiornamento2"><?php echo $ordine["UltimoAggiornamento"];?></td>
                                        <td headers="ordineId2<?php echo $ordine["Id"];?> azioniDisponibili2"><button type="button" class="btn btn-light btn-sm consegna" id="<?php echo $ordine["Id"];?>">Consegna</button></td>
                                    </tr>
                        <?php endfor; ?>
                        </tbody>
                    </table>
                </div>
                <hr class="bg-secondary mt-1">

                <label class="mt-1 h5">ORDINI CONSEGNATI:</label>
                <div class="table-responsive">
                    <table class="table table-light table-striped table-hover mt-1" id="tableEnd">
                        <thead>
                        <tr>
                            <th id="idOrdine3" scope="col">Id Oridne</th>
                            <th id="idCliente3" scope="col">Id Cliente</th>
                            <th id="idIndirizzo3" scope="col">Id Indirizzo</th>
                            <th id="idProdotto3" scope="col">Id Prodotto</th>
                            <th id="quantita3" scope="col">Quantita</th>
                            <th id="ultimoAggiornamento3" scope="col">Ultimo Aggiornamento</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php for($i = 0; $i < count($templateParams["ordiniConsegnati"]); $i++): 
                                    $ordine = $templateParams["ordiniConsegnati"][$i]; ?>
                                    <tr>
                                        <th id="ordineId3<?php echo $ordine["Id"];?>" scope="row"><?php echo $ordine["Id"];?></th>
                                        <td headers="ordineId3<?php echo $ordine["Id"];?> idCliente3"><?php echo $ordine["IdUtente"];?></td>
                                        <td headers="ordineId3<?php echo $ordine["Id"];?> idIndirizzo3"><?php echo $ordine["IdIndirizzo"];?></td>
                                        <td headers="ordineId3<?php echo $ordine["Id"];?> idProdotto3"><?php echo $ordine["IdProdotto"];?></td>
                                        <td headers="ordineId3<?php echo $ordine["Id"];?> quantita3"><?php echo $ordine["Quantita"];?></td>
                                        <td headers="ordineId3<?php echo $ordine["Id"];?> ultimoAggiornamento3"><?php echo $ordine["UltimoAggiornamento"];?></td>
                                    </tr>
                            <?php endfor; ?>
                        </tbody>
                    </table>
                </div>
            </form>
        </div>
        <div class="col-12 col-md-8"></div>
        <div id="appendAdvancementResult"></div>
    </div>
</div>