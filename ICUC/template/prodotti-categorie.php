        <div class="container-fluid">
            <div class="row mb-2 mt-5">
                <div class="col-md-2"></div>


                <div class="col-md-2 p-4 p-5  pb-0">

                    <div class="accordion d-md-none" id="accordionExample">
                        <div class="accordion-item">
                            <div class="accordion-header" id="headingOne">
                                <button class="accordion-button fas fa-filter" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                </button>
                            </div>
                            <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <p class="text-secondary text-uppercase fw-bold">Filtri</p>
                                    <hr class="bg-secondary">
                                    <p class="text-secondary text-uppercase">Ordina per</p>
                                    <div class="d-flex flex-row">
                                        <div class="col-4 border rounded-start border-secondary text-center button-filter alpha">
                                            <p class="small-text text-secondary text-uppercase mb-0">Nome<br>(ABC..)</p>
                                        </div>
                                        <div class="col-4 border border-start-0 border-end-0 border-secondary text-center button-filter prezzoAlto">
                                            <p class="small-text text-secondary text-uppercase mb-0">Prezzo<br>alto</p>
                                        </div>
                                        <div class="col-4 border rounded-end border-secondary text-center button-filter prezzoBasso">
                                            <p class="small-text text-secondary text-uppercase mb-0">Prezzo<br>basso</p>
                                        </div>
                                    </div>
                                    <hr class="bg-secondary">
                                    <p class="text-secondary text-uppercase mb-1">Filtra per prezzo max</p>
                                    <label for="maxPriceT" hidden>PriceRange:</label>
                                    <input type="range" id="maxPriceT" class="form-range text-secondary" value="100" min="0" max="100" oninput="priceRangeHelper(this.value)" onchange="priceRangeHelper(this.value)">
                                    <hr class="bg-secondary">
                                    <div class="d-flex flex-row">
                                        <div class="col-6">
                                            <button type="button" id="filtroPrezzoT" class="btn btn-outline-danger">Filtra prezzo</button>
                                        </div>
                                        <div class="col-6 text-end mt-2">
                                            <p class="fw-bold selectedPrice">0 - 100€</p>
                                        </div>
                                    </div>
                                    <hr class="bg-secondary my-2">
                                    <div class="filter-accordion">
                                        <button class="btn text-uppercase text-secondary text-left px-0">
                                            Provenienza
                                        </button>
                                        <div>
                                            <?php foreach($templateParams["provenienze"] as $provenienza):?>
                                                <div class="provenienza"><?php echo $provenienza["Provenienza"];?>
                                                    <input title ="Provenienza" type="text" value="<?php echo $provenienza["Id"];?>" hidden>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                        <hr class="bg-secondary my-2">
                                        <button class="btn text-uppercase text-secondary text-left px-0">
                                            Annate
                                        </button>
                                        <div>
                                            <?php foreach($templateParams["annate"] as $annata):?>
                                                <div class="annata"><?php echo $annata["Annata"];?>
                                                    <input title="Annata" type="text" value="<?php echo $annata["Id"];?>" hidden>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                    <hr class="bg-secondary my-2">
                                    <div>
                                        <button type="button" class="btn btn-outline-danger rimuoviFiltri">Rimuovi filtri</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="d-none d-md-block">
                        <p class="text-secondary text-uppercase fw-bold">Filtri</p>
                        <hr class="bg-secondary">
                        <p class="text-secondary text-uppercase">Ordina per</p>
                        <div class="d-flex flex-row">
                            <div class="col-4 border rounded-start border-secondary text-center button-filter alpha">
                                <p class="small-text text-secondary text-uppercase mb-0">Nome<br>(ABC..)</p>
                            </div>
                            <div class="col-4 border border-start-0 border-end-0 border-secondary text-center button-filter prezzoAlto">
                                <p class="small-text text-secondary text-uppercase mb-0">Prezzo<br>alto</p>
                            </div>
                            <div class="col-4 border rounded-end border-secondary text-center button-filter prezzoBasso">
                                <p class="small-text text-secondary text-uppercase mb-0">Prezzo<br>basso</p>
                            </div>
                        </div>
                        <hr class="bg-secondary">
                        <p class="text-secondary text-uppercase mb-1">Filtra per prezzo max</p>
                        <label for="maxPriceD" hidden>PriceRange:</label>
                        <input type="range" id="maxPriceD" class="form-range text-secondary" value="100" min="0" max="100" oninput="priceRangeHelper(this.value)" onchange="priceRangeHelper(this.value)">
                        <hr class="bg-secondary">
                        <div class="d-flex flex-row">
                            <div class="col-6">
                                <button type="button" id="filtroPrezzoD" class="btn btn-outline-danger">Filtra prezzo</button>
                            </div>
                            <div class="col-6 text-end mt-2">
                                <p class="fw-bold selectedPrice">0 - 100€</p>
                            </div>
                        </div>
                        <hr class="bg-secondary my-2">
                        <div class="filter-accordion">
                            <button class="btn text-uppercase text-secondary text-left px-0">
                                Provenienza
                            </button>
                            <div>
                                <?php foreach($templateParams["provenienze"] as $provenienza):?>
                                    <div class="provenienza"><?php echo $provenienza["Provenienza"];?>
                                        <label for="<?php echo $provenienza["Id"];?>IdLabelProv" hidden>Provenienza: </label>
                                        <input type="text" id="<?php echo $provenienza["Id"];?>IdLabelProv" value="<?php echo $provenienza["Id"];?>" hidden>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <hr class="bg-secondary my-2">
                            <button class="btn text-uppercase text-secondary text-left px-0">
                                Annate
                            </button>
                            <div>
                                <?php foreach($templateParams["annate"] as $annata):?>
                                    <div class="annata"><?php echo $annata["Annata"];?>
                                        <label for="<?php echo $annata["Id"];?>IdLabelAnn" hidden>Provenienza: </label>
                                        <input type="text" id="<?php echo $annata["Id"];?>IdLabelAnn" value="<?php echo $annata["Id"];?>" hidden>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <hr class="bg-secondary my-2">
                        <div>
                            <button type="button" class="btn btn-outline-danger rimuoviFiltri">Rimuovi filtri</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 p-5">
                    <div>
                        <div class="col-12 d-flex">
                            <label for="searchD" hidden>Search:</label>
                            <input class="form-control me-2 rounded-0 parolaScelta" id="searchD" type="search" placeholder="Cerca prodotti..." aria-label="Search">
                            <button class="btn btn-danger rounded-0 ricercaParola" type="submit">Cerca</button>
                        </div>
                        <div class="row row-cols-1 row-cols-md-3 g-4 mt-3 elencoProdotti">
                            
                        </div>
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
        