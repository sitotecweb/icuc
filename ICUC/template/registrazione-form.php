        <div class="container-fluid">
            <div class="row g-3 mt-5">
                <div class="col-1 col-md-3"></div>
                <div class="col-10 col-md-6">
                    <form method="post">
                        <div class="row">
                            <h1 class="text-center text-uppercase">Registrazione</h1>
                            <div class="d-flex flex-column flex-md-row mt-1">
                                <div class="col-md-5">
                                    <label for="inputName" class="form-label">Nome</label>
                                    <input name="nome" type="text" class="form-control" id="inputName" required>
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-5">
                                    <label for="inputSurname" class="form-label">Cognome</label>
                                    <input name="cognome" type="text" class="form-control" id="inputSurname" required>
                                </div>
                                <div class="col-md-1"></div>
                            </div>
                            <div class="d-flex flex-column flex-md-row mt-1">
                                <div class="col-md-5">
                                    <label for="inputEmail4" class="form-label">Email</label>
                                    <input name="email" type="email" class="form-control" id="inputEmail4" required>
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-5">
                                    <label for="inputPassword4" class="form-label">Password</label>
                                    <input name="password" type="password" class="form-control" id="inputPassword4" required>
                                </div>
                                <div class="col-md-1"></div>
                            </div>
                            <div class="d-flex flex-column flex-md-row mt-1">
                                <div class="col-md-5">
                                    <label for="inputData" class="form-label">Data di nascita</label>
                                    <input name="dataNascita" type="date" class="form-control" id="inputData" onblur="return dobcheck()" required>
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-5">
                                    <label for="inputAddress" class="form-label">Indirizzo</label>
                                    <input name="indirizzo" type="text" class="form-control" id="inputAddress" placeholder="via Roma, 15" required>
                                </div>
                                <div class="col-md-1"></div>
                            </div>
                            <div class="d-flex flex-column flex-md-row mt-1">
                                <div class="col-md-5">
                                    <label for="inputCity" class="form-label">Città</label>
                                    <input name="citta" type="text" class="form-control" id="inputCity" required>
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-3">
                                    <label for="inputState" class="form-label">Provincia</label>
                                    <select name="provincia" id="inputState" class="form-select" required>
                                        <option value="" selected>--</option>
                                        <option value="ag">Agrigento</option>
                                        <option value="al">Alessandria</option>
                                        <option value="an">Ancona</option>
                                        <option value="ao">Aosta</option>
                                        <option value="ar">Arezzo</option>
                                        <option value="ap">Ascoli Piceno</option>
                                        <option value="at">Asti</option>
                                        <option value="av">Avellino</option>
                                        <option value="ba">Bari</option>
                                        <option value="bt">Barletta-Andria-Trani</option>
                                        <option value="bl">Belluno</option>
                                        <option value="bn">Benevento</option>
                                        <option value="bg">Bergamo</option>
                                        <option value="bi">Biella</option>
                                        <option value="bo">Bologna</option>
                                        <option value="bz">Bolzano</option>
                                        <option value="bs">Brescia</option>
                                        <option value="br">Brindisi</option>
                                        <option value="ca">Cagliari</option>
                                        <option value="cl">Caltanissetta</option>
                                        <option value="cb">Campobasso</option>
                                        <option value="ci">Carbonia-iglesias</option>
                                        <option value="ce">Caserta</option>
                                        <option value="ct">Catania</option>
                                        <option value="cz">Catanzaro</option>
                                        <option value="ch">Chieti</option>
                                        <option value="co">Como</option>
                                        <option value="cs">Cosenza</option>
                                        <option value="cr">Cremona</option>
                                        <option value="kr">Crotone</option>
                                        <option value="cn">Cuneo</option>
                                        <option value="en">Enna</option>
                                        <option value="fm">Fermo</option>
                                        <option value="fe">Ferrara</option>
                                        <option value="fi">Firenze</option>
                                        <option value="fg">Foggia</option>
                                        <option value="fc">Forl&igrave;-Cesena</option>
                                        <option value="fr">Frosinone</option>
                                        <option value="ge">Genova</option>
                                        <option value="go">Gorizia</option>
                                        <option value="gr">Grosseto</option>
                                        <option value="im">Imperia</option>
                                        <option value="is">Isernia</option>
                                        <option value="sp">La spezia</option>
                                        <option value="aq">L'aquila</option>
                                        <option value="lt">Latina</option>
                                        <option value="le">Lecce</option>
                                        <option value="lc">Lecco</option>
                                        <option value="li">Livorno</option>
                                        <option value="lo">Lodi</option>
                                        <option value="lu">Lucca</option>
                                        <option value="mc">Macerata</option>
                                        <option value="mn">Mantova</option>
                                        <option value="ms">Massa-Carrara</option>
                                        <option value="mt">Matera</option>
                                        <option value="vs">Medio Campidano</option>
                                        <option value="me">Messina</option>
                                        <option value="mi">Milano</option>
                                        <option value="mo">Modena</option>
                                        <option value="mb">Monza e della Brianza</option>
                                        <option value="na">Napoli</option>
                                        <option value="no">Novara</option>
                                        <option value="nu">Nuoro</option>
                                        <option value="og">Ogliastra</option>
                                        <option value="ot">Olbia-Tempio</option>
                                        <option value="or">Oristano</option>
                                        <option value="pd">Padova</option>
                                        <option value="pa">Palermo</option>
                                        <option value="pr">Parma</option>
                                        <option value="pv">Pavia</option>
                                        <option value="pg">Perugia</option>
                                        <option value="pu">Pesaro e Urbino</option>
                                        <option value="pe">Pescara</option>
                                        <option value="pc">Piacenza</option>
                                        <option value="pi">Pisa</option>
                                        <option value="pt">Pistoia</option>
                                        <option value="pn">Pordenone</option>
                                        <option value="pz">Potenza</option>
                                        <option value="po">Prato</option>
                                        <option value="rg">Ragusa</option>
                                        <option value="ra">Ravenna</option>
                                        <option value="rc">Reggio di Calabria</option>
                                        <option value="re">Reggio nell'Emilia</option>
                                        <option value="ri">Rieti</option>
                                        <option value="rn">Rimini</option>
                                        <option value="rm">Roma</option>
                                        <option value="ro">Rovigo</option>
                                        <option value="sa">Salerno</option>
                                        <option value="ss">Sassari</option>
                                        <option value="sv">Savona</option>
                                        <option value="si">Siena</option>
                                        <option value="sr">Siracusa</option>
                                        <option value="so">Sondrio</option>
                                        <option value="ta">Taranto</option>
                                        <option value="te">Teramo</option>
                                        <option value="tr">Terni</option>
                                        <option value="to">Torino</option>
                                        <option value="tp">Trapani</option>
                                        <option value="tn">Trento</option>
                                        <option value="tv">Treviso</option>
                                        <option value="ts">Trieste</option>
                                        <option value="ud">Udine</option>
                                        <option value="va">Varese</option>
                                        <option value="ve">Venezia</option>
                                        <option value="vb">Verbano-Cusio-Ossola</option>
                                        <option value="vc">Vercelli</option>
                                        <option value="vr">Verona</option>
                                        <option value="vv">Vibo valentia</option>
                                        <option value="vi">Vicenza</option>
                                        <option value="vt">Viterbo</option>
                                    </select>
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-1">
                                    <label for="inputZip" class="form-label">CAP</label>
                                    <input name="cap" type="text" class="form-control" id="inputZip" required>
                                </div>
                                <div class="col-md-1"></div>
                            </div>
                            <div class="mt-3">
                                <div class="col-12">
                                    <div class="form-check">
                                        <input name="condizioni" class="form-check-input" type="checkbox" id="gridCheck" required>
                                        <label class="form-check-label" for="gridCheck">
                                        Accetto i <a href="" class="text-decoration-none text-dark fw-bold">Termini e Condizioni</a>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="my-5">
                                <div class="col-12">
                                    <?php if(isset($templateParams["erroreRegistrazione"])): ?>
                                    <div class="form-text mb-5"><strong><?php echo $templateParams["erroreRegistrazione"]; ?></strong></div>
                                    <?php endif;?>
                                    <button type="button" class="btn btn-danger" onclick="formhash(this.form, this.form.password);">Registrati</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-1 col-md-3"></div>
            </div>
        </div>