<?php

require_once("./bootstrap.php");


$templateParams["titolo"] = "Home";
$templateParams["nome"] = "ultimi-arrivi.php";

$templateParams["ultimiArrivi"] = $dbh->getLatestProducts();
$templateParams["emilianoRomagnoli"] = $dbh->getEmilianoRomagnoli();
$templateParams["annata2020"] = $dbh->getAnnata2020();

$templateParams["js"] = array("js/jquery-3.4.1.min.js", "js/footer.js");

require("template/base.php");

?>